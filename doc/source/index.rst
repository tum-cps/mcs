.. ModRob Configuration Synthesis documentation master file, created by
   sphinx-quickstart on Tue May 31 16:06:43 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root ``toctree`` directive.

Welcome to ModRob Configuration Synthesis's documentation!
==========================================================

.. git_commit_detail::
    :branch:
    :commit:
    :sha_length: 10

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   background
   autoapi/index

ModRob Configuration Synthesis
------------------------------
This is the (so-far sparse) documentation of the project ModRob Configuration Synthesis by the Cyber-Physical Systems group at TU Munich.

Please feel free to contact the maintainers Jonathan Kuelz and Matthias Mayer at <firstname.lastname>@tum.de with questions or contributions.


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Important references
--------------------

* `CoBRA Standard for Robots, Tasks, Solutions, Costs <https://cobra.cps.cit.tum.de/>`_ --> Documentation
* Slack Channel (ask for invite)
* `Official Documentation Build <https://mcs-tum-cps-a9ef8fb7a07b76bb48a4290f9e3abe7b49180b0121a2704f2cb.pages.gitlab.lrz.de/doc/>`_

Building this documentation
---------------------------

The easiest way to build this documentation is with ``make html`` from within the doc folder; other formats such as pdf should also work.
The documentation is then available in doc/build/html/index.html.

The CI version uses the commands in ci/.gitlab-ci.yml in the section *pages*.
