Background
==========

Here we can document abstract information about design, API, publications, ...

Sphinx Examples and Hacks
-------------------------

* Code blocks - end introduction sentence with double colon, e.g. RobotFilerter.py -> IntermediateFilterResults::allow_changes
* Object fields with @property method - just comment method, e.g. Optimizer.py -> Optimizer::db
* Note (similar to param) can be used to highlight information, e.g. ScenarioPlanner -> HierarchicalScenarioPlanner::__init__

Publications
------------

* CoBRA: A Composable Benchmark for Robotic Applications, M. Mayer, K. Kuelz, M. Althoff, ICRA'24 - `Preprint <https://arxiv.org/abs/2203.09337>`_ and `website <https://cobra.cps.cit.tum.de>`_.
