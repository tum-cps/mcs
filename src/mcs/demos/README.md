# Demo Readme

This folder contains demos to serve as showcase on how to use different modules and libraries.
The [base](base) directory describes the simple usage of robots, tasks, and solutions without elaborate planning.
The demos in [ompl](ompl) use more complex planning algorithms to solve tasks that depend on the Open Motion Planning Library.
Please look into the main [README](../../../README.md) to get to know how to install all prerequisites.

Please note that the meshcatvisualizer terminates when your python script does.
This is the reason for the include statements in the demo: If you want to see a visualization, do not end the program by pressing enter.
The visualization is only accessible while your program did not terminate.
Additional information from the demos is displayed in the logs (on the console and or files defaulting to [/tmp/timor.log](/tmp/timor.log)).

To get more insights into functionalities, you can also check out the [unit tests](../../../tests).
Actually, there is a unittest that runs all demos to make sure they are functional and running.
Demo files with a leading underscore in their name will **not** be included in this test,
so if you want to exclude one (e.g. due to runtime constraints), follow this naming convention.
