#!/usr/bin/env python3
# Author: Jonathan Külz, Matthias Mayer
# Date: 12.01.22
# Update: 19.4.22
# Run with ompl installed, best via Dockerfile
from time import time

from meshcat.animation import Animation
import numpy as np
import pinocchio as pin
from timor import Geometry, ModuleAssembly, Transformation
from timor.Robot import PinRobot as Robot
from timor.task import Obstacle
from timor.task.CostFunctions import QDist
from timor.task.Solution import SolutionHeader, SolutionTrajectory
from timor.task.Task import Task, TaskHeader
from timor.utilities import spatial
from timor.utilities.file_locations import robots
from timor.utilities import logging
from timor.utilities.spatial import homogeneous, rotZ
from timor.utilities.tolerated_pose import ToleratedPose
from timor.utilities.visualization import MeshcatVisualizerWithAnimation

from mcs.OMPLPathPlanner import IKPlanner, RrtcPlanner
from mcs.utilities.trajectory import from_kunz_trajectory


def main():
    """Example of how to use the OMPLPathPlanner."""
    # Loading a robot from URDF
    robot_urdf = robots['panda'].joinpath('urdf').joinpath('panda.urdf')
    robot_meshes = robots['panda'].parent
    robot = Robot.from_urdf(robot_urdf, robot_meshes)
    robot_assembly = ModuleAssembly.from_monolithic_robot(robot)

    panda_TCP = pin.SE3(homogeneous(np.array([0, 0, .21]), rotZ(-np.pi / 4)[:3, :3]))  # Guessed that one manually
    robot.add_tcp_frame('panda_joint7', panda_TCP)  # Add the hand (not the fingers) as reference

    # Setting up some joint angles
    while True:
        q1 = pin.randomConfiguration(robot.model)
        robot.update_configuration(q1)
        if not robot.has_self_collision():
            break
    while True:
        q2 = pin.randomConfiguration(robot.model)
        robot.update_configuration(q2)
        if not robot.has_self_collision():
            break

    q_collision = np.asarray((1.9979, 0.07563, -1.6646, -3.0895, -1.9272, 0.3238, 1.3961))
    if not robot.has_self_collision(q_collision):
        logging.warning("q_collision should be in self-collision")

    # For mobile plattforms like Frankie - prevent them rolling to far away from the coordinate origin
    mask = np.zeros_like(q1, bool)
    for i, (lower, upper) in enumerate(zip(robot.model.lowerPositionLimit, robot.model.upperPositionLimit)):
        if any(abs(limit) > 10 for limit in (lower, upper)):
            mask[i] = True
    q1[mask] = q1[mask] / np.linalg.norm(q1[mask])
    q2[mask] = q2[mask] / np.linalg.norm(q2[mask])
    q_collision[mask] = q_collision[mask] / np.linalg.norm(q_collision[mask])
    goal = robot.fk(q2)

    # Kinematics Demo
    robot.update_configuration(q1)
    q2_ik, success = robot.ik(ToleratedPose(goal), ik_method='scipy')
    if success:
        robot.update_configuration(q2_ik)
    else:
        logging.warning(f'No Inverse Kinematics Soluation found for {goal.homogeneous}.')

    # Simple visualization
    robot.visualize()

    # Add collision objects
    box = Geometry.Box(dict(x=.3, y=.8, z=.6), spatial.homogeneous([.7, .6, .31]))
    sphere = Geometry.Sphere(dict(r=.1), spatial.homogeneous(np.asarray([1, -1, 1])))
    tabletop = Geometry.Box(dict(x=1, y=.8, z=.05))
    legs = [Geometry.Box(dict(x=.07, y=.07, z=.5), spatial.homogeneous([.5 - 1 * (i % 2), .4 - .8 * (i // 2), -.25]))
            for i in range(4)]
    table = Geometry.ComposedGeometry((box, sphere, tabletop, *legs))
    obstacle = Obstacle.Obstacle('123', collision=table, name='Table')

    header = TaskHeader('test', 'PyTest')
    task = Task(header)
    task.add_obstacle(obstacle)
    viz = task.visualize()
    robot.set_base_placement(Transformation.from_translation((0., 0., 0.04)))  # Place over table
    robot.visualize(viz)

    # Manually observe collisions in the task
    if robot.has_collisions(task):
        robot.collision_info(task)

    # Highlight self-collisions (without task)
    robot.update_configuration(q_collision)
    viz = pin.visualize.MeshcatVisualizer()
    viz.initViewer()
    robot.visualize_self_collisions(viz)

    # Finally, some path planning (without obstacles, but with self-collision):
    # Find two collision free joint angles to move in between
    t0 = time()
    while True:
        q_start = pin.randomConfiguration(robot.model)
        q_start[mask] = q_start[mask] / np.linalg.norm(q_start[mask])
        robot.update_configuration(q_start)
        if not robot.has_collisions(task):
            break
        if time() - t0 > 20:
            raise TimeoutError()
    t0 = time()
    while True:
        q_goal = pin.randomConfiguration(robot.model)
        q_goal[mask] = q_goal[mask] / np.linalg.norm(q_goal[mask])
        goal_pose = robot.fk(q_goal)
        # Accept collision free goal that is not trivially close to the start
        if not robot.has_collisions(task) and np.linalg.norm(q_start - q_goal) > 0.5:
            break
        if time() - t0 > 20:
            raise TimeoutError()

    # Find connecting path
    logging.info('Start the planner')
    planner = RrtcPlanner(robot, task, resolution=0.01, simplify_path=True, time_limit=10, rrtc_range=.9)
    path = planner.find_path(q_start, q_goal)  # The RRTC planner only gets via points
    path_reversed = planner.find_path(q_goal, q_start)
    assert np.all(path[0].q == path_reversed[-1].q), "The reversed path should end where the other starts"
    assert np.all(path[-1].q == path_reversed[0].q), "The reversed path should start where the other ends"
    ik_planner = IKPlanner(robot, task, time_limit=20)
    path_to_ik = ik_planner.find_path(q_start, ToleratedPose(goal_pose))

    if path is False:
        logging.warning("No path found for point to point")
    else:
        logging.info(
            "Animation of planning to desired configuration; this also shows how to generate a trajectory manually:")
        path = np.asarray(path.q)
        frame_rate = 30
        t = from_kunz_trajectory(path, dt=1 / frame_rate,
                                 v_max=robot.model.velocityLimit, a_max=np.tile(1., robot.configuration.shape))
        vis = MeshcatVisualizerWithAnimation()
        vis.initViewer()
        robot.visualize(vis)
        task.visualize(vis)

        anim = Animation(default_framerate=frame_rate)
        for i in range(0, t.q.shape[0]):
            with anim.at_frame(vis.viewer, i) as frame:
                robot.update_configuration(t.q[i, :])
                vis.create_animation_frame(pin.GeometryType.VISUAL, frame)

        vis.viewer.set_animation(anim)

    if path_to_ik is False:
        logging.warning("IK Path not found")
    else:
        logging.info("Animation of planning to desired pose not configuration showing visualization via a solution:")
        ik_sol = SolutionTrajectory(path_to_ik, SolutionHeader("1"), task, robot_assembly, QDist(), robot.placement)
        ik_sol.visualize()

    input('Press enter to continue...')


if __name__ == '__main__':
    main()
