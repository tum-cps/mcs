import traceback

from timor import ModuleAssembly, ModulesDB
from timor.task.Task import Task
import timor.utilities.logging as logging
from timor.utilities.visualization import MeshcatVisualizerWithAnimation

import cobra
from mcs.PathPlanner import PathPlanningFailedException
from mcs.ConfigurationOptimizer import UsePredefinedAssembly
from mcs.utilities.local_files import head


def main():
    """Example loading a simple construction site and finding a valid robot trajectory."""
    db = ModulesDB.from_name('modrob-gen2')
    assembly = ModuleAssembly.from_serial_modules(
        db, ('105', '2', '2', '24', '2', '25', '1', '1', '1', 'GEP2010IL'))
    q0 = assembly.robot.configuration

    # Load task
    if not head.joinpath('tests/data/tasks/').is_dir():
        print("Running with different demo task that is available online...")
        task = Task.from_json_file(cobra.task.get_task(id='simple/PTP_1'))
    else:
        task = Task.from_id('CONCERT/Demo_1', head.joinpath('tests/data/tasks/'))
    assembly.robot.set_base_placement(task.base_constraint.base_pose.nominal.in_world_coordinates())
    vis = MeshcatVisualizerWithAnimation()
    vis.initViewer()
    task.visualize(vis)

    # Find path
    logging.info("Solving task ...")
    solver = UsePredefinedAssembly(assembly, timeout=10)
    for _ in range(10):
        assembly.robot.update_configuration(q0)  # Reset robot to known valid start
        try:
            solution = solver.solve(task)
            break
        except PathPlanningFailedException:
            logging.debug(traceback.print_exc())
            logging.warning("Path planner did not find valid path. Retry ...")
        except ValueError:
            logging.debug(traceback.print_exc())
            logging.warning("Planning failed due to invalid constructed solution or not solvable goal. Retry ...")
        except TimeoutError:
            logging.debug(traceback.print_exc())
            logging.warning("Planner timed out. Retry ...")
    else:
        raise TimeoutError("No solution found in 10 trials; weird ...")

    logging.info(f"Got solution taking {solution.time_steps[-1]} seconds")

    # Visualize
    solver.visualize(vis)
    solution.visualize(vis)
    logging.info(f"View planning result {vis.viewer.url()}")
    logging.info(f"The task is {'valid' if solution.valid else 'invalid'}")
    # solution.trajectory.plot()
    input("Press any key to finish demo ...")


if __name__ == '__main__':
    main()
