from copy import deepcopy
from datetime import datetime

from timor import Transformation
from timor.task import Tolerance
from timor.task.Constraints import BasePlacement, CollisionFree, JointLimits, SelfCollisionFree
from timor.task.Task import TaskHeader
from timor.utilities.tolerated_pose import ToleratedPose

from mcs.optimize.TaskGenerator import liu_2020_desired_workspace
from mcs.utilities.local_files import package


"""
Create dataset for Case 3 described in [1]. These are tasks that try to optimize the desired workspace of a robot by
testing a grid of translations and rotations in workspace for reachability -existence of an inverse kinematics solution-
with a robot. Could be a good task to judge robot utility if the concrete goals are not known when optimizing the robot
but one knows a rough area in which the robot should be able to act.

The grid of desired poses consists of:
* translation grid around a center pose in the x / y / z  (trans_grid_size / width).
* rotation "grid" at each of these positions, i.e. testing different end-effector rotations. The default check 4*2*4
  orientations taking every combination of these:
    * 4 rotations about Z (+/- 45° and +/- 135°)
    * 2 rotations about Y (+/- 45°)
    * 4 rotations about X (+/- 45° and +/- 135°)

[1] Liu, et al., Optimizing performance in automation through modular robots, 2020.
"""


def main():
    """Create dataset for Case 3 described in [1]."""
    out_dir = package.parent.joinpath("output")
    if not out_dir.is_dir():
        out_dir.mkdir()
    header = TaskHeader("Liu2020", version="R2022a", taskName="Liu2020", tags=["Liu 2020", "Synthetic"],
                        date=datetime(2023, 3, 1), author=["Matthias Mayer"], email=["matthias.mayer@tum.de"],
                        affiliation=["TU Munich"])
    header_WS = header.asdict()
    header_WS["ID"] += "_Case3"
    header_WS["taskName"] += "_Case3"
    header_WS["tags"] += ["Case 3", "Dexterity", "Workspace Optimization"]

    # Note: Not forced to fulfill all goals; the more the better
    constraints = (SelfCollisionFree(), CollisionFree(), JointLimits(("q", "tau")),
                   BasePlacement(ToleratedPose(Transformation.neutral(), Tolerance.DEFAULT_SPATIAL)))

    for WS_name, center, trans_grid_size, trans_grid_width in (
            ("0", Transformation.from_translation((0.3 + 5 / 2 * 0.1, 0, 0)), (2, 2, 1), 0.1),  # Dummy for debug
            ("1", Transformation.from_translation((0.3 + 5 / 2 * 0.1, 0, 0)), (5, 5, 1), 0.1),  # Workspace 1
            ("2", Transformation.from_translation((0.3 + 6 / 2 * 0.05, 0, 0)), (6, 6, 1), 0.05),  # Workspace 2
            ("3", Transformation.from_translation((0.3 + 3 / 2 * 0.05, 0, 0)), (5, 5, 1), 0.05)  # Workspace 3
    ):
        header_WS_N = deepcopy(header_WS)
        header_WS_N["ID"] += f"_{WS_name}"
        header_WS_N["taskName"] += f"_{WS_name}"
        header_WS_N["tags"] += [f"Workspace {WS_name}"]

        task = liu_2020_desired_workspace(
            trans_grid_size, trans_grid_width, center, constraints=constraints, header_overwrite=header_WS_N)
        task.to_json_file(out_dir.joinpath(f"{header_WS_N['ID']}.json"))
        print(f"Case {WS_name} can be viewed under the following URL")
        with out_dir.joinpath(f"{header_WS_N['ID']}.html").open("w") as f:
            f.write(task.visualize().viewer.static_html())

    input("Press any key to finish.")


if __name__ == '__main__':
    main()
