#!/usr/bin/env python3
# Author: Jonathan Külz
# Date: 11.01.22
from pathlib import Path

import numpy as np
import pinocchio as pin
from pinocchio.robot_wrapper import RobotWrapper
from pinocchio.visualize import MeshcatVisualizer
from timor.utilities.file_locations import robots


def main():
    """
    Provides some insights into typical pinocchio interfaces.

    https://github.com/stack-of-tasks/pinocchio
    """
    # Loads a Model
    robot = str(robots['panda'].joinpath('urdf').joinpath('panda.urdf').as_posix())
    mesh_dir = str(Path(robot).parent.parent.parent)
    model = pin.buildModelFromUrdf(robot)
    model, collision_model, visual_model = pin.buildModelsFromUrdf(robot, mesh_dir)
    print('Model Name: {}'.format(model.name))

    # Generates model data such as Jacobian, center of mass and many more
    data = model.createData()

    # Example use: Find a random configuration and print all joint positions
    q = pin.randomConfiguration(model)
    pin.forwardKinematics(model, data, q)
    for name, oMi in zip(model.names, data.oMi):
        print('{} - {} {} {}'.format(name, *oMi.translation.T.flat))

    # Visualize // This currently only works until the script terminates
    viz = MeshcatVisualizer(model, collision_model, visual_model)
    viz.initViewer()
    viz.loadViewerModel()
    viz.display(np.zeros_like(q))

    # Visualize the same robot, but build from the robot wrapper class and displaced in thy xy-plane
    twin_robo = RobotWrapper.BuildFromURDF(robot, package_dirs=mesh_dir)
    displacement = pin.XYZQUATToSE3((.8, .5, 0, 0, 0, .1, 1))
    twin_robo.model.jointPlacements[1] = displacement * twin_robo.model.jointPlacements[1]
    twin_robo.visual_model.geometryObjects[0].placement = displacement * \
        twin_robo.visual_model.geometryObjects[0].placement
    twin_robo.collision_model.geometryObjects[0].placement = displacement * \
        twin_robo.collision_model.geometryObjects[0].placement
    twin_robo.visual_data.oMg[0] = displacement * twin_robo.visual_data.oMg[0]
    twin_robo.collision_data.oMg[0] = displacement * twin_robo.collision_data.oMg[0]
    twin = MeshcatVisualizer(twin_robo.model, twin_robo.collision_model, twin_robo.visual_model)
    twin.initViewer(viz.viewer)  # Assign the same viewer to this visualizer
    twin.loadViewerModel(rootNodeName="Twin")
    offset_q = q.copy()
    twin.display(offset_q)

    # Self-Collision Checking
    collision_model.addAllCollisionPairs()
    collision_data = pin.GeometryData(collision_model)
    pin.computeCollisions(model, data, collision_model, collision_data, q, False)
    for k in range(len(collision_model.collisionPairs)):
        cr = collision_data.collisionResults[k]
        cp = collision_model.collisionPairs[k]
        print('Collision pair', cp.first, cp.second, 'Collision:', 'Yes' if cr.isCollision() else 'No')

    # Example two: Inverse kinematics
    input("Press Enter to continue...")


if __name__ == '__main__':
    main()
