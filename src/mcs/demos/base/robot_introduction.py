#!/usr/bin/env python3
# Author: Jonathan Külz
# Date: 22.06.22
import warnings

import numpy as np
import pinocchio as pin
from timor import ModulesDB, Transformation
from timor.Robot import PinRobot
from timor.utilities import file_locations, logging, prebuilt_robots, spatial
from timor.utilities.tolerated_pose import ToleratedPose

"""It is recommended to run this demo in debugging mode and to click through it step by step."""


def main():
    """
    This demo shows properties and methods of the most commonly used robot representation: The PinRobot

    The PinRobot is a wrapper about a robot model created with the pinocchio library, an external robotics toolset.
    They provide fast calculation of robot kinematics and dynamics. To provide easy access to these methods and some
    useful additional features, the PinRobot class was created.
    """
    # There are multiple ways to create / generate a PinRobot
    #  1. You can just provide a pinocchio robot model if you have one at hand (unlikely)

    #  2. You can create it from a URDF file (a standard robot description format)
    #    We load the Franka Emika Panda from URDF (you could find it on Github if it was not already here)
    urdf_location = file_locations.robots['panda'].joinpath('urdf').joinpath('panda.urdf')
    panda = PinRobot.from_urdf(urdf_file=urdf_location,
                               package_dir=urdf_location.parent.parent.parent)
    # Have a look at the visualization - we have a panda in our lab, so you probably recognize it:
    panda_viz = panda.visualize()

    #  3. You can build it from a module assembly - this is a description of modular robots developed at TUM
    #    How to build a proper Module Assembly is not part of this tutorial - we just load a predefined one:
    predefined_assembly = prebuilt_robots.random_assembly(6, ModulesDB.from_name('modrob-gen2'))
    modular_robot = predefined_assembly.to_pin_robot()  # it's as easy as that

    """Now we have robots - what's next?"""
    # ----- VISUALIZING -----
    # You have seen the visualization of the panda above - let's plot the modular robot
    visualizer = modular_robot.visualize()

    # ----- FORWARD KINEMATICS -----
    # That robot is in a default zero-position. Let's move it.
    q_random = modular_robot.random_configuration()
    print("Will move the robot to the random joint angles:", q_random)

    # The fk method does have some useful options - have a look at the documentation / code to learn more
    tcp_position = modular_robot.fk(configuration=q_random, kind='tcp', visual=True)
    print("The robot's end effector is now placed at the following placement:\n", tcp_position)

    input("\n\nWhen looking at the visualization, the robot is still in its old position. Press enter to update it...")
    visualizer.updatePlacements(pin.VISUAL)  # Visualizer updates have to be performed manually - have a look at it now!

    # ----- INVERSE KINEMATICS -----
    # Want to reach a certain point with the robot? You need to solve the inverse kinematics for that!
    position_desired = np.array([0.5, -0.5, 0.7])
    neutral_rotation = np.eye(3)
    nominal_desired = spatial.homogeneous(translation=position_desired, rotation=neutral_rotation)

    # We only care about position, not the orientation (in this demo case). We need to define a tolerance for the
    #  algorithm to define that:
    from timor.task.Tolerance import CartesianXYZ
    # We use a very small tolerance in xyz-axis directions, but with no constraints on orientation
    ik_tolerance = CartesianXYZ.default()
    desired_pose = ToleratedPose(Transformation(nominal_desired), ik_tolerance)

    # Let's use the panda again
    q_solution, success = panda.ik(
        eef_pose=desired_pose,
        max_iter=1000,
        q_init=panda.q  # We take the current configuration as initial guess for the ik algorithm
    )
    if not success:
        raise ValueError("The demo failed - try again.")

    print("The ik has been solved.")
    input("\n\nPress enter to update the visualization of the panda robot...")
    panda.update_configuration(q_solution)  # This was also called internally by fk before
    panda_viz.updatePlacements(pin.VISUAL)

    # ----- COLLISION CHECKS -----
    # We can also check if the robot is in (self) collision
    panda.has_self_collision()
    panda.has_self_collision(np.ones((panda.dof, 1)))  # Or if a certain configuration leads to collision

    # ----- USEFUL ATTRIBUTES -----
    # Here's some more robot attributes:
    q = panda.random_configuration()
    velocity = np.random.random(q.size)
    acceleration = np.zeros(q.size)
    torques = panda.id(q, velocity, acceleration)  # Joint torques, calculated using recursive newton euler
    ddq = panda.fd(torques, q, velocity)  # noqa: F841 (ignore this) # Acceleration resulting from some applied torques
    print(f"The panda robot has {panda.dof} joints.")
    print(f"The panda robot has a mass of {panda.mass} kg.")
    print(f"The panda robot base is placed at \n{panda.placement}.")

    input("\n\nPress Enter to end the demo...")


if __name__ == '__main__':
    warnings.filterwarnings("ignore")  # This is a demo, we don't care about warnings
    logging.setLevel('ERROR')
    main()
