#!/usr/bin/env python3
# Author: Jonathan Külz
# Date: 03.03.22
import pinocchio as pin

from timor import Module


def main():
    """Run demo creating a random robco robot."""
    db = Module.ModulesDB.from_name('modrob-gen2')
    base = db.by_id['105']
    assembly = Module.ModuleAssembly(db, [base.id])
    another_assembly = Module.ModuleAssembly(db, [base.id])
    def dead_end_filter(module): return module not in db.bases and module not in db.end_effectors
    def joint_filter(module): return dead_end_filter(module) and (len(module.joints) > 0)
    for i in range(5):
        module_filter = dead_end_filter if i % 2 == 0 else joint_filter
        assembly.add_random_from_db(module_filter)
        another_assembly.add_random_from_db(module_filter)

    robot = assembly.to_pin_robot()
    conf = pin.randomConfiguration(robot.model)
    print('Robot in random configuration:')
    robot.update_configuration(conf)
    robot.visualize(coordinate_systems='joints')

    print('Another random robot in a random configuration:')
    another_robot = another_assembly.to_pin_robot()
    another_robot.update_configuration(pin.randomConfiguration(another_robot.model))
    another_robot.visualize(coordinate_systems='joints')

    print('Robot in random configuration with coordinate system viz:')
    robot.visualize(coordinate_systems='joints')
    print('Robot in random configuration with verbose coordinate system viz:')
    robot.visualize(coordinate_systems='full')
    robot.visualize_self_collisions()  # Will only be different if there are collisions

    input('Press ENTER to continue...')


if __name__ == '__main__':
    main()
