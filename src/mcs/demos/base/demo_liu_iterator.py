#!/usr/bin/env python3
# Author: Jonathan Külz
# Date: 22.08.22
import itertools
import time

from timor.configuration_search.LiuIterator import Science2019
from timor.Module import ModulesDB
from timor.utilities import logging


def main(early_stop=1000):
    """
    Showcases the LiuIterator adapted from the Science Robotics paper and times model generation.

    :param early_stop: How many samples to test for performance calculation (None = all)
    """
    schunk_db = ModulesDB.from_name('IMPROV')
    not_in_paper = ('L7', 'L10', 'PB23', 'PB24')
    for module_name in not_in_paper:
        schunk_db.remove(schunk_db.by_name[module_name])
    iterator = Science2019(schunk_db, min_dof=6)

    # The possible combination of powerballs is actually hard coded in the paper
    iterator.joint_combinations = (('22', '22', '21'), ('22', '22', '22'))
    real_links = {'4', '5', '14', '15'}  # No extensions

    def paper_compliant_combo(links: tuple[str]) -> bool:
        is_link = tuple(link for link in links if link in real_links)
        if len(is_link) != 1:
            return False
        if len(links) == 3:
            if links[1] not in real_links:
                return False
        return True

    link_combinations_in_paper = tuple(link_combination for link_combination in iterator.link_combinations
                                       if paper_compliant_combo(link_combination))
    iterator.link_combinations = link_combinations_in_paper

    # Make the fixes from above work
    iterator.links_between_joints = iterator._identify_links_between_joints()
    iterator.joint_combinations_for_base_eef = iterator._identify_base_chain_eef()
    iterator._current_joint_combination = iterator.joint_combinations[iterator.state.joints]
    iterator._current_num_joint_combinations = len(iterator.valid_joint_combinations)
    iterator._current_num_joint_modules = len(iterator.joint_combinations[iterator.state.joints])

    if early_stop is not None:
        logging.info(f"Only checking the first {early_stop} elements for assembly speed "
                     f"(please set to None if you want to check all).")
    t0 = time.time()
    for assembly in itertools.islice(iterator, early_stop):
        urdf = assembly.to_urdf()  # noqa: F841
    iterator.reset()
    t1 = time.time()
    for assembly in itertools.islice(iterator, early_stop):
        robot_model = assembly.to_pin_robot()  # noqa: F841
    iterator.reset()
    t2 = time.time()
    logging.info(f"Took {1000 * (t1 - t0) / len(iterator):.2}ms per URDF, {t1 - t0:.2f}s in total.")
    logging.info(f"Took {1000 * (t2 - t1) / len(iterator):.2}ms per robot model, {t2 - t1:.2f}s in total.")


if __name__ == '__main__':
    logging.setLevel(logging.INFO)
    main()
