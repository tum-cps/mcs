#!/usr/bin/env python3
# Author: Jonathan Külz
# Date: 03.02.22


def main():
    """
    MCS demo scripts; base demos show capabilities w/out OMPL installed; ompl demos also show path planning with OMPL.

    This __init__ makes demos importable.
    """
    print(main.__doc__)


if __name__ == '__main__':
    main()
