#!/usr/bin/env python3
# Author: Jonathan Külz
# Date: 04.03.22
# flake8: noqa
from pathlib import Path

import numpy as np
import pinocchio as pin
from timor import Module, ModuleAssembly
from timor.utilities import prebuilt_robots, visualization
from timor.utilities.file_locations import get_module_db_files


def make_random_robot_move(n_joints: int, t_total: int = 20, dt: float = .1) -> None:
    """
    Builds a random robot and moves it for a given amount of time.
    """
    robot = prebuilt_robots.random_assembly(n_joints, *get_module_db_files('modrob-gen2')).to_pin_robot()
    num_configs = 10
    q_reach = list()
    while True:
        q_rand = pin.randomConfiguration(robot.model)
        robot.update_configuration(q_rand)
        if not robot.has_self_collision():
            q_reach.append(q_rand)
        if len(q_reach) >= num_configs:
            break

    solutions = q_reach  # Don't care about collision
    q = np.vstack(solutions)

    total_steps = int(t_total / dt)
    evenly_spaced = np.linspace(q[:-1, :], q[1:, :], num=int(total_steps / (q.shape[0] - 1)))
    steps = np.reshape(evenly_spaced, (-1, 3), order='F')

    visualization.animation(robot, steps, dt)


def get_six_axis_modrob_v2(length: str = 'long') -> ModuleAssembly:
    """
    Creates a six axis assembly from modrob v2 database with various lengths

    * long roughly corresponds to 1m reach
    * medium roughly corresponds to 0.5m reach
    """
    if length == 'long':
        l1 = '24'
        l2 = '25'
    elif length == 'medium':
        l1 = '20'
        l2 = '25'
    else:
        raise ValueError(f"Unknown length: {length}")

    db = Module.ModulesDB.from_name('modrob-gen2')
    assembly = Module.ModuleAssembly(
        database=db,
        assembly_modules=('105', '2', '2', l1, '2', l2, '1', '1', '1', 'GEP2010IL'),
        connections=(
            (0, '105_base', 1, '2_c_in'), (1, '2_c_out', 2, '2_c_in'), (2, '2_c_out', 3, f'{l1}_c_in'),
            (3, f'{l1}_c_out', 4, '2_c_in'), (4, '2_c_out', 5, f'{l2}_c_in'), (5, f'{l2}_c_out', 6, '1_c_in'),
            (6, '1_c_out', 7, '1_c_in'), (7, '1_c_out', 8, '1_c_in'), (8, '1_c_out', 9, 'gripper_in')),
        base_connector=(0, 'base'))
    return assembly
