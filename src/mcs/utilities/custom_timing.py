#!/usr/bin/env python3
# Author: Jonathan Külz
# Date: 03.02.22
import gc
import itertools
from timeit import Timer, default_number

from tqdm import tqdm


class ProgressTimer(Timer):
    """This timer uses tqdm to show the job progress while timing. There might be an influence on the actual timing"""

    def timeit(self, number=default_number):
        """
        Custom Variation of timeit. Timer that shows a progress bar for timing
        """
        # wrap the iterator in tqdm
        it = tqdm(itertools.repeat(None, number), total=number, position=1, leave=False)
        gcold = gc.isenabled()
        gc.disable()
        try:
            timing = self.inner(it, self.timer)
        finally:
            if gcold:
                gc.enable()

        return timing

    def repeat(self, repeat: int = 1, number: int = default_number) -> list[float]:
        """
        Custom Variation of timeit.repeat that shows a 2-level progress bar.

        Running this in an IDE you might want to enable the 'Emulate terminal' function - or else nested
        tqdm is not working.
        """
        r = []
        for _ in tqdm(range(repeat), total=repeat, position=0, leave=False):
            t = self.timeit(number)
            r.append(t)
        return r
