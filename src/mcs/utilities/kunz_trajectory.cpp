// Main file for pybind

#include "kunz_trajectory/Trajectory.h"
#include "kunz_trajectory/Path.h"
#include <math.h>
#include <Eigen/Core>
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>  // Convert numpy <-> Eigen
#include <pybind11/stl.h>  // Convert python list <-> std::list

namespace py = pybind11;

class KunzTrajectoryPy : public Trajectory {
    public:
        KunzTrajectoryPy(std::list<Eigen::VectorXd> waypoints,
                         double max_deviation,
                         Eigen::Ref<Eigen::VectorXd> max_velocity,
                         Eigen::Ref<Eigen::VectorXd> max_acceleration);
        std::map<std::string, Eigen::MatrixXd> sampleTrajectory(double delta_t, double t_start = 0., double t_end = -1.);

    private:
        KunzTrajectoryPy(const Path &path, const Eigen::VectorXd &maxVelocity, const Eigen::VectorXd &maxAcceleration, double timeStep = 0.001);
};

KunzTrajectoryPy::KunzTrajectoryPy(const Path &path, const Eigen::VectorXd &maxVelocity, const Eigen::VectorXd &maxAcceleration, double timeStep) :
    Trajectory(path, maxVelocity, maxAcceleration, timeStep) {}  // Just wrap base class

KunzTrajectoryPy::KunzTrajectoryPy(std::list<Eigen::VectorXd> waypoints,
                 double max_deviation,
                 Eigen::Ref<Eigen::VectorXd> max_velocity,
                 Eigen::Ref<Eigen::VectorXd> max_acceleration) :
    KunzTrajectoryPy(Path(waypoints, max_deviation), max_velocity, max_acceleration) {}

std::map<std::string, Eigen::MatrixXd> KunzTrajectoryPy::sampleTrajectory(double delta_t, double t_start, double t_end) {
    if (t_end == -1.) t_end = getDuration();
    if (t_end < t_start) throw py::value_error("Can only sample non-empty time sets, i.e., t_start < t_end");
    if (delta_t <= 0.) throw py::value_error("Can only sample with finite, positive time step delta_t");
    int result_length = ceil((t_end-t_start) / delta_t);
    // py::print("Sampling",  result_length, " steps.");

    if(isValid())  {
        // Create numpy / eigen arrays
        int dof = getPosition(0).rows();
        Eigen::MatrixXd position(2,2); position.resize(result_length, dof);
        Eigen::MatrixXd velocity(2,2); velocity.resize(result_length, dof);
        Eigen::MatrixXd times(2,2); times.resize(result_length, 1);
        // Iterate and fill arrays with samples
        double t = t_start;
        for (int row = 0; row < result_length; ++row)
        {
            // py::print("Time ", t);
            // py::print(getPosition(t));
            position.row(row) = getPosition(t);
            velocity.row(row) = getVelocity(t);
            times(row) = t;
            t += delta_t;
            t = std::min(t, t_end);  // Make sure to land on end of trajectory even if not mulitple of t_sample
        }
        return std::map<std::string, Eigen::MatrixXd> {{"q", position}, {"dq", velocity}, {"t", times}};
    } else {
        throw py::value_error("Called on invalid kunz trajectory.");
    }
}

PYBIND11_MODULE(kunz_trajectory, m) {
    m.doc() = R"pbdoc(
    Pybind11 of time-optimal trajectory.

    :cite: T. Kunz, M. Stilman, 'Time-Optimal Trajectory Generation for Path Following with Bounded Acceleration and
      Velocity', 2012, http://hdl.handle.net/1853/44347

    )pbdoc";

    py::class_<KunzTrajectoryPy>(m, "KunzTrajectoryPy")
        .def(py::init<std::list<Eigen::VectorXd>, double,
             Eigen::Ref<Eigen::VectorXd>, Eigen::Ref<Eigen::VectorXd>>(), R"pbdoc(
             Constructor a trajectory through a list of path points with maximum deviation and joint-wise velocity and acceleration limits.

             :param waypoints: List of waypoints in N-dim. (joint) space
             :param max_deviation: Maximum deviation from each waypoint
             :param max_velocity: Maximum velocity in each of the N dimensions
             :param max_acceleration: Maximum acceleration in each of the N dimensions
        )pbdoc",
        py::arg("waypoints"), py::arg("max_deviation"), py::arg("max_velocity"), py::arg("max_acceleration"))
        .def("isValid", &KunzTrajectoryPy::isValid, R"pbdoc(
            Check whether trajectory obeying constraints could be created.
        )pbdoc")
        .def("getDuration", &KunzTrajectoryPy::getDuration, R"pbdoc(
            Get duration of whole trajectory in seconds.
        )pbdoc")
        .def("getPosition", &KunzTrajectoryPy::getPosition, R"pbdoc(
            Get position at time within the trajectory.

            :param arg0: time to get position vector at.
        )pbdoc")
        .def("getVelocity", &KunzTrajectoryPy::getVelocity, R"pbdoc(
            Get velocity at time within trajectory.

            :param arg0: time to get velocity vector at.
        )pbdoc")
        .def("sampleTrajectory", &KunzTrajectoryPy::sampleTrajectory, R"pbdoc(
            Get samples from trajectory with distance delta_t from t_start to t_end.

            :param delta_t: time delta between samples.
            :param t_start: time to start sampling trajectory at.
            :param t_end: time to stop sampling at (-1 = end of trajectory).
        )pbdoc",
        py::arg("delta_t"), py::arg("t_start") = 0., py::arg("t_end") = -1.);

    m.attr("__version__") = "dev";
}
