from itertools import compress
from typing import Optional, Sequence

import numpy as np
from scipy.optimize import minimize

from timor.utilities import logging
from timor.utilities.trajectory import Trajectory

from mcs.utilities import kunz_trajectory


class TrajectoryGenerationError(ValueError):
    """Raised when a trajectory could not be generated."""


def from_kunz_trajectory(via_points: Sequence[np.ndarray],
                         dt: float, v_max: np.ndarray,
                         a_max: np.ndarray,
                         max_deviation: float = 0.01,
                         max_via_points_deviation: Optional[float] = None,
                         remove_duplicates: bool = True,
                         fix_via_point: str = 'replace') -> Trajectory:
    """
    Generate time optimal trajectory according to Kunz et al. with limited velocity and acceleration.

    :param via_points: Via points to connect with PTP trajectory. Start at first row, stop at last, pass at most
      max_deviation from each intermediate points in L2 distance.
    :param dt: sample time in seconds
    :param v_max: Maximum velocity per joint; should be 0 < v_max < inf.
    :param a_max: Maximum acceleration per joint;  should be 0 < a_max < inf.
    :param max_deviation: Maximum L2 distance between intermediate points and the final trajectory in trajectory's
      space (usually joint space if via points are in joint space).
    :param max_via_points_deviation: Maximum L2 distance between via_points and the final trajectory in trajectory's
      space (usually joint space if via points are in joint space).
    :param remove_duplicates: If true, consecutive waypoints that are basically equal will be removed.
    :param fix_via_point: How to handle via points that are not on the trajectory. Can be 'replace' to replace the
      closest or 'insert' to insert an additional sample in the trajectory.

    :return: A trajectory instance according to the cited algorithm.
    :raise ValueError: If the trajectory could not be generated.
    :note: Needs libeigen3-dev installed; expected at /usr/include/eigen3
    :cite: T. Kunz, M. Stilman, "Time-Optimal Trajectory Generation for Path Following with Bounded Acceleration and
      Velocity", 2012, http://hdl.handle.net/1853/44347
    """
    if isinstance(via_points, np.ndarray):
        via_points = list(via_points[i, :] for i in range(via_points.shape[0]))

    if fix_via_point not in {'replace', 'insert'}:
        raise ValueError(f"Unknown fix_via_point option {fix_via_point}.")

    if a_max.shape != v_max.shape or a_max.ndim != 1 or v_max.ndim != 1 or \
            any(p.shape != a_max.shape for p in via_points):
        raise ValueError("Inconsistent via points, velocities, accelerations.")

    if np.isinf(v_max).any() or np.isinf(a_max).any():
        raise ValueError("Kunz trajectory generation does not support infinite velocity or acceleration limits.")
    if np.isnan(v_max).any() or np.isnan(a_max).any():
        raise ValueError("Kunz trajectory generation does not support nan velocity or acceleration limits.")
    if np.any(v_max <= 0) or np.any(a_max <= 0):
        raise ValueError(
            "Kunz trajectory generation does not support negative or zero velocity or acceleration limits.")

    if remove_duplicates:
        deltas = np.diff(via_points, axis=0)
        mask = np.linalg.norm(deltas, axis=1) > 1e-8
        if not all(mask):
            logging.warning("Found duplicate via points that will be removed. Consider creating trajectory manually if"
                            " you need trajectory via points closer than 1e-8 in Euclidean distance.")
        via_points = list(compress(via_points, [True, *mask]))

    if len(via_points) == 1:
        return Trajectory(np.asarray((0.,)), np.asarray(via_points))

    # Catch seldom bug if via_points describe 180° turn by testing if scalar product of consecutive directions is -1
    # https://github.com/tobiaskunz/trajectories/issues/4
    deltas = np.diff(via_points, axis=0)
    directions = deltas / np.linalg.norm(deltas, axis=1, keepdims=True)
    if np.any(np.sum(directions[:-1] * directions[1:], axis=1) < -0.999):
        # Solve by creating trajectories before and after turn separately and concatenating them
        idx_turn = np.argwhere(np.sum(directions[:-1] * directions[1:], axis=1) < -0.999)[0] + 1
        idx_turn = [0, *idx_turn, len(via_points) - 1]
        ret = from_kunz_trajectory(via_points[0:2], dt, v_max, a_max, max_deviation, max_via_points_deviation,
                                   remove_duplicates, fix_via_point)
        for start, end in zip(idx_turn[1:-1], idx_turn[2:]):
            ret += from_kunz_trajectory(via_points[start:end + 1], dt, v_max, a_max, max_deviation,
                                        max_via_points_deviation, remove_duplicates, fix_via_point)
        return ret

    # Create trajectory and sample it
    trajectory = kunz_trajectory.KunzTrajectoryPy(via_points, max_deviation, v_max, a_max)
    if not trajectory.isValid():
        raise TrajectoryGenerationError("Could not generate trajectory.")
    if np.isinf(trajectory.getDuration()):  # Would result in Segmentation Fault in next step
        raise TrajectoryGenerationError("Could not generate trajectory. Infinite length.")

    traj = trajectory.sampleTrajectory(dt)
    traj['t'] = traj['t'].squeeze(axis=1)

    if max_via_points_deviation is not None:
        t_idx = 0
        for via_point in via_points:
            # Find closest point on trajectory's q after time index of last via point (t_idx)
            dist = np.linalg.norm(traj['q'] - via_point, axis=1)
            t_idx = min(t_idx, len(dist) - 1)  # Make sure we still have a valid index left at end
            t_idx = np.argmin(dist[t_idx:]) + t_idx
            if dist[t_idx] < max_via_points_deviation + 1e-10:
                continue
            # Refine trajectory around this point
            min_res = minimize(lambda t: np.linalg.norm(trajectory.getPosition(t) - via_point), traj['t'][t_idx],
                               bounds=((traj['t'][max(0, t_idx - 1)],
                                        max(traj['t'][min(t_idx + 1, len(traj['t']) - 1)], trajectory.getDuration())),))
            if not min_res.success:  # Not yet reason to abandon but minimize seems to have major problem
                logging.warning("Could not find time where trajectory is closest to via point.")
                logging.debug(f"Minimization result struct: {min_res}")
            t_best = min_res.x[0]
            q_best = trajectory.getPosition(t_best)
            # If closest still worse raise error; give some additional tolerance well below the tolerated deviation
            if np.linalg.norm(q_best - via_point) > max_via_points_deviation + max_deviation / 100:
                raise TrajectoryGenerationError("Could not find trajectory close enough to via point.")
            # Add via point to trajectory at best time according to finer sampled trajectory
            if fix_via_point == 'replace':
                traj['t'][t_idx] = t_best
                traj['q'][t_idx, :] = q_best
                traj['dq'][t_idx, :] = trajectory.getVelocity(t_best)
            elif fix_via_point == 'insert':
                if t_best > traj['t'][t_idx]:
                    t_idx += 1
                traj['t'] = np.insert(traj['t'], t_idx, t_best)
                traj['q'] = np.insert(traj['q'], t_idx, q_best, axis=0)
                traj['dq'] = np.insert(traj['dq'], t_idx, trajectory.getVelocity(t_best), axis=0)
            else:
                raise ValueError(f"Unknown fix_via_point option {fix_via_point}.")
            t_idx += 1  # Created specific sample; make sure not overwritten

    if len(traj['t']) > 1:
        traj['ddq'] = np.gradient(traj['dq'], traj['t'], axis=0)
    else:
        traj['ddq'] = np.zeros_like(traj['dq'])
    traj['ddq'] = np.maximum(np.minimum(traj['ddq'], a_max), -a_max)
    traj['dq'] = np.maximum(np.minimum(traj['dq'], v_max), -v_max)
    return Trajectory(**traj)
