from copy import copy
from time import time
from typing import Dict, List, Optional

import numpy as np
import scipy.cluster.vq

from timor import RobotBase
from timor.task import Goals
from timor.utilities import dtypes, logging


def create_ik_candidates_with_timeout(robot: RobotBase,
                                      goals: Dict[str, Goals.At],
                                      timeout: float,
                                      *,
                                      minimum_search_time: Optional[float] = None,
                                      return_partial: bool = False,
                                      min_ik_count: int = 1,
                                      max_ik_count: int = 5,
                                      rng: Optional[np.random.Generator] = None,
                                      ik_kwargs: Optional[Dict] = None,
                                      ) -> Dict[str, List[np.ndarray]]:
    """
    Create >= 1 IK candidates for all goals within a maximum of timeout seconds

    Spends at least minimum_search_time seconds on finding IK candidates for all given goals.
    Spends up to timeout seconds on finding IK candidates if one or more goals could not at least find min_ik_count.
    If more than max_ik_count IK candidates are found for a goal, they are clustered via k-means and one candidate per
    cluster is returned.

    :param robot: Robot to find IK candidates for
    :param goals: Dictionary of goal_id to goal to solve ik for
    :param timeout: Maximum time to spend on finding IK candidates (in seconds)
    :param minimum_search_time: Minimum time to spend on finding IK candidates (in seconds); defaults to 0.
      Set > 0. to force some variety in IK candidates esp. if they can be found very quickly this allows to explore more
      of them and return a more diverse set of IK candidates with little extra time (i.e. a few 100 ms).
    :param ik_kwargs: kwargs for robot.ik s.a. max_iter, check_static_torques, task, ik_method
    :param return_partial: If True, return IK candidates for goals that could be solved for within timeout;
      alternatively raise TimeoutError
    :param min_ik_count: Minimum number of IK candidates to find for each goal before returning; can lead to runtime
      >> minimum_search_time
    :param max_ik_count: Maximum number of IK candidates to find for each goal before returning; if more are found in
      time we try to return candidates with maximized L2 distance via k-means clustering
    :param rng: Random number generator to use
    :return: Dictionary of goal_id to list of IK candidates
    :raise TimeoutError: If no IK candidates could be found for at least one goal and return_partial is False
    """
    t0 = time()
    rng = np.random.default_rng() if rng is None else rng
    if minimum_search_time is None:
        minimum_search_time = 0.
    ik_kwargs = copy(ik_kwargs) if ik_kwargs is not None else {}
    ik_kwargs.setdefault('allow_random_restart', False)  # Such that converges in ~10ms and adheres to q_init
    ik_candidates = {
        goal_id: [] for goal_id in goals.keys()
    }

    # Use at least minimum_search_time to generate IK candidates
    while time() - t0 < minimum_search_time or any(len(ik) < min_ik_count for ik in ik_candidates.values()):
        q_init = robot.random_configuration()
        for g_id, g in goals.items():
            success = False
            try:
                with dtypes.timeout(1, None):  # Fallback; usually expect IK in ~10 ms
                    ik, success = robot.ik(g.goal_pose, q_init=q_init, **ik_kwargs)
            except TimeoutError:
                logging.warning("Timeout while finding IK candidates in create_ik_candidates_with_timeout; "
                                "this should not happen. Reconsider ik_kwargs.")
            if success:
                ik_candidates[g_id].append(ik)
            if time() - t0 > timeout:  # Handle hard timeout of for over goals
                break
        if time() - t0 > timeout:  # Handle hard timeout of while
            if return_partial:
                break  # Can still salvage some IK candidates
            raise TimeoutError(
                f"Could not find enough IKs: { {g_id: len(iks) for g_id, iks in ik_candidates.items()} }.")

    # Try to get to <= max_ik_count by fitting kNN and returning any candidate for each cluster; ~1 ms runtime
    ret = {}
    for goal_id, ik_candidate_list in ik_candidates.items():
        logging.debug(f"Found {len(ik_candidate_list)} IK candidates for goal {goal_id}")
        logging.debug(f"Solutions: {np.asarray(ik_candidate_list)}")
        logging.debug(f"Solutions mod 2 pi: {np.mod(ik_candidate_list, 2 * np.pi)}")
        if len(ik_candidate_list) <= max_ik_count:
            ret[goal_id] = ik_candidate_list
            continue
        try:
            _, labels = scipy.cluster.vq.kmeans2(
                np.mod(np.asarray(ik_candidate_list) + np.pi, 2 * np.pi) - np.pi, max_ik_count, seed=rng)
            ret[goal_id] = [rng.choice(np.asarray(ik_candidate_list)[labels == i]) for i in np.unique(labels)]
        except np.linalg.LinAlgError:
            logging.debug("K Means failed, returning random candidates")
            ret[goal_id] = rng.choice(ik_candidate_list, max_ik_count, replace=False)
        logging.debug(f"Reduced IK list for {goal_id} to {len(ret[goal_id])} candidates")
        logging.debug(f"Solutions mod 2 pi: {np.mod(ret[goal_id], 2 * np.pi)}")
    return ret
