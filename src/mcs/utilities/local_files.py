#!/usr/bin/env python3
# Author: Jonathan Külz
# Date: 29.03.22
from pathlib import Path

__utilities = Path(__file__).parent.absolute()
package = __utilities.parent  # Main directory of the package
src = package.parent
head = src.parent  # Main directory of the project
# Example tasks, solutions, assets for tests - can be shared with timor
environments = package / 'environments'  # Gym environment base path
