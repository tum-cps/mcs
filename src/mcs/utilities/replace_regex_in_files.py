#!/usr/bin/env python3
# Author: Jonathan Külz
# Date: 20.07.22
from argparse import ArgumentParser
from pathlib import Path
import re

parser = ArgumentParser(description="Replace regex in files")
parser.add_argument('-r', '--regex', help="Regex to replace", required=True)
parser.add_argument('-s', '--string', help="String to replace with", required=True)
parser.add_argument('-d', '--dir', help="Top level directory to start parsing", required=True)
parser.add_argument('-p', '--file_pattern', help="Selection pattern for files", default='*')
parser.add_argument('--dry', help="Dry run", action='store_true')


def highlight(match: re.Match) -> str:
    """Highlights a matched pattern with ANSI escape codes."""
    return f"\033[1m{match.group(0)}\033[0m"


def main():
    """Replaces all occurrences / matches of a regex in a file."""
    args = parser.parse_args()
    regex = re.compile(args.regex)
    line_pattern = re.compile(f'.*{args.regex}.*')
    replacement = args.string
    top_level = Path(args.dir)
    dry = args.dry
    for path in top_level.rglob(args.file_pattern):
        if path.is_file():
            with path.open('r') as f:
                content = f.read()

            # Highlight matches and show how they would be changed
            matching_lines = line_pattern.findall(content)
            if len(matching_lines) == 0:
                continue
            print(f"{path} would be changed - found {len(matching_lines)} matches:")
            for line in matching_lines:
                print(re.sub(regex.pattern, highlight, line, flags=re.I).lstrip(), end='\t\t-->\t')  # Highlight old one
                print(regex.sub(replacement, re.sub(regex.pattern, highlight, line, flags=re.I)).lstrip())  # show new

            if not dry:
                new_content = regex.sub(replacement, content)
                if new_content != content:
                    print(f"{path} changed", end='\n\n---\n')
                    with path.open('w') as f:
                        f.write(new_content)


if __name__ == '__main__':
    main()
