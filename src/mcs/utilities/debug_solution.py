from timor.task import Constraints


def debug_solution(solution):
    """Prints debug information about a solution, e.g., which constraints fail and when."""
    if not solution.valid:
        print(f"No valid solution for task {solution.task.id} found.")
        for g in solution.task.goals:
            if not g.achieved(solution):
                print(f"Goal {g.id} fulfilled: {g.achieved(solution)}")
        for c in solution.task.constraints:
            if not c.fulfilled(solution):
                print(f"{type(c)} fulfilled: {c.fulfilled(solution)}")
                if not isinstance(c, (Constraints.JointLimits, Constraints.CollisionFree)):
                    continue
                ts = []
                for t in solution.time_steps:
                    if not c.is_valid_at(solution, t):
                        ts.append(t)
                print(f"Fails at t = {ts}")
                if isinstance(c, Constraints.JointLimits):
                    print(f"Limits: q = {solution.robot.joint_limits}, "
                          f"dq = {solution.robot.joint_velocity_limits}, "
                          f"tau = {solution.robot.joint_torque_limits}")
                    for t in ts:
                        print(f"t={t}: q={solution.q[solution.get_time_id(t)]}, "
                              f"dq={solution.dq[solution.get_time_id(t)]}, "
                              f"ddq={solution.ddq[solution.get_time_id(t)]}, "
                              f"tau={solution.torques[solution.get_time_id(t)]}")
