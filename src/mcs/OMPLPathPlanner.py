from __future__ import annotations

from multiprocessing import Manager, Process, Queue
import queue
import random

import time
from typing import Callable, List, Optional, Sequence, Tuple

import meshcat.geometry
import numpy as np
import ompl.base
import ompl.geometric
from pinocchio.visualize import MeshcatVisualizer

from mcs.PathPlanner import GoalStateInvalidError, PathPlanningFailedException, PathPlannerBase, StartStateInvalidError
from timor import RobotBase, Transformation
from timor.task import Task
from timor.utilities import logging
from timor.utilities.tolerated_pose import ToleratedPose
from timor.utilities.trajectory import Trajectory
from timor.utilities.visualization import place_arrow, place_sphere


class OmplPlanner(PathPlannerBase):
    """A planner that is based on functionalities provided by the open motion planning library, OMPL"""

    dof: int  # The degrees of freedom of the robot == the dimensionality of the state space
    resolution: float  # The lower, the more granular collision checks are going to be performed
    setup: Optional[ompl.geometric.SimpleSetup]
    simplify_path: bool  # If true, tries to smoothen every trajectory found
    state_space: Optional[ompl.base.StateSpace]  # The search space for the planner

    def __init__(self,
                 robot: RobotBase,
                 task: Task = None,
                 resolution: float = 0.01,
                 simplify_path: bool = True,
                 time_limit: float = float('inf'),
                 safety_margin: float = 0.01):
        """
        Initialize the planner, using additional attributes needed by all OMPL based planners

        :param resolution: A float between 0 and 1, describing the resolution that's gonna be used for collision checks,
          where 1 means resolution=state space and 0 means infinite resolution.
        :param simplify_path: A boolean indicator whether to try path smoothing
        :param safety_margin: The safety margin is a padding added to all obstacles to increase needed separation
          during collision check with the robot. In the same unit as the loaded geometries (default meter).
        """
        super().__init__(robot, task, time_limit)
        self.resolution = resolution
        self.simplify_path = simplify_path
        self.safety_margin = safety_margin
        self._setup_state_space(robot)

    def find_path(self, start_state: any, goal_state: any, *args, **kwargs) -> Trajectory:
        """Includes OMPL-specific setup and cleanup"""
        self._adapt_state_to(start_state, goal_state, *args, **kwargs)
        try:
            trajectory = self._find_path(start_state, goal_state, *args, **kwargs)
        finally:
            self._release()
        return trajectory

    def _find_path(self, start_state: np.ndarray, goal_state: np.ndarray, *args, **kwargs) -> Trajectory:
        """
        Find a path from start to goal state, where a state is a joint configuration of self.robot

        :param start_state: The joint configuration the robot starts in
        :param goal_state: The joint configuration that is desired
        :return: An array of configurations that, followed one after the other, leads from q_start to q_goal
        :raise PathPlannerError: if path cannot be found
        """
        t0 = time.time()
        success = self.setup.solve(self.time_limit)

        if success and self.setup.haveExactSolutionPath():
            if self.simplify_path:
                # Take what's left of the time limit at most
                self.setup.simplifySolution(self.time_limit - (time.time() - t0))
            path = self.setup.getSolutionPath()
            path_states = path.getStates()
            q = np.vstack(tuple(self._state_to_numpy(s) for s in path_states))
            return Trajectory(t=1., q=q)

        elif success.getStatus() == success.TIMEOUT:
            raise TimeoutError("Path could not be found within the desired time.")
        elif success.getStatus() == success.APPROXIMATE_SOLUTION:
            approximate_path = self.setup.getSolutionPath()
            if (logging.getEffectiveLevel() <= logging.DEBUG
                    and approximate_path is not None
                    and not isinstance(goal_state, ToleratedPose)):
                q_end = self._state_to_numpy(approximate_path.getState(approximate_path.getStateCount() - 1))
                logging.debug(f"Final state of path: {q_end} vs desired {goal_state};"
                              f" L2-error: {np.linalg.norm(q_end - goal_state)}")
            raise PathPlanningFailedException("Could only find partial, approximate solution")

        raise PathPlanningFailedException("The OMPL path planner failed to find a valid path.")

    def _adapt_state_to(self, start_state: np.ndarray, goal_state: np.ndarray, *args, **kwargs):
        """Sets up the ompl state for the start and goal state"""
        if self.setup.getLastPlannerStatus().getStatus() != ompl.base.PlannerStatus.UNKNOWN:
            logging.debug("Clearing previous planner data")
            self.setup.clear()
        self._set_start(start_state)
        self._set_goal(goal_state)

    def _is_valid_state(self, state: ompl.base.State) -> bool:
        """
        A custom check if the state is without collision and within the joint limits
        """
        q = self._state_to_numpy(state)
        if not self.robot.q_in_joint_limits(q):
            return False
        self.robot.update_configuration(q)
        if self.task is None:
            return not self.robot.has_self_collision()
        else:
            return not self.robot.has_collisions(self.task, self.safety_margin)

    def _setup_state_space(self, robot: RobotBase):
        """
        Creates the search space, given a robot.

        :param robot: A robot object
        """
        self.dof = robot.dof
        self.state_space = ompl.base.RealVectorStateSpace(self.dof)
        self.state_space_bounds = ompl.base.RealVectorBounds(self.dof)
        for dim in range(self.dof):
            self.state_space_bounds.setLow(dim, robot.joint_limits[0, dim])
            self.state_space_bounds.setHigh(dim, robot.joint_limits[1, dim])
        self.state_space.setBounds(self.state_space_bounds)

        if hasattr(self, 'setup'):
            del self.setup  # Explicit deletion to avoid memory leaks
        self.setup = ompl.geometric.SimpleSetup(self.state_space)
        self.setup.setStateValidityChecker(ompl.base.StateValidityCheckerFn(self._is_valid_state))
        ompl.base.SpaceInformation.setStateValidityCheckingResolution(self.setup.getSpaceInformation(), self.resolution)

    def _set_start(self, start_state: np.ndarray):
        """Transforms a np array to a valid ompl state"""
        if not self._is_valid_state(start_state):
            raise StartStateInvalidError(f"Start state is not valid: {start_state}")

        ompl_state = ompl.base.State(self.state_space)
        self._numpy_to_state(start_state, ompl_state)
        self.setup.setStartState(ompl_state)

    def _set_goal(self, goal_state: np.ndarray):
        """Transforms a np array to a valid ompl state"""
        if not self._is_valid_state(goal_state):
            raise GoalStateInvalidError(f"Goal state is not valid: {goal_state}")

        ompl_state = ompl.base.State(self.state_space)
        self._numpy_to_state(goal_state, ompl_state)
        self.setup.setGoalState(ompl_state)

    def _release(self):
        """Destructor, cleanup of c(++) resources; should be called if planner no longer needed"""
        pass

    def _numpy_to_state(self, np_array: np.ndarray, state: ompl.base.State):
        """Copy np array (or convertible) to ompl state"""
        np_array = np.asarray(np_array, dtype=float)
        for dim in range(self.state_space.getDimension()):
            state[dim] = np_array[dim]

    def _state_to_numpy(self, state: ompl.base.State) -> np.ndarray:
        """
        Transforms an ompl planning state (here joint configuration) to a numpy array.

        :param state: Pointer to OMPL state
        :return: Numpy array of joint configuration
        """
        return np.fromiter((state[i] for i in range(self.dof)), dtype=float)

    def visualize(self, viz: MeshcatVisualizer, prefix: str = "PathPlanner"):
        """
        Visualize the planner graph + simplified path in meshcat.

        The planning tree is visualized as the blue graph with blue balls for the vertices ({prefix}/vertices in
        meshcat) and blue lines connecting them ({prefix}/graph_connection). The small blue arrows show edge direction
        pointing from the start state towards the leaves of the tree ({prefix}/direction; this may not be the
        exploration direction, e.g., RRT connect might also create nodes starting from a goal state).
        The red spheres show the via point of the simplified path; the robot will move through them, but they may not
        necessarily coincide with the blue explored state of the planning tree. Shown as "{prefix}/simplified" in
        meshcat namespace.

        :param viz: Meshcat visualizer to show this graph in
        :param prefix: Prefix for the OMPL graph in meshcat
        """
        planner_data = ompl.base.PlannerData(self.setup.getSpaceInformation())
        self.setup.getPlanner().getPlannerData(planner_data)
        if planner_data.numStartVertices() != 1:
            raise ValueError("Cannot visualize planner with non-unique start states")

        candidates = [(None, planner_data.getStartIndex(0))]  # Queue of (parent, vertex) pairs for BFS over search tree
        lines = []

        while len(candidates) > 0:  # BFS over search tree
            parent, vertex = candidates.pop()
            q = self._state_to_numpy(planner_data.getVertex(vertex).getState())
            T = self.robot.fk(q)
            place_sphere(viz, f"{prefix}/vertices/{vertex:03}", 0.025, T,
                         meshcat.geometry.MeshBasicMaterial(color=0x0000ff))
            if parent is not None:
                lines += [parent.translation, T.translation]
            kids = ompl.util.vectorUint()
            count = planner_data.getEdges(vertex, kids)
            for i in range(count):
                candidates.append((T, kids[i]))

        # Draw lines representing the graph edges
        positions = np.asarray(lines)
        line = meshcat.geometry.LineSegments(
            geometry=meshcat.geometry.PointsGeometry(position=positions.T,
                                                     color=np.tile((0, 0, 0.8, 1.), (positions.shape[0], 1)).T),
            material=meshcat.geometry.LineBasicMaterial(vertexColors=True)
        )
        viz.viewer[prefix]["graph_connection"].set_object(line)

        # Draw arrows representing the edge directions
        directions = positions[1::2, :] - positions[0::2, :]
        directions /= np.linalg.norm(directions, axis=1, keepdims=True)
        positions = (positions[0::2, :] + positions[1::2, :]) / 2
        for i, (d, p) in enumerate(zip(directions, positions)):
            if np.inner(d, np.array([0, 0, 1])) < 0.5:
                x = np.cross(d, np.array([0, 0, 1]))
            else:
                x = np.cross(d, np.array([0, 1, 0]))
            x /= np.linalg.norm(x)
            y = np.cross(d, x)
            place_arrow(viz, prefix + f"/direction/{i}",
                        placement=Transformation.from_roto_translation(np.asarray((x, y, d)).T, p),
                        material=meshcat.geometry.MeshBasicMaterial(color=0x0000dd),
                        scale=0.4)

        # Draw simplified path points
        simplified_path = self.setup.getSolutionPath()
        for i in range(simplified_path.getStateCount()):
            state = np.fromiter(
                (simplified_path.getState(i)[j] for j in
                 range(simplified_path.getSpaceInformation().getStateDimension())), dtype=float)
            place_sphere(viz, f"{prefix}/simplified/vertices/{i}", .05, self.robot.fk(state),
                         meshcat.geometry.MeshBasicMaterial(color=0xff0000))


class RrtcPlanner(OmplPlanner):
    """An implementation of the Rapidly-exploring Random Tree-Connect (RRTC) planner"""

    def __init__(self,
                 robot: RobotBase,
                 task: Task = None,
                 resolution: float = 0.01,
                 rrtc_range: float = 0.99,
                 simplify_path: bool = True,
                 time_limit: float = float('inf'),
                 safety_margin: float = 0.01
                 ):
        """
        An OMPL-based planner that uses RRT-Connect to find a path

        :param rrtc_range: Sets the range the planner is supposed to be. This parameter greatly influences the runtime
          of the algorithm. It represents the maximum length of a motion to be added in the tree of motions.
        """
        super().__init__(robot, task, resolution=resolution,
                         simplify_path=simplify_path, time_limit=time_limit,
                         safety_margin=safety_margin)
        self.rrtc_range = rrtc_range
        planner = ompl.geometric.RRTConnect(self.setup.getSpaceInformation())
        planner.setRange(rrtc_range)
        self.setup.setPlanner(planner)


def parallelizable_inverse_kinematics(q_in: Queue,
                                      goal_samples: List[np.array],
                                      ik_solve: Callable[[np.array], Tuple[np.array, bool]],
                                      robot: RobotBase,
                                      is_valid_state: Callable[[Sequence], bool],
                                      state_difference_threshold: float = 0.2,
                                      max_number_samples: int = 100):
    """
    Task that takes in q's or generates them randomly and adds valid ik solutions to goal_samples

    :param q_in: multiprocessing queue with joint angles to check for ik solution
    :param goal_samples: multiprocessing list with all ik solutions (within joint limits)
    :param ik_solve: takes in q_init and q_sol and returns status code (0 <-> ik successful)
    :param robot: robot to solve ik with
    :param is_valid_state: maps joint state q to true if valid, ie not colliding
    :param state_difference_threshold: minimum euclidean distance to any previously found ik solution to be
                                       considered a new solution
    :param max_number_samples: Maximum random samples to generate at full speed (afterwards only 1 per sec)
    """
    # For asynchronous debugging
    # import pydevd_pycharm
    # pydevd_pycharm.settrace('172.17.0.1', port=2222, stdoutToServer=True, stderrToServer=True)

    while True:
        try:
            q = q_in.get(False)
            logging.debug("Process external q_init")
        except queue.Empty:
            if max_number_samples > 0:
                q = robot.random_configuration()
                max_number_samples -= 1
                logging.debug(f"Process internal q_init {q}")
            else:
                logging.debug("No more random samples")
                q = q_in.get(block=True)

        q, valid = ik_solve(q)

        if valid:  # IK success
            # Sort out ik solutions that are outside limits or collide
            if not is_valid_state(q):
                continue
            if all(np.linalg.norm(i - q) > state_difference_threshold for i in goal_samples):
                # only remember solution if sufficiently different
                logging.debug("New IK sol: " + str(q))
                goal_samples.append(q)


class IKPlanner(RrtcPlanner):
    """Solves path planning for known robot_state (q) to desired end-effector placement"""

    goal: Optional[PoseGoal]

    def find_path(self, start_state: np.ndarray, goal_state: ToleratedPose, *args, **kwargs) -> Trajectory:
        """
        The IK Planner solve is different to other ompl planners as it expects a placement as goal.

        The start state continues to be a robot configuration.

        :param start_state: A robot configuration q.
        :param goal_state: A goal pose (4x4) for the robot to reach
        :return: A trajectory starting in start_state, leading to a goal state where robot.fk(goal) = goal_state
        """
        return super().find_path(start_state, goal_state, *args, **kwargs)

    def _adapt_state_to(self, start_state: np.ndarray, goal_state: np.ndarray, *args, **kwargs):
        """Differing from its parent class in the way that we define a PoseGoal, not a q GoalState."""
        if self.setup.getLastPlannerStatus().getStatus() != ompl.base.PlannerStatus.UNKNOWN:
            logging.debug("Clearing previous planner data")
            self.setup.clear()
        self._set_start(start_state)
        self.goal = PoseGoal(self.setup.getSpaceInformation(), goal_state, self.robot, self._is_valid_state, self.task)
        self.setup.setGoal(self.goal)
        self.goal.start()

    def _release(self):
        """Destructor, cleanup of c(++) resources; should be called if planner no longer needed"""
        self.goal._release()
        super()._release()


class PoseGoal(ompl.base.GoalSampleableRegion):
    """
    An OMPL conformant goal that is fulfilled if the robot configuration results in a desired end-effector placement.

    To generate goal samples it solves inverse kinematics for a given robot either from random configurations or from
    those previously tested for their distance to the goal pose; the ik is calculated in a seperated process that
    should be terminated by calling the goal's release methode.
    """

    def __init__(self,
                 state_space_info: ompl.base.SpaceInformation,
                 goal_pose: ToleratedPose,
                 robot: RobotBase,
                 is_valid_state: Callable[[np.array], bool],
                 task: Optional[Task] = None):
        """
        Constructor of PoseGoal

        :param state_space_info: OMPL state space info (e.g. from PlannerBase::setup.getSpaceInformation)
        :param goal_pose: Desired pose with tolerances
        :param robot: robot that should reach goalPose; should implement fk, ik
        :param is_valid_state: Function that returns if a robot state / configuration is valid
        :param task: Optional task to check for collisions; if None, only self-collisions are checked
        """
        if not isinstance(goal_pose, ToleratedPose):  # Make sure no deprecated leftovers lead to silent failure
            raise ValueError("Goal must be a placement with tolerances")
        self.goal_pose: ToleratedPose = goal_pose
        super().__init__(state_space_info)
        self.robot = robot
        self.dof = self.robot.dof

        self.weight_rot = 1.75  # s.t. 1° = 0.0175 rad deviation is weighted equal to 1 cm = 0.01 m
        self.setThreshold(0.01)

        self._mp_manager = Manager()
        logging.debug("started mp manager: " + str(self._mp_manager._process.pid))
        self._goalSamples = self._mp_manager.list()
        self._ik_candidates = self._mp_manager.Queue()  # need pickleable representation -> np array...
        self._solve = lambda q_init: self.robot.ik(self.goal_pose, q_init=q_init, task=task)
        self._ik_process = Process(target=parallelizable_inverse_kinematics, args=(self._ik_candidates,
                                                                                   self._goalSamples, self._solve,
                                                                                   self.robot, is_valid_state))

    def start(self):
        """start ik sampling process; e.g. call before sampleGoal"""
        if not self._ik_process.is_alive():
            self._ik_process.start()
        else:
            logging.debug("skiped _ik_process.start() as already running")
        logging.debug("started ik process manager: " + str(self._ik_process.pid))

    def _release(self):
        """Destructor makes sure ik sampler process is killed"""
        self._ik_process.terminate()  # Use manager -> shutdown first
        self._ik_process.join()
        self._mp_manager.shutdown()
        self._mp_manager.join()
        del self.goal_pose
        del self._solve

    def distanceGoal(self, state: ompl.base.State):
        """
        Returns distance between eef at state and desired goal; adds state as initial guess for ik solver.

        Position distance is given in mm, rotational distance based on rodriguez angle

        :param state: State at which the distance is evaluated
        """
        q = np.fromiter(state, dtype=float, count=self.dof)
        fk_result = self.robot.fk(q)
        rot, pos = fk_result[:3, :3], fk_result[:3, 3]
        d_pos = self.goal_pose.nominal.in_world_coordinates()[:3, 3] - pos
        d_rot = self.goal_pose.nominal.in_world_coordinates()[:3, :3].transpose() * rot
        d_rot = abs(np.arccos((np.trace(d_rot) - 1) / 2))  # Rodriguez angle
        dis = max(np.linalg.norm(d_pos), d_rot * self.weight_rot)
        self._ik_candidates.put(q)
        return dis

    def sampleGoal(self, return_goal: ompl.base.State):
        """
        Return a robot configuration that solves the desired placement;

        :param return_goal: Configuration that results in goalPose for given robot or None if no ik solution found
                           so far (check with canSample())
        """
        if self.canSample():
            q = random.choice(self._goalSamples)
            for i in range(len(q)):
                return_goal[i] = q[i]
            logging.debug("PoseGoal: Sampled goal: " + str(q))
        else:
            if not self._ik_process.is_alive():
                logging.warning("Please call start() for PoseGoal object before trying to sample goals")
            return_goal = None

    def maxSampleCount(self):
        """Number of different inverse kinematic solutions found"""
        return len(self._goalSamples)

    def couldSample(self):
        """Always true as rather hard to proof that ik not solvable"""
        return True
