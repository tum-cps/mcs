import abc
from copy import deepcopy, copy
import importlib
import itertools
import math
from time import time
from typing import Any, Dict, List, Optional, Sequence, Tuple, Type, Union
import uuid

import networkx as nx
import numpy as np
from pinocchio.visualize import MeshcatVisualizer

from mcs.utilities.ik_helpers import create_ik_candidates_with_timeout
from mcs.utilities.trajectory import TrajectoryGenerationError, from_kunz_trajectory
from mcs.PathPlanner import PathPlannerBase, PathPlanningFailedException
import timor.configuration_search.AssemblyFilter as rf
from timor import ModuleAssembly, Transformation
from timor.Robot import RobotBase
from timor.task import Constraints, Goals
from timor.task.Task import Task
from timor.utilities.dtypes import Lazy
from timor.utilities.trajectory import Trajectory
import timor.utilities.logging as logging


class TaskSolverBase(abc.ABC):
    """A base class for task solvers - they generate solution trajectories for a assembly working in a task."""

    timeout: float  # Global timeout in seconds, maximum time spent for solving a task

    def __init__(self, task: Task, assembly: Optional[ModuleAssembly] = None, timeout: float = float('inf'),
                 safety_margin: float = 0.01, rng: Optional[np.random.Generator] = None):
        """
        Provide the same timeout interface to every child.

        :param task: A task to find solutions for
        :param assembly: A robot assembly to find solutions with
        :param timeout: Maximum amount of time to be used for a solve_task call
        :param safety_margin: Minimum distance between objects in collision checking
        :param rng: Random number generator to use
        """
        self._task: Task = copy(task)
        self._task.constraints = tuple(
            Constraints.ConstraintBase.from_json_data(c.to_json_data(), frames=self._task.frames)
            for c in self._task.constraints)  # TODO : Remove this once deepcopy(task.constraints) works
        self._assembly: Optional[ModuleAssembly] = assembly
        self._evaluated: bool = False  # Indicator whether current task has been evaluated using current robot
        self.timeout: float = timeout
        # List of planners used in solving the last task for visualization / debugging
        self._all_planners: List[PathPlannerBase] = []
        self._successful_planners: List[PathPlannerBase] = []
        self.safety_margin = safety_margin

        if self._collision_free_constraint is not None:
            # Update safety margin in collision free constraint and own task copy
            self._collision_free_constraint.safety_margin = (
                max(self._collision_free_constraint.safety_margin, safety_margin))

        self._rng = np.random.default_rng() if rng is None else rng

    @property
    def assembly(self) -> Optional[ModuleAssembly]:
        """Keep assembly private such that changing it resets internal state."""
        return self._assembly

    @assembly.setter
    def assembly(self, new_assembly: ModuleAssembly):
        """Reset information about being solved"""
        self._evaluated = False
        self._assembly = new_assembly
        self.reset()

    @property
    def _collision_free_constraint(self) -> Optional[Constraints.CollisionFree]:
        """Getter for the collision free constraint in the task solver's task."""
        collision_free_constraint = [c for c in self.task.constraints if type(c) is Constraints.CollisionFree]
        if len(collision_free_constraint) > 0:
            if len(collision_free_constraint) > 1:
                raise NotImplementedError("Cannot yet handle multiple collision free constraints in task.")
            return collision_free_constraint[0]
        else:
            collision_free_constraint = \
                [c for c in self.task.constraints if isinstance(c, Constraints.SelfCollisionFree)]
            if len(collision_free_constraint) > 0:
                if len(collision_free_constraint) > 1:
                    raise NotImplementedError("Cannot yet handle multiple collision free constraints in task.")
                return collision_free_constraint[0]
        return None

    @property
    def _joint_limit_constraint(self) -> Optional[Constraints.JointLimits]:
        """Getter for the joint limit constraint in the task solver's task."""
        joint_limit_constraint = [c for c in self.task.constraints if isinstance(c, Constraints.JointLimits)]
        if len(joint_limit_constraint) > 0:
            if len(joint_limit_constraint) > 1:
                return Constraints.JointLimits(
                    itertools.chain.from_iterable(c.to_json_data()['parts'] for c in joint_limit_constraint)
                )
            return joint_limit_constraint[0]
        return None

    @property
    def task(self) -> Task:
        """Keep the task private, as it should not be changed."""
        return self._task

    @property
    def robot(self) -> Optional[RobotBase]:
        """Default getter"""
        return self._assembly.robot

    def reset(self):
        """Reset the solver to its initial state. Should be called at each start of solve."""
        self._successful_planners = []
        self._all_planners = []

    def solve(self, assembly: Optional[ModuleAssembly] = None,
              base_placement: Optional[Transformation] = None) -> Optional[Trajectory]:
        """
        Compute a trajectory that solves the task with a given robotassembly if possible.

        :param assembly: If given, sets the assembly of the solver
        :param base_placement: Where to put the base of the assembly
        :return: A trajectory if found or None if not
        :raise ValueError: If solution cannot be found at specific point
        :raise NotImplementedError: If goal type is not yet solvable
        """
        self.reset()

        if assembly is not None:
            self.assembly = assembly
        elif self.assembly is None:
            raise ValueError("There is no assembly assigned to this solver.")

        # TODO : Alternative - assembly keeps track of its base_placement; Advantage: Only need to create robot if
        #  necessary
        if base_placement is not None:
            self.assembly.robot.set_base_placement(base_placement)

        if self._evaluated:
            raise ValueError(f"Task {self.task} has already been evaluated using assembly {self.assembly}.")
        self._set_base()
        solution = self._solve()
        self._evaluated = True
        return solution

    def _set_base(self):
        """Can be overridden by children to set the assembly base before starting to optimize"""
        pass

    @abc.abstractmethod
    def _solve(self, t0: Optional[float] = None) -> Optional[Trajectory]:
        """
        The logic of solve, implemented by children

        :param t0: Start time of the task solving prcocess (defaults to now)
        """

    def visualize(self, viz: MeshcatVisualizer, prefix: str = "TaskSolver", show_all: bool = False):
        """
        Visualize the latest solution generation.

        :param viz: The visualizer to use
        :param prefix: The namespace to use within the visualizer; add level with slash
        :param show_all: If True, show all planners, incl those that failed, otherwise only the successful ones
        """
        for i, planner in enumerate(self._all_planners if show_all else self._successful_planners):
            planner.visualize(viz, prefix + f"/planner_{i}")


class SimpleTaskSolver(TaskSolverBase):
    """
    A basic Planner that should (eventually) be able to solve all goals respecting given constraints.

    Not specialized or optimized
    """

    maximum_speed: float  # For all joints in the trajectory
    trajectory_resolution: float  # Time step size in seconds
    trajectory_acceleration_time: float

    def __init__(self,
                 task: Task,
                 assembly: Optional[ModuleAssembly] = None,
                 trajectory_resolution: float = .01,
                 trajectory_acceleration_lim: float = 1.,
                 timeout: float = float('inf'),
                 time_limit_per_goal: float = 60.,
                 maximum_path_deviation: float = 1e-4,
                 safety_margin: float = 0.01,
                 ik_generation_kwargs: Optional[Dict] = None,
                 dq_checking_resolution: float = 0.01,
                 solve_goals_sequentially: bool = False,
                 rng: Optional[np.random.Generator] = None):
        """The simple task solver is a naive approach for finding trajectories to solve goals

        :param trajectory_resolution: time step size in seconds for the final sampled trajectory
        :param trajectory_acceleration_lim: limit joint acceleration (must be < inf)
        :param timeout: Maximum time for all goals
        :param time_limit_per_goal: time in seconds allowed to solve each goal
        :param maximum_path_deviation: maximum L2 distance in *joint space* between the final trajectory and the precise
          ik solution and linear interpolation of path segments.
        :param safety_margin: Minimum distance between objects in collision checking
        :param ik_generation_kwargs: Configuration for IK generation, s.a., extra timeout, min/max ik count per goal...
          For details see :py:func:`timor.utilities.ik_helpers.create_ik_candidates_with_timeout`
        :param dq_checking_resolution: Resolution for checking if a path is collision free as L2 distance in joint space
        :param solve_goals_sequentially: For solving goal after goal; default is to solve all at/reach goals together
        :param rng: Random number generator to use
        """
        super().__init__(task, assembly, timeout=timeout, safety_margin=safety_margin, rng=rng)
        self.trajectory_resolution = trajectory_resolution
        if np.isscalar(trajectory_acceleration_lim) and trajectory_acceleration_lim == float('inf'):
            raise ValueError("Need finite acceleration limit")
        self._trajectory_acceleration_lim = trajectory_acceleration_lim
        self.time_limit_per_goal = time_limit_per_goal
        self.maximum_path_deviation = maximum_path_deviation
        if ik_generation_kwargs is None:
            ik_generation_kwargs = {"min_ik_count": 1, "max_ik_count": 10, "timeout": timeout,
                                    "minimum_search_time": timeout / 5}
        if 'ik_kwargs' not in ik_generation_kwargs:
            ik_generation_kwargs['ik_kwargs'] = {}
        self.ik_generation_kwargs: Dict[str, Any] = ik_generation_kwargs  # Use via get_current_ik_generation_kwargs
        self.dq_checking_resolution = dq_checking_resolution
        self.solve_goals_sequentially = solve_goals_sequentially

    @property
    def joint_velocity_limits(self) -> np.ndarray:
        """Getter for maximum joint speed"""
        return self.assembly.robot.joint_velocity_limits

    @property
    def trajectory_acceleration_lim(self) -> np.ndarray:
        """Getter for maximum joint acceleration allowed for trajectory generation"""
        maximum_acceleration = self._trajectory_acceleration_lim
        if np.isscalar(maximum_acceleration):
            maximum_acceleration = np.tile(maximum_acceleration, self.joint_velocity_limits.shape)
        return maximum_acceleration

    def get_current_ik_generation_kwargs(self, time_left: float) -> Dict[str, Any]:
        """Update ik_generation_kwargs with current time_left and task."""
        ik_generation_kwargs = deepcopy(self.ik_generation_kwargs)
        ik_generation_kwargs['minimum_search_time'] = (
            min(ik_generation_kwargs.get('minimum_search_time', math.inf), time_left))
        ik_generation_kwargs['timeout'] = min(ik_generation_kwargs.get('timeout', math.inf), time_left)
        ik_generation_kwargs['ik_kwargs'].update({'task': self.task})
        return ik_generation_kwargs

    def _set_base(self):
        """Don't optimize, but just take the center of the possible space the base can be placed into"""
        if isinstance(self.task.base_constraint, Constraints.BasePlacement):  # No optimization at all
            self.robot.set_base_placement(self.task.base_constraint.base_pose.nominal.in_world_coordinates())

    def _solve(self, t0: Optional[float] = None) -> Optional[Trajectory]:
        """
        Solves a task by solving the goals one by one.

        :param t0: Start time of the task solving process (defaults to now)
        :return: Trajectory that solves the task or None if it could not be found
        :raise PathPlanningFailedException: If no path could be found to a goal
        :raise TimeoutError: If the task could not be solved in time
        :raise TrajectoryGenerationError: If the trajectory could not be generated
        """
        if t0 is None:
            t0 = time()
        goal_order: Tuple[str, ...]
        goal_order_constraint = [x for x in self.task.constraints if isinstance(x, Constraints.GoalOrderConstraint)]
        if len(goal_order_constraint) > 1:
            raise ValueError("Found multiple goal orders")
        if goal_order_constraint:
            goal_order = goal_order_constraint[0].order
        else:
            goal_order = tuple(self.task.goals_by_id.keys())

        if any(isinstance(c, Constraints.AllGoalsFulfilled) for c in self.task.constraints):
            additional_goals = set(self.task.goals_by_id.keys()) - set(goal_order)
            if len(additional_goals) > 0:
                logging.warning(
                    f"Found goals outside goal order; adding their solution to the end ({additional_goals}).")
            goal_order = goal_order + tuple(additional_goals)

        q_start = np.expand_dims(self.robot.q, 0)
        q_previous = q_start
        goal_traj = {}

        # We solve all PTP together to reduce the distance between the ik solutions for each goal
        if (not self.solve_goals_sequentially
                and len(self.task.goals) > 0
                and all(isinstance(g, (Goals.Pause, Goals.At)) for g in self.task.goals)):
            logging.debug("Solving all PTP goals jointly, reducing the overall trajectory time")
            at_goals = tuple(g_id for g_id in goal_order if isinstance(self.task.goals_by_id[g_id], Goals.At))
            timeout = t0 + self.timeout - time()
            # Jointly generates trajectory for each connection between reach goals
            goal_traj.update(self._solve_multi_reach(at_goals, timeout=timeout, q_previous=None))
            q_start = goal_traj[at_goals[0]][None, 0]  # Start from the first goal

        for goal_id in goal_order:  # Find places where goals are fulfilled
            if goal_id in goal_traj:
                continue
            goal = self.task.goals_by_id[goal_id]
            if isinstance(goal, Goals.Pause):
                continue  # No pre-solving needed
            if not isinstance(goal, Goals.At):
                raise NotImplementedError(f"Cannot solve goal of type {type(goal)}")
            time_limit_per_goal = min(self.timeout - (time() - t0), self.time_limit_per_goal)
            try:
                q_previous = self._solve_reach_goal(goal_id, q_previous[-1, :], time_limit_per_goal)
            except PathPlanningFailedException as e:
                logging.info(f"Path planner raised {e}")
                raise PathPlanningFailedException(
                    f"Could not find a valid path to goal {goal_id} from {q_previous[-1, :]}.")
            goal_traj[goal_id] = q_previous

        trajectory = Trajectory(t=np.asarray((0, self.trajectory_resolution)), q=np.tile(q_start, (2, 1)))

        for goal_id in goal_order:
            q_previous = np.expand_dims(trajectory.q[-1], 0)
            goal = self.task.goals_by_id[goal_id]
            if isinstance(goal, Goals.Pause):
                trajectory = trajectory + self._solve_pause_goal(goal_id, q_previous)
                continue

            traj = from_kunz_trajectory(via_points=np.vstack((trajectory.q[-1, :], goal_traj[goal_id])),
                                        dt=self.trajectory_resolution,
                                        v_max=self.joint_velocity_limits,
                                        a_max=self.trajectory_acceleration_lim,
                                        max_deviation=self.maximum_path_deviation)
            if isinstance(goal, Goals.At) and not goal.goal_pose.valid(self.robot.fk(traj.q[-1, :])):
                logging.warning(f"Goal pose {goal.goal_pose} not valid for robot@q={traj.q[-1, :]}, "
                                f"fk={self.robot.fk(traj.q[-1, :])}; consider decreasing maximum_path_deviation")

            # Check that torques respected as these only loosely depend on trajectory's v/a_max
            if self._joint_limit_constraint.tau:
                for t, q, dq, ddq in zip(traj.t, traj.q, traj.dq, traj.ddq):
                    if not self._joint_limit_constraint.check_single_state(self.task, self.robot, q, dq, ddq):
                        tau = self.robot.id(q, dq, ddq)
                        raise TrajectoryGenerationError(
                            f"Joint torque limits violated (torque={tau}, time={t}, goal id={goal_id})")

            if type(self.task.goals_by_id[goal_id]) is Goals.Reach:
                traj = traj + Trajectory.stationary(time=0.1, q=traj.q[-1, :], sample_time=self.trajectory_resolution)
            traj.goal2time[goal_id] = traj.t[-1]

            trajectory = trajectory + traj

        return trajectory

    def _solve_pause_goal(self,
                          goal_id: str,
                          q_previous: np.ndarray) -> Trajectory:
        """
        Solves a pause goal by adding a pause of the required length to the current trajectory.

        :param goal_id: ID of the goal to solve
        :param q_previous: Current trajectory
        :return: New trajectory
        """
        goal = self.task.goals_by_id[goal_id]
        if not isinstance(goal, Goals.Pause):
            raise ValueError(f"{type(goal)} is an invalid goal type for solve_pause_goal.")
        pause_duration = goal.duration
        pause_traj = Trajectory.stationary(time=pause_duration, q=q_previous[-1, :],
                                           sample_time=self.trajectory_resolution)
        pause_traj.goal2time[goal_id] = pause_traj.t[-1]
        return pause_traj

    def _solve_multi_reach(self, goal_ids: Sequence[str], timeout: float,
                           q_previous: Optional[np.ndarray] = None) -> Dict[str, np.ndarray]:
        """
        Solve multiple reach goals together and minimizes the total trajectory length in joint space.

            1. Find multiple IK solutions for each goal
            2. Create a graph with all IK solutions as vertices and edge weight = L2 distance between IK solutions of
                consecutive goals.
            3. Test paths of increasing length for collision-free distance between the IK solutions with OMPL planner
            4. Update edge weights with collision free distance and cache planned path. Go back to step 3.
            5. When converging or reaching the time limit, return the shortest collision-free path

        :param goal_ids: IDs of the goals to solve (their order is assumed given)
        :param timeout: Timeout in seconds
        :param q_previous: Optionally, consider the current end of a preceding trajectory to start from.
        :return: Dictionary mapping goal IDs to paths from previous goal to next this goal
        """
        t0 = time()
        graph_start_name = 'graph_start-' + str(uuid.uuid4())
        graph_end_name = 'graph_end-' + str(uuid.uuid4())
        if any(goal_id in {graph_start_name, graph_end_name} for goal_id in goal_ids):
            raise ValueError(f"Cannot use {graph_start_name} or {graph_end_name} as goal IDs with solve_multi_reach.")

        goals = {goal_id: self.task.goals_by_id[goal_id] for goal_id in goal_ids}

        # Find IK solutions for all goals in dict goal_id -> list of IK solutions (np.ndarray)
        ik_generation_kwargs = self.get_current_ik_generation_kwargs(t0 + timeout - time())
        logging.debug(f"Find IK candidates for {goal_ids} with {ik_generation_kwargs}")
        ik_candidates = create_ik_candidates_with_timeout(self.robot, goals, rng=self._rng, **ik_generation_kwargs)
        logging.info("Found {} IK candidates in {} of {} seconds for planning".format(
            {goal_id: len(ik_candidates[goal_id]) for goal_id in goal_ids}, time() - t0, timeout
        ))

        # Build directed graph with
        #   * vertex index (goal_id, ik_candidate_idx) - represents IK candidate for goal; indices into ik_candidates
        #   * edge weight time between two IK candidates approx. by distance in each joint / joint's velocity limit
        #     (connects IK candidates of consecutive goals; optionally contains collision-free 'path' attribute)
        #   * add vertices for start and end
        def _lower_bound_time_between(q1: np.ndarray, q2: np.ndarray) -> float:
            """Constant velocity, infinite acceleration estimate of time between two configurations."""
            return np.max(np.abs(q1 - q2) / self.robot.joint_velocity_limits)

        search_graph = nx.DiGraph()
        if q_previous is not None:
            search_graph.add_node((graph_start_name, 0), ik_candidate=q_previous)
        for goal_id in goal_ids:
            for idx, ik_candidate in enumerate(ik_candidates[goal_id]):
                search_graph.add_node((goal_id, idx), ik_candidate=ik_candidate)
                # Create edges from q_previous to all IK candidates of the first goal
                if goal_id == goal_ids[0]:
                    if q_previous is None:
                        search_graph.add_edge((graph_start_name, 0), (goal_id, idx),
                                              weight=0, path=np.asarray([ik_candidate, ]))
                    else:
                        search_graph.add_edge((graph_start_name, 0), (goal_id, idx),
                                              weight=_lower_bound_time_between(q_previous, ik_candidate))
                # Create edges from all IK candidates of the last goal to graph_end
                elif goal_id == goal_ids[-1]:
                    search_graph.add_edge((goal_id, idx), (graph_end_name, 0), weight=0, path=[])

        # Create edges between all IK candidates of consecutive goals
        for goal_start_idx, goal_end_idx in zip(goal_ids[:-1], goal_ids[1:]):
            for ik_start_idx, ik_candidate_start in enumerate(ik_candidates[goal_start_idx]):
                for ik_end_idx, ik_candidate_end in enumerate(ik_candidates[goal_end_idx]):
                    search_graph.add_edge((goal_start_idx, ik_start_idx), (goal_end_idx, ik_end_idx),
                                          weight=_lower_bound_time_between(ik_candidate_start, ik_candidate_end))

        # Find all possible paths through single ik_solution for each goal ordered by length
        # Each path is [(goal_start_name, 0), (goal_id[1], ik_candidate_idx), ...]
        paths = nx.shortest_simple_paths(search_graph, (graph_start_name, 0), (graph_end_name, 0), weight='weight')
        return_paths = 0
        handled_paths = set()

        def _lower_bound_path_time(path_to_measure: np.array) -> float:
            """
            Return a constant velocity, infinite acceleration lower bound for the time needed to traverse a path.

            :param path_to_measure: np.array of shape (n_steps, n_joints)
            :return: Lower bound for time needed to traverse path with constant velocity, infinite acceleration
            """
            return np.sum(np.max(np.abs(np.diff(path_to_measure, axis=0)) / self.robot.joint_velocity_limits, axis=1))

        def _handle_no_connecting_path(start_idx, end_idx):
            """Remove edge from start to end if no path could be found + debug log"""
            logging.debug(f"Start: {search_graph.nodes[start_idx]['ik_candidate']}")
            logging.debug(f"End: {search_graph.nodes[end_idx]['ik_candidate']}")
            search_graph.remove_edge(start_idx, end_idx)

        while time() - t0 < timeout:
            try:
                path = next(paths)  # Use while / next to be able to adapt paths during iterations to updated weights
            except (StopIteration, nx.NetworkXNoPath):
                if return_paths == 0:
                    raise PathPlanningFailedException("No suggested combination of ik could be turned into valid path.")
                break  # Have at least one valid return path; continue finding shortest
            # Time s. t. >= two paths can still be planned
            time_per_step = (t0 + timeout - time()) / len(path) / 2

            # Cut off start/end node; add first goal as single step if no connection to previous trajectory
            if q_previous is None:
                logging.debug("No previous q; don't plan for first path step")
                path = path[1:]
            path = path[:-1]

            if tuple(path) in handled_paths:  # Shortcut to not check the same path twice
                continue

            # Create trajectory for this path of ik_candidates
            for start_idx, end_idx in zip(path[:-1], path[1:]):
                # Shortcut already known sub-paths
                logging.debug(f"Check path from {start_idx} to {end_idx}")
                if 'path' in search_graph.edges[(start_idx, end_idx)]:
                    if search_graph.edges[(start_idx, end_idx)]['path'] is None:
                        logging.warning(f"Path segment from {start_idx} to {end_idx} already known to be invalid - "
                                        f"this should not happen anymore")
                        break  # Path is not possible - ignore this combination of IK candidates
                    logging.debug(f"Path segment from {start_idx} to {end_idx} already known - skip segment")
                    continue
                # Create new sub-path
                try:
                    search_graph.edges[(start_idx, end_idx)]['path'] = self._solve_reach_goal(
                        end_idx[0],  # which goal to solve
                        search_graph.nodes[start_idx]['ik_candidate'],  # where to connect from (prev. ik)
                        q_suggestion=search_graph.nodes[end_idx]['ik_candidate'],  # fixed ik candidate for goal sol.
                        time_limit=time_per_step)
                except PathPlanningFailedException:
                    logging.debug(f"Path not found from {start_idx} to {end_idx} - skip")
                    _handle_no_connecting_path(start_idx, end_idx)
                    break  # This combination of iks does not seem to work - try another one
                except TimeoutError:
                    logging.debug(f"OMPL/single reach solution timed out from {start_idx} to {end_idx} - skip")
                    _handle_no_connecting_path(start_idx, end_idx)
                    break  # This combination of iks does not seem to work - try another one
                # Update length based on actual path executed with constant speed
                # Complete trajectory parameterization, e.g., with kunz_trajectory would take 20x longer and be similar
                logging.debug(f"Path found from {start_idx} to {end_idx} with "
                              f"{len(search_graph.edges[(start_idx, end_idx)]['path'])} steps")
                search_graph.edges[(start_idx, end_idx)]['weight'] = \
                    _lower_bound_path_time(search_graph.edges[(start_idx, end_idx)]['path'])
                if logging.getEffectiveLevel() <= logging.DEBUG:
                    logging.debug(f"Path weights: {search_graph.edges[(start_idx, end_idx)]['weight']}")
                    logging.debug("Length with kunz: {}".format(
                                  from_kunz_trajectory(search_graph.edges[(start_idx, end_idx)]['path'], .1,
                                                       self.joint_velocity_limits,
                                                       self.trajectory_acceleration_lim).t[-1]))
            else:  # Only done if no break occurred - complete valid path could be found
                return_paths += 1
            handled_paths.add(tuple(path))  # Add path to handled path; either has all sub-steps or invalid
            # Restart iterator to use new weights - cheap ~µs
            try:
                paths = nx.shortest_simple_paths(search_graph, (graph_start_name, 0), (graph_end_name, 0),
                                                 weight='weight')
            except nx.NetworkXNoPath:
                logging.debug("No more paths between start and end of search graph")
                paths = []  # Empty iterator will be handled at start of while loop

        # Return shortest path
        edges_with_path = [e[:2] for e in search_graph.edges.data('path', None) if e[2] is not None]
        if return_paths == 0:
            logging.debug(f"Valid connections: {edges_with_path}")
            raise TimeoutError("Could not find a path to all goals.")
        logging.info(f"Found {return_paths} possible paths to connect all goals.")

        # Return path is now path through sub-graph with valid 'path' on edge
        shortest_path_idx = nx.shortest_path(nx.edge_subgraph(search_graph, edges_with_path),
                                             (graph_start_name, 0), (graph_end_name, 0),
                                             weight='weight')
        return {end_idx[0]: search_graph.edges[(start_idx, end_idx)]['path']
                for start_idx, end_idx in zip(shortest_path_idx[:-1], shortest_path_idx[1:]) if end_idx[0] in goal_ids}

    @staticmethod
    def _plot_search_graph(search_graph: nx.DiGraph) -> None:
        """Debug plot the search graph constructed in _solve_multi_reach."""
        nx.nx_agraph.write_dot(search_graph, "search_graph.dot")
        print("Use `dot -Tpng search_graph.dot > search_graph.png` to visualize the search graph.")
        print("Replace 'weight' with 'label' in dot file to show edge weights.")

    def _solve_reach_goal(self,
                          goal_id: str,
                          q_previous: np.ndarray,
                          time_limit: Optional[float] = None,
                          q_suggestion: Optional[np.ndarray] = None) -> np.ndarray:
        """
        Solves a reach and (inefficiently) At goal.

        Returns a nxnDoF path to go to next goal; PTP movements between path's via points should be collision free
        (at least with the OMPL planner)

        :param goal_id: At or Reach goal to plan a path to
        :param q_previous: robot assembly configuration to start planning the path to the goal from with shape (nDoF,)
        :param time_limit: custom time limit for this goal
        :param q_suggestion: suggestion for the ik solution to the goal with shape (nDoF,)
        :return: path with n steps for each of assembly.DoF's joints
        :raise PathPlanningFailedException, TimeoutError: If no valid path could be found to the goal within the time
        """
        goal: Goals.At = self.task.goals_by_id[goal_id]
        if time_limit is None:
            time_limit = self.time_limit_per_goal
        if q_previous.shape != (self.robot.dof,):
            raise ValueError(f"q_previous has wrong shape {q_previous.shape} for robot with {self.robot.dof} DoF.")

        t0 = time()

        ik_candidates, success = self._create_reach_goal_ik_candidates(goal, q_previous, t0, time_limit, q_suggestion)

        logging.debug(f"Found ik solutions in {time() - t0} with {ik_candidates}")
        # Try without via points first
        if success and self._check_linear_path(q_previous, ik_candidates[0]):
            logging.debug("Linear connecting path valid.")
            return np.vstack([q_previous, ik_candidates[0]])

        # Use OMPL to fix if available
        if time() - t0 < time_limit:
            ompl_spec = importlib.util.find_spec("ompl")
            if ompl_spec is not None:
                logging.debug("Try solving reach goal with OMPL.")
                if q_suggestion is None:
                    from mcs.OMPLPathPlanner import IKPlanner
                    planner = IKPlanner(self.robot, self.task, time_limit=t0 + time_limit - time(),
                                        resolution=self.dq_checking_resolution, simplify_path=True, rrtc_range=.9,
                                        safety_margin=self.safety_margin)
                    self._all_planners.append(planner)
                    path = planner.find_path(q_previous, goal.goal_pose)
                else:
                    from mcs.OMPLPathPlanner import RrtcPlanner
                    planner = RrtcPlanner(self.robot, self.task, time_limit=t0 + time_limit - time(),
                                          resolution=self.dq_checking_resolution, simplify_path=True, rrtc_range=.9,
                                          safety_margin=self.safety_margin)
                    self._all_planners.append(planner)
                    path = planner.find_path(q_previous, q_suggestion)
                self._successful_planners.append(planner)
                return path.q
            elif success:  # Last chance test connecting other ik solutions with linear paths
                logging.debug("Trying other IK solutions with linear connecting paths.")
                for candidate in ik_candidates[1:]:
                    if time() - t0 > time_limit:
                        break
                    if self._check_linear_path(q_previous, candidate):
                        return np.vstack([q_previous, candidate])

        raise PathPlanningFailedException(f"Could not find a valid path to goal {goal_id}.")

    def _create_reach_goal_ik_candidates(self, goal: Goals.At, q_previous: np.ndarray, t0: float, time_limit: float,
                                         q_suggestion: Optional[np.ndarray] = None) -> Tuple[List[np.ndarray], bool]:
        """
        Create IK candidates for a reach goal and sort them by distance to q_previous.

        :param goal: Reach goal to find ik solutions for
        :param q_previous: previous robot configuration to find ik solution next to
        :param t0: start time of solve
        :param time_limit: time limit for finding iks for this goal
        :param q_suggestion: Optional suggestion for the ik solution to the goal with shape (nDoF,)
        :return: ik_candidates, success
        """
        if q_suggestion is None:
            # Find IK solutions ordered by distance to q_previous
            ik_generation_kwargs = self.get_current_ik_generation_kwargs(t0 + time_limit - time())
            ik_generation_kwargs['minimum_search_time'] /= len(self.task.goals)  # Adapt soft timeout to time per goal
            ik_generation_kwargs['max_ik_count'] = np.inf  # Can cheaply handle a lot as we only minimize L2 distance
            logging.debug(f"Find IK candidates for {goal.id} with {ik_generation_kwargs}")
            try:
                ik_candidates = create_ik_candidates_with_timeout(self.robot, {goal.id: goal}, rng=self._rng,
                                                                  return_partial=True, **ik_generation_kwargs)[goal.id]
            except TimeoutError as e:
                logging.info(f"IK generation raised {e}")
                return [], False  # No fitting IK solution -> last chance is with OMPL
            if len(ik_candidates) > 0:
                distances = np.linalg.norm(np.asarray(ik_candidates) - q_previous, axis=1, ord=float('inf'))
                ik_candidates = [ik_candidates[idx] for idx in np.argsort(distances)]  # sort ik_candidates by distance
                logging.info(f"Found {len(ik_candidates)} ik candidates for goal {goal.id} ({ik_candidates})")
                return ik_candidates, True
            else:
                logging.info(f"Found no IK candidates for {goal.id}")
                return [], False  # No fitting IK solution -> last chance is with OMPL
        else:
            if goal.goal_pose.valid(self.robot.fk(q_suggestion)):
                logging.debug("Suggested IK solution fulfills goal.")
                return [q_suggestion], True
            else:
                logging.debug("Suggested IK solution does not fulfill goal.")
                return [], False  # No fitting IK solution -> last chance is with OMPL

    def _check_linear_path(self, q1: np.ndarray, q2: np.ndarray) -> bool:
        """Check if a linear interpolation between two configurations respects all task constraints."""

        def straight_line_path(q_start, q_end, step_size_dq) -> np.ndarray:
            """Create linear interpolation between a start and end configuration with max step size in any joint."""
            steps = int(np.ceil(np.max(np.abs(q_start - q_end)) / step_size_dq))
            return q_start + np.arange(steps + 1)[:, None] * (q_end - q_start)[None, :] / steps

        fine_path = Lazy(lambda: straight_line_path(q1, q2, self.dq_checking_resolution))
        # Check for joint limits if required by task
        if self._joint_limit_constraint is not None:
            if not all(
                    self._joint_limit_constraint.check_single_state(self.task, self.robot, q, np.zeros_like(q))
                    for q in (q1, q2)):
                return False
        # Static torque must be respected at all times
        if self._joint_limit_constraint is not None and self._joint_limit_constraint.tau:
            if not all(
                    self._joint_limit_constraint.check_single_state(self.task, self.robot, q, np.zeros_like(q))
                    for q in fine_path()):
                return False
        # Check for collisions if required by task
        if self._collision_free_constraint is not None:
            if not all(
                    self._collision_free_constraint.check_single_state(self.task, self.robot, q, np.zeros_like(q))
                    for q in fine_path()):
                return False
        return True


class HierarchicalTaskSolverBase(TaskSolverBase, abc.ABC):
    """
    Extends the TaskSolverBase with the custom utility of applying filters of increasing complexity

    to a assembly-task pair before performing (expensive) full-solution search.
    """

    intermediate_results: Optional[rf.IntermediateFilterResults]  # Partial solutions, evaluated by the filters applied
    _filter_desc: Dict[str, Type[rf.AssemblyFilter]] = {  # Maps string arguments to filter classes
        'robot_creation': rf.RobotCreationFilter,
        'ik': rf.InverseKinematicsSolvable,
        'static_torque': rf.StaticTorquesValid,
        'joint_limits': rf.JointLimitsMet
    }

    def __init__(self,
                 task: Task,
                 assembly: Optional[ModuleAssembly] = None,
                 filters: Sequence[Union[rf.AssemblyFilter, str]] = (),
                 *args,
                 **kwargs):
        """
        Initialize everything a task solver needs, including the filters that make up the hierarchical element.

        :param filters: An ordered iterable of AssemblyFilters to be applied to a robot possibly solving a task in the
          order provided. Usually, you want to order them from least to most complex to rule out "bad" robots as cheap
          as possible. Filter names can be provided as strings - in that case, filters are instantiated with default
          arguments if applicable.
        :param args: Positional arguments used by other classes in multiple-inheritance initialization
        :param kwargs: Keyword arguments used by other classes in multiple-inheritance initialization
        """
        if not (all(isinstance(ft, str) for ft in filters) or all(isinstance(ft, rf.AssemblyFilter) for ft in filters)):
            raise ValueError("Invalid filters argument: Must either be strings or robot filters. Mixing not allowed.")
        if any(isinstance(ft, str) for ft in filters):
            if not all(ft in self._filter_desc for ft in filters):
                raise ValueError(f"Invalid filter argument. Not all of {', '.join(filters)} are in possible options"
                                 f" {', '.join(self._filter_desc.keys())}.")
            filters = tuple(self._filter_desc[ft]() for ft in filters)
        self.filters = tuple(filters)
        for ft in self.filters:
            ft.reset()
        self.intermediate_results = rf.IntermediateFilterResults()
        try:
            rf.assert_filters_compatible(self.filters)
        except rf.InvalidFilterCombinationError:
            logging.warning("Constructing HierarchicalTaskSolver with incompatible filter chain")
        super().__init__(task, assembly, *args, **kwargs)

    def solve(self, assembly: ModuleAssembly = None,
              base_placement: Optional[Transformation] = None) -> Optional[Trajectory]:
        """
        Overrides the default solve to apply filters before trying to find a full solution.

        :param assembly: Sets the assembly of the solver
        :param base_placement: Where to put the base of the assembly
        """
        t0 = time()
        self.reset()
        if base_placement is not None and base_placement != self.robot.placement:
            raise NotImplementedError("Cannot yet handle base placement in hierarchical task solver.")
        if assembly is not None:
            self.assembly = assembly
        elif self.assembly is None:
            raise ValueError("There is no robot assigned to this solver.")

        if self._evaluated:
            raise ValueError(f"Task {self.task} has already been solved with robot {self.robot}.")

        self._set_base()
        self._evaluated = True
        for assembly_filter in self.filters:
            # Do the simple checks before trying to solve the problem
            try:
                passes_filter = assembly_filter.check(self.assembly, self.task, self.intermediate_results)
            except rf.OverwritesIntermediateResult:
                logging.debug(f"{filter.__class__.__name__} overwrites an intermediate result. Re-evaluating.")
                with self.intermediate_results.allow_changes():
                    passes_filter = assembly_filter.check(self.assembly, self.task, self.intermediate_results)
                    if not passes_filter:
                        # So the filter wants to change previous results, but it wouldn't even pass
                        return None

                # In the double-checking if previous filters still hold, overwriting is no longer allowed
                for previous in self.intermediate_results.filters_validated[:-1]:
                    # The last one has just been evaluated, so only need to check previous ones
                    passes_filter &= previous.check(self.assembly, self.task, self.intermediate_results)
                    if not passes_filter:
                        return None
            if not passes_filter:
                return None

        # TODO: Use intermediate results in solve
        solution_trajectory = self._solve(t0=t0)
        return solution_trajectory

    @TaskSolverBase.assembly.setter
    def assembly(self, new_assembly: ModuleAssembly):
        """Reset information about being solved"""
        self.intermediate_results = rf.IntermediateFilterResults()
        self._evaluated = False
        self._assembly = new_assembly


class SimpleHierarchicalTaskSolver(HierarchicalTaskSolverBase, SimpleTaskSolver):
    """Performs hierarchical filtering of assembly-task pairs before applying simple task solvers on them"""

    def __init__(self, task: Task, assembly: Optional[ModuleAssembly] = None,
                 filters: Sequence[Union[rf.AssemblyFilter, str]] = (),
                 **kwargs):
        """Mixes the init methods of the parent classes"""
        super().__init__(task, assembly, filters, **kwargs)
