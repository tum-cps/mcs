# Author: Jonathan Külz
# Date: 12.04.22
# Defines the base environment needed for optimization with Stable Baselines and the Open AI Gym
# Please define your own, complex environment in environments/CustomEnvironment.py
import gymnasium as gym


EnvironmentBase = gym.Env

# <-- Environment utility wrappers etc can go here -->
