# Author: Jonathan Külz
# Date: 14.04.22
# Defines "proxy" environments that do not really support sophisticated agent-interaction.
# Think of them as "one-step-environments", where the episode ends after a single action
# Their main purpose is to provide a unified interface for all kind of learning processes, even if an algorithm
# already returns a complete solution
from datetime import datetime
from typing import Any, Dict, Optional, Tuple

import gymnasium as gym
import pinocchio as pin

from mcs.PathPlanner import PathPlanningFailedException
from mcs.utilities.trajectory import TrajectoryGenerationError
from timor import ModuleAssembly
from timor.task.CostFunctions import CostFunctionBase
from timor.task.Solution import SolutionBase, SolutionHeader, SolutionTrajectory
from timor.task.Task import Task

from mcs.TaskSolver import TaskSolverBase


class FullSolutionEvaluationEnvironment(gym.Env[SolutionBase, SolutionBase]):
    """An environment that evaluates a pre-computed solution.

    This "one-step-environment" can and should (only) be used for optimizers that don't need interaction
    with an environment but, based on a problem statement (task), directly return a full solution.
    This environment expects only one kind of action: the complete solution as returned by an optimizer.
    Consequently, the sequence length of all sequences is one only, and before every step the environment needs
    to be reset.
    """

    action_space = None  # Gym property
    observation_space = None  # Gym property
    stable_baselines_compatible = False  # No action and observation space possible here, not sb3 compatible
    metadata = {'render.modes': ['human']}

    def __init__(self, render_mode='human'):
        """
        Initializes the environment.
        """
        super().__init__()
        if render_mode not in self.metadata['render.modes']:
            raise ValueError(f"Unknown render mode: '{render_mode}'")
        self.render_mode = render_mode
        self.solution = None
        self.reset()

    def render(self) -> pin.visualize.MeshcatVisualizer:
        """Visualize the environment"""
        if self.solution is None:
            raise ValueError("This environment cannot be rendered before an action was performed.")

        return self.solution.visualize()

    def reset(self, seed: Optional[int] = None, options: Optional[Dict[str, Any]] = None):
        """
        Reset the environment. In this case, as the environment is rather simple, only needs to reset the solution.

        :param seed: Seed for random number generator
        :param options: None
        :return: Nothing
        """
        super().reset(seed=seed)
        self.solution = None

    def step(self, action: SolutionBase) -> Tuple[SolutionBase, float, bool, bool, dict]:
        """
        Evaluate the given solution.

        :param action: A solution solving a task (accessible via solution.task)
        :return: The solution (as observation), the negative cost (as reward),
          a boolean indicating whether the episode has ended (always true for this environment!),
          a boolean indicating whether the episode was truncated (always false for this environment!),
          and as info object, a dictionary containing the validity of the solution.
        """
        self.reset()  # "One-Step-Environment"
        self.solution = action
        return self.solution, -self.solution.cost, True, False, {'success': self.solution.valid}


class TaskSolverAssemblyEvaluationEnvironment(gym.Env[SolutionBase, ModuleAssembly]):
    """An environment that takes pre-built assemblies

    This "one-step-environment" can and should (only) be used for optimizers that don't need interaction
    with an environment but, based on a problem statement (task), directly return a assembly.
    This environment expects only one kind of action: the complete assembly as returned by an optimizer.
    This assembly will then be evaluated on a custom cost function using a predefined trajectory planner on a task
    and the according solution will be returned.
    Consequently, the sequence length of all sequences is one only, and before every step the environment needs
    to be reset.
    """

    action_space = None  # Gym property
    observation_space = None  # Gym property
    stable_baselines_compatible = False  # No action and observation space possible here, not sb3 compatible
    metadata = {'render.modes': ['human']}

    def __init__(self,
                 task_solver: 'TaskSolverBase',
                 cost_function: CostFunctionBase,
                 render_mode: str = 'human'):
        """
        Initializes the environment.

        :param task_solver: Can map assembly -> trajectory via solve method
        :param cost_function: The cost function instance to use for solution evaluation
        """
        super().__init__()
        self.task_solver: 'TaskSolverBase' = task_solver
        self.task: Task = task_solver.task
        self.cost_function: CostFunctionBase = cost_function
        self.assembly: Optional[ModuleAssembly] = None
        self.solution: Optional[SolutionBase] = None
        if render_mode not in self.metadata['render.modes']:
            raise ValueError(f"Unknown render mode: '{render_mode}'")
        self.render_mode = render_mode
        self.reset()

    def reset(self, seed: Optional[int] = None, options: Optional[Dict[str, Any]] = None):
        """
        Reset the environment. In this case, as the environment is rather simple, only needs to reset the solution.

        :param seed: Seed for random number generator
        :param options: None
        :return: Nothing
        """
        super().reset(seed=seed)
        self.assembly = None
        self.solution = None

    def render(self) -> pin.visualize.MeshcatVisualizer:
        """Visualize an environment"""
        if self.solution is None:
            return self.task.visualize(robots=self.assembly)
        return self.solution.visualize()

    def step(self, action: ModuleAssembly) -> Tuple[SolutionTrajectory, float, bool, bool, dict]:
        """
        Evaluate the given robot assembly and return the solution.

        :param action: The assembly to evaluate
        :return: The solution (as observation), the negative cost (as reward),
          a boolean indicating whether the episode has ended (always true for this environment!),
          a boolean indicating whether the episode was truncated (always false for this environment!),
          and as info object, a dictionary containing the validity of the solution.
        :raises TimeoutError: If the task solver times out
        :raises PathPlanningFailedException: If the task solver cannot find a path and the specific reason is unknown
        :raises TrajectoryGenerationError: If the task solver cannot generate a trajectory
        """
        self.assembly = action
        trajectory = self.task_solver.solve(self.assembly)

        return self._evaluate_trajectory(trajectory)

    def _evaluate_trajectory(self, trajectory) -> Tuple[SolutionTrajectory, float, bool, bool, dict]:
        """Map a trajectory to the step return value."""
        header = SolutionHeader(taskID=self.task.id, date=datetime.now())
        solution = SolutionTrajectory(
            trajectory=trajectory,
            header=header,
            task=self.task,
            assembly=self.assembly,
            cost_function=self.cost_function,
            base_pose=self.assembly.robot.placement)
        self.solution = solution
        return solution, -solution.cost, True, False, {'success': solution.valid}


class FixedPenaltyTaskSolverAssemblyEvaluationEnvironment(TaskSolverAssemblyEvaluationEnvironment):
    """
    An environment that returns a fixed penalty if there is an exception within the step method of the task solver eval.
    """

    def __init__(self, reward_fail: float = -1000.0, *args, **kwargs):
        """
        Initializes the environment.

        :param reward_fail: Reward to return if task solver fails.
        :param args: Arguments for parent class (TaskSolverAssemblyEvaluationEnvironment)
        :param kwargs: Arguments for parent class (TaskSolverAssemblyEvaluationEnvironment)
        """
        super().__init__(*args, **kwargs)
        self.reward_fail = reward_fail

    def step(self, action: ModuleAssembly) -> Tuple[Optional[SolutionTrajectory], float, bool, bool, dict]:
        """
        Evaluate the given robot assembly and return the solution.

        :param action: The assembly to evaluate
        :return: The solution (as observation), the negative cost (as reward),
          a boolean indicating whether the episode has ended (always true for this environment!),
          a boolean indicating whether the episode was truncated (always false for this environment!),
          and as info object, a dictionary containing the validity of the solution.
        """
        self.assembly = action
        try:
            trajectory = self.task_solver.solve(self.assembly)
        except (TimeoutError, PathPlanningFailedException, TrajectoryGenerationError):
            return None, self.reward_fail, True, False, {'success': False, 'timeout': True}

        if trajectory is None:
            return None, self.reward_fail, True, False, {'success': False, 'filter_fail': True}

        sol, reward, done, truncated, info = self._evaluate_trajectory(trajectory)
        return sol, reward if sol.valid else self.reward_fail, done, truncated, info
