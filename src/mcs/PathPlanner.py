#!/usr/bin/env python3
from __future__ import annotations

import abc
from abc import ABC
from typing import Optional

from timor.Robot import RobotBase
from timor.task import Task
from timor.utilities.trajectory import Trajectory


class PathPlanningFailedException(Exception):
    """Raised when a path cannot be found without knowing the specific reason."""


class StartStateInvalidError(PathPlanningFailedException):
    """Raised when the start state is invalid."""


class GoalStateInvalidError(PathPlanningFailedException):
    """Raised when the goal state is invalid."""


class PathPlannerBase(ABC):
    """
    A Path planner base class.

    The purpose of a planner is to take a robot, its environment (in the form of a task)
    and then plan movements/paths for this robot from one goal to another considering obstacles
    and static robot limitations.
    """

    robot: RobotBase
    task: Optional[Task]
    time_limit: float

    def __init__(self, robot: RobotBase, task: Task = None, time_limit: float = float('inf')):
        """
        Initialize PlannerBase

        :param robot: The robot whose actions are going to be planned
        :param task: The task, a description of the environment the robot works in. If none, expects the
          environment is always collision-free
        :param time_limit: maximum time allowed to be spent for solving a problem
        """
        self.robot = robot
        self.task = task
        self.time_limit = time_limit

    def find_path(self, start_state: any, goal_state: any, *args, **kwargs) -> Trajectory:
        """The external interface to solve a task. Can be overridden by more complex methods in subclasses"""
        return self._find_path(start_state, goal_state, *args, **kwargs)

    @abc.abstractmethod
    def _find_path(self, start_state: any, goal_state: any, *args, **kwargs) -> Trajectory:
        """
        The logic of the planner: Given the task and the robot, find a solution.
        """
