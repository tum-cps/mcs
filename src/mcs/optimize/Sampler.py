#!/usr/bin/env python3
# Author: Jonathan Külz
# Date: 20.10.22
from __future__ import annotations
import abc
import itertools
from typing import Iterable, Iterator, Optional, Sequence, Tuple, Type, TypeVar, Union

import numpy as np
from timor.task import Tolerance
from timor.utilities import spatial
from timor.utilities.transformation import Transformation, TransformationLike
from timor.utilities.tolerated_pose import ToleratedPose


class UniformRandomSampler(Iterator, abc.ABC):
    """
    A sampler returns uniformly distributed random samples from a pre-defined set of type SampleReturnType.

    Can be used as an infinite Iterator.
    """

    SampleReturnType = TypeVar('SampleReturnType')

    def __init__(self, rng: np.random.Generator = None):
        """
        The minimal setup of a sampler is a random number generator.

        The logic of uniform random sampling is implemented by numpy -- UniformRandomSampler just wrap this
        functionality in a convenient interface to sample specific types of objects.

        :param rng: Random number generator to use for sampling. Defaults to np.random.default_rng()
        """
        self.rng: np.random.Generator = rng if rng is not None else np.random.default_rng()

    @abc.abstractmethod
    def sample(self) -> SampleReturnType:
        """Return a random sample from the pre-defined set"""
        raise NotImplementedError

    def __iter__(self) -> Iterable[SampleReturnType]:
        """Each sampler should also be iterable"""
        return self

    def __next__(self) -> SampleReturnType:
        """In the simplest case a sampler iteration just returns a (random) sample"""
        return self.sample()


class FiniteSampler(UniformRandomSampler, abc.ABC):
    """
    A finite sampler draws samples from a limited data structure (Collections).

    It can offer both, exhaustive iteration or uniform sampling over all samples.
    """

    SampleReturnType = TypeVar('SampleReturnType')

    @abc.abstractmethod
    def as_finite_iterable(self, randomize: bool = True) -> Iterable[SampleReturnType]:
        """
        Return an iterator over all possible samples in this finite sampler.

        :param randomize: If True, the samples are returned in a random order. Otherwise, the samples are returned in
            the order they are stored in the sampler (which is deterministic if the original set to sample from was
            given as a Sequence).
        """
        pass

    @abc.abstractmethod
    def __len__(self):
        """Number of samples in this finite sampler"""
        pass


class FiniteSetSampler(FiniteSampler):
    """
    A finite set sampler draws samples from a limited number of samples.
    """

    SampleReturnType = TypeVar('SampleReturnType')

    def __init__(self, sample_from: Iterable, max_size: int = None, rng: np.random.Generator = None):
        """
        Sets up a finite set to sample from.

        Be cautious when using this sampler with a large number of samples, as all samples are stored in memory.

        :param sample_from: This can any Iterable, for example another sampler.
        :param max_size: The upper bound on the number of samples to keep from sample_from. If None, sample_from must
            be a Sized object with a limited length.
        """
        if max_size is None:
            try:
                assert len(sample_from) < float('inf')
            except TypeError:
                raise ValueError("If max_size is not given, sample_from must be a Sized object.")
            max_size = len(sample_from)
        if max_size < 1:
            raise ValueError("The max_size of the limited sampler must be at least 1")

        samples = itertools.islice(sample_from, max_size)
        super().__init__(rng)
        self._samples: Tuple[FiniteSetSampler.SampleReturnType, ...] = tuple(samples)

    def __len__(self) -> int:
        """Length of finite set sampler is number of samples it draws from."""
        return len(self._samples)

    def as_finite_iterable(self, randomize: bool = True) -> Iterable[SampleReturnType]:
        """Returns an iterable over all samples -- by default in random order."""
        if randomize:
            return self.rng.permuted(self._samples)
        return self._samples

    def sample(self) -> SampleReturnType:
        """Sample a random transformation from the set of samples"""
        return self.rng.choice(self._samples)


class GridIndexSampler(FiniteSampler):
    """
    Samples indices of an N dimensional grid. Designed as a mixin / cooperative inheritance class.
    """

    SampleReturnType = Tuple[int, ...]  # N dimensional

    def __init__(self, grid_size: Sequence[int], rng: np.random.Generator = None):
        """
        Number of samples in each dimension of the grid

        :param grid_size: N-dimensional giving the number of samples in each direction of the grid
        """
        super().__init__(rng)
        self._grid_size: np.ndarray = np.asarray(grid_size)

    def as_finite_iterable(self, randomize: bool = True) -> Iterable[SampleReturnType]:
        """
        Returns an iterable over all grid indices -- by default in random order.

        :note: random order needs to copy all elements and may therefore use a lot of memory for higher dimensions and
          number of grid points
        """
        ranges = (range(n) for n in self._grid_size)
        if randomize:
            all_indices = list(itertools.product(*ranges))
            self.rng.shuffle(all_indices)
            return all_indices
        return itertools.product(*ranges)

    def sample(self) -> SampleReturnType:
        """Sample a discrete grid-point index."""
        return tuple(map(int, self.rng.integers(self._grid_size)))

    def __len__(self) -> int:
        """Number of grid points."""
        return np.product(self._grid_size)


class GridSampler(GridIndexSampler):
    """
    Samples values from an N dimensional uniform grid. Designed as a cooperative inheritance class.
    """

    SampleReturnType = np.ndarray  # N dimensional

    def __init__(self, grid_size: Sequence[int], spacing: Union[float, Sequence[float]],
                 rng: np.random.Generator = None):
        """
        Initialize a grid with grid_size samples per dimension and spacing distance between samples.

        :param grid_size: N-dimensional giving the number of samples in each direction of the grid
        :param spacing: either distance between samples in all directions (single float for uniform grid) or
            N-dimensional array of distances in each of the N directions of the grid.
        """
        super().__init__(grid_size, rng=rng)
        if isinstance(spacing, float):
            spacing = np.repeat(spacing, len(grid_size))
        if not len(spacing) == len(grid_size):
            raise ValueError(f"grid_size and spacing must have the same length. Got {grid_size} and {spacing}")
        self._grid_spacing: np.ndarray = np.asarray(spacing)  # N dimensional vector of distance per dimension of grid

    def as_finite_iterable(self, randomize: bool = True) -> Iterable[SampleReturnType]:
        """Returns an iterator over all grid values; default in randomized order."""
        return map(self.grid_idx2grid_value, GridIndexSampler.as_finite_iterable(self, randomize))

    def sample(self) -> SampleReturnType:
        """Sample the value at one grid point."""
        return self.grid_idx2grid_value(GridIndexSampler.sample(self))

    def grid_idx2grid_value(self, grid_idx) -> SampleReturnType:
        """Returns the value at the center of the grid cell with given index."""
        return self.get_continuous_coordinate(grid_idx, self._grid_size, self._grid_spacing)

    @staticmethod
    def get_continuous_coordinate(i: Union[int, np.ndarray], n: Union[int, np.ndarray],
                                  w: Union[float, np.ndarray]) -> Union[float, np.ndarray]:
        """
        Get the continuous coordinate of the i-th element in a discretized 1D/ND-space with n elements of width w.

        Assumes that the elements are centered at coordinate=0 in each direction, s.t. the coordinate for the first
        element is minus the coordinate of the last element. The total width of the space in each direction is n*w.
        Enumeration of elements starts at 0.

        :param i: Index of the element to get the coordinate for / N indices for ND space
        :param n: Number of elements / N numbers for each dimension of ND space
        :param w: Width of each element / N widths for each dimension of ND space
        :return: single coordinate (float) or array for ND space
        """
        return w * (i - (n - 1) / 2)

    @property
    def spacing(self) -> np.ndarray:
        """The spacing between grid points in each dimension."""
        return self._grid_spacing


class UniformRotationSampler(UniformRandomSampler):
    """
    Samples rotations uniformly at random.

    The rotation axis is sampled uniformly on the unit sphere and the rotation angle is sampled uniformly in the
    range [0, pi].
    """

    SampleReturnType = np.ndarray  # 3x3

    def __init__(self, rng: np.random.Generator = None):
        """Set up the random number generator."""
        super().__init__(rng)

    def sample(self) -> SampleReturnType:
        """Sample a random rotation matrix."""
        axis = 2 * self.rng.random(size=(3,)) - 1
        axis /= np.linalg.norm(axis)
        angle = self.rng.uniform(0, np.pi)
        return spatial.axis_angle2rot_mat(np.hstack((axis, angle)))


class IdentityRotationSampler(UniformRotationSampler):
    """A dummy sampler that always returns the identity rotation matrix."""

    def sample(self) -> UniformRotationSampler.SampleReturnType:
        """Return the identity rotation matrix."""
        return np.eye(3)


class TransformationSampler(UniformRandomSampler, abc.ABC):
    """
    A transformation sampler generates a uniformly distributed random sample from the SE(3) group of transformations.

    All orientations are sampled as a uniformly distributed random rotation. "Uniform" sampling means sampling uniform
    in the cartesian space.
    """

    SampleReturnType = Transformation

    default_limits: np.ndarray  # Needs to be defined by subclasses

    def __init__(self,
                 limits: Sequence[Sequence[float]] = None,
                 offset: TransformationLike = Transformation.neutral(),
                 rotation_sampler: Optional[Union[UniformRotationSampler, Type[UniformRotationSampler]]] = UniformRotationSampler,  # noqa: E501
                 rng: np.random.Generator = None):
        """
        Initialize a sampler with limits and optionally, a transformation offset for every sample.

        :param limits: Limits for the sampled coordinates as 3x2 vector (min, max)
        :param offset: The sampled coordinates are offset by this transformation
        :param rotation_sampler: Either an initialized sampler or a class reference to instantiate a sampler with the
            same rng as this sampler. If None, rotations will not be sampled.
        """
        UniformRandomSampler.__init__(self, rng=rng)
        if limits is None:
            limits = self.default_limits
        else:
            limits = np.asarray(limits)
        if rotation_sampler is None:
            rotation_sampler = IdentityRotationSampler
        elif isinstance(rotation_sampler, type):
            rotation_sampler = rotation_sampler(rng=rng)
        self._limits: np.ndarray = np.asarray(limits)
        self._offset: Transformation = Transformation(offset)
        self._rotation_sampler: UniformRotationSampler = rotation_sampler

    def sample(self) -> Transformation:
        """Sample a random transformation from the SE(3) group"""
        return self.sample_2_transformation(self.sample_orientation(), self.sample_translation())

    def sample_orientation(self) -> np.array:
        """Sample a random orientation from SO(3)"""
        return self._rotation_sampler.sample()

    def sample_2_transformation(self, orientation, translation):
        """Turns samples orientation and translation into a transformation."""
        return self._offset @ Transformation.from_roto_translation(R=orientation, p=translation)

    @abc.abstractmethod
    def sample_translation(self) -> np.array:
        """The translation sampling behavior will depend on the child class implementation"""

    @property
    @abc.abstractmethod
    def volume(self) -> float:
        """The volume of the sampling space"""


class TransformationGridSampler(TransformationSampler, GridSampler, abc.ABC):
    """
    Samples transformations on a grid in translation and / or rotation space.
    """

    def __init__(self,
                 grid_size: Sequence[int],
                 spacing: Union[float, Sequence[float]],
                 limits: np.ndarray = None,
                 offset: TransformationLike = Transformation.neutral(),
                 rotation_sampler: Optional[Union[UniformRotationSampler, Type[UniformRotationSampler]]] = UniformRotationSampler,  # noqa: E501
                 rng: np.random.Generator = None
                 ):
        """
        Initialize a sampler on a grid of len(grid_size) dimensions.

        :param grid_size: Number of samples in each dimension of the grid
        :param spacing: Distance between samples in each dimension of the grid
        :param limits: the boundary of the grid discretizing translation
        :param offset: the transformation to the center of the grid (orientation + translation)
        """
        TransformationSampler.__init__(self, limits, offset, rotation_sampler=rotation_sampler, rng=rng)
        GridSampler.__init__(self, grid_size, spacing, rng=rng)

    @classmethod
    def from_tolerated_pose(cls,
                            tolerated_pose: ToleratedPose, *,
                            grid_size: Optional[Union[int, Sequence[int]]] = None,
                            grid_width: Optional[Union[float, Sequence[float]]] = None) \
            -> TransformationGridSampler:
        """
        Create a Grid UniformRandomSampler for a tolerated pose.

        :param tolerated_pose: The tolerated pose to draw samples from
        :param grid_width: Maximum distance between samples for all tolerances or each individually
            (mutually exclusive with grid_size)
        :param grid_size: Number of grid points for all tolerances or each individually
            (mutually exclusive with spacing)
        :return: Grid UniformRandomSampler that produces valid poses within the tolerated pose
        """
        if isinstance(tolerated_pose.tolerance, Tolerance.CartesianXYZ):
            if (grid_width is None) is (grid_size is None):
                raise ValueError("Needs exactly either spacing or grid_size")
            cube_width = tolerated_pose.tolerance.stacked[:, 1] - tolerated_pose.tolerance.stacked[:, 0]
            cube_mid_offset = np.sum(tolerated_pose.tolerance.stacked, axis=1) / 2
            if grid_size is None:
                grid_size = np.ceil(cube_width / np.asarray(grid_width)).astype(int)
            grid_width = cube_width / grid_size
            return CartesianGridSampler(grid_size=grid_size, spacing=grid_width.tolist(),
                                        offset=tolerated_pose.nominal @ Transformation.from_translation(cube_mid_offset)
                                        )
        else:
            raise NotImplementedError(f"Cannot create sampler for tolerated pose of type {type(tolerated_pose)}")

    @abc.abstractmethod
    def iter_finite_translation(self, randomize: bool = True) -> Iterable[np.array]:
        """Iterable over all translations."""

    def as_finite_iterable(self, randomize: bool = True) -> Iterable[Transformation]:
        """Iterates over all possible translations of this sampler and samples orientations as well."""
        return map(lambda rot_matrix, translation: self.sample_2_transformation(rot_matrix, translation),
                   itertools.repeat(self._rotation_sampler.sample()),
                   self.iter_finite_translation(randomize=randomize))


class CartesianSampler(TransformationSampler):
    """Samples from a cartesian space"""

    default_limits = np.hstack([-np.ones((3, 1)), np.ones((3, 1))])  # Cube with side length 2

    def sample_translation(self) -> np.array:
        """
        Sample a random translation from the cartesian space

        :return: A random translation as a (3,) np array
        """
        return self.rng.uniform(self._limits[:, 0], self._limits[:, 1], size=3)

    @property
    def lim_x(self):
        """Exposes the limits in x-direction"""
        return self._limits[0]

    @property
    def lim_y(self):
        """Exposes the limits in y-direction"""
        return self._limits[1]

    @property
    def lim_z(self):
        """Exposes the limits in z-direction"""
        return self._limits[2]

    @property
    def volume(self) -> float:
        """The volume of the sampling space"""
        return abs(np.prod(self._limits[:, 1] - self._limits[:, 0]))


class CartesianGridSampler(TransformationGridSampler, CartesianSampler):
    """
    Grid translation sampler samples from cartesian translation space discretized in N voxels.

    A grid sampler has two sampling modes - either sample(), which returns a transformation that is centered in one
    of the discrete voxels, or sample_grid_point(), which returns the index of the sampled voxel (and no orientation).
    """

    default_grid_size = (10, 10, 10)  # Number of voxels in (x, y, z)-dimension
    default_widths = (.1, .1, .1)  # Expansion of each voxel in (x, y, z)-dimension

    def __init__(self,
                 grid_size: Union[int, Sequence[int, int, int]] = default_grid_size,
                 spacing: Union[float, Sequence[float, float, float]] = default_widths,
                 offset: TransformationLike = Transformation.neutral(),
                 rotation_sampler: Optional[Union[UniformRotationSampler, Type[UniformRotationSampler]]] = UniformRotationSampler,  # noqa: E501
                 rng: np.random.Generator = None
                 ):
        """
        Initialize a grid sampler with a given grid size and voxel spacing.

        :param grid_size: Number of voxels in (x, y, z)-dimension. Can be a single number for a cubic grid.
        :param spacing: Expansion of each voxel in (x, y, z)-dimension. Can be a single number for cubic voxels.
        :param offset: The sampled coordinates are offset by this transformation in case of continuous sampling. There
          is no such thing as an offset for the discrete samples returned by this class.
        """
        if isinstance(grid_size, (int, np.integer)):
            grid_size = (grid_size, grid_size, grid_size)
        if not all(((n > 0) and (isinstance(n, (int, np.integer)))) for n in grid_size):
            raise ValueError(f"Invalid grid size: {grid_size}. Has to be positive integers.")
        if isinstance(spacing, float):
            spacing = (spacing, spacing, spacing)
        if not all((w > 0) for w in spacing):
            raise ValueError(f"Invalid voxel spacing: {spacing}. Has to be positive.")

        lim = self.get_continuous_coordinate(np.array(grid_size) - 1, np.array(grid_size), np.array(spacing)) \
            + np.array(spacing) / 2  # coordinate of most positive voxel + half the grid-width
        super().__init__(grid_size=grid_size, limits=np.vstack((-lim, lim)).T, offset=offset,
                         spacing=spacing, rotation_sampler=rotation_sampler, rng=rng)

    def iter_finite_translation(self, randomize: bool = True) -> Iterable[np.array]:
        """Iterates over all translations; optionally in random order."""
        return GridSampler.as_finite_iterable(self, randomize)

    def sample_translation(self) -> np.array:
        """Sample a transformation to the center of one of the discrete voxels."""
        return GridSampler.sample(self)


class SphericalSampler(TransformationSampler):
    r"""
    Samples from a volume described by a partial sphere in spherical coordinates.

    Spherical coordinates: [$r$, $\theta$, $\phi$] ~ radial, azimuthal, polar where
    $r \geq 0$, $0 \leq \theta < \pi$ and $-\pi \leq \phi < \pi$]
    :source: https://en.wikipedia.org/wiki/Spherical_coordinate_system
    """

    default_limits = np.array(((0, 1), (0, np.pi), (-np.pi, np.pi)))  # Default to ball with r=1

    def sample_translation(self) -> np.array:
        """
        Sample a translation s.t. every point in the volume defined by [r, theta, phi] is sample with equal probability.

        :source: stats.stackexchange.com/questions/8021
        :return: A random translation as a (3,) np array
        """
        r = self.rng.uniform(self.lim_r[0] ** 3, self.lim_r[1] ** 3) ** (1 / 3)

        cos_theta = self.rng.uniform(-1 + 2 * (np.pi - self.lim_theta[1]) / np.pi, 1 - 2 * self.lim_theta[0] / np.pi)
        theta = np.arccos(cos_theta)

        phi = self.rng.uniform(self.lim_phi[0], self.lim_phi[1])
        return spatial.spherical2cartesian(np.array((r, theta, phi)))

    @property
    def lim_r(self) -> np.array:
        """The radial limits"""
        return self._limits[0, :]

    @property
    def lim_theta(self) -> np.array:
        """The azimuthal limits"""
        return self._limits[1, :]

    @property
    def lim_phi(self) -> np.array:
        """The polar limits"""
        return self._limits[2, :]

    @property
    def volume(self) -> float:
        """The volume of the sampling space"""
        return 4 / 3 * np.pi * \
            (self.lim_r[1] ** 3 - self.lim_r[0] ** 3) * \
            (self.lim_theta[1] - self.lim_theta[0]) / np.pi * \
            (self.lim_phi[1] - self.lim_phi[0]) / (2 * np.pi)
