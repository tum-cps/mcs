import abc
import time
from typing import Iterable, Optional, Sequence, Tuple, Type

import numpy as np
from pinocchio.visualize import MeshcatVisualizer

from timor.configuration_search import AssemblyFilter
from timor.Module import ModuleAssembly, ModulesDB
from timor.task.CostFunctions import GoalsFulfilled
from timor.task.Solution import SolutionBase, SolutionHeader, SolutionTrajectory
from timor.task.Task import Task
from timor.utilities import logging, prebuilt_robots
from timor.utilities.trajectory import Trajectory
from timor.utilities.transformation import Transformation, TransformationLike
from tqdm import tqdm

from mcs.TaskSolver import SimpleTaskSolver, TaskSolverBase


class ConfigurationOptimizer(abc.ABC):
    """Base class for optimizing the configuration of a modular robot."""

    task_solver_ref: Type[TaskSolverBase]  # The reference to the class of the task solver
    timeout: float  # The maximum time in seconds for an optimizer to find a solution for a single task
    _task: Optional[Task]  # The task currently being optimized. Unpacked to ease access in evaluation

    def __init__(self, db: ModulesDB, task_solver_ref: Type[TaskSolverBase], *, timeout: float = float('inf')):
        """
        Base initializer

        :param db: A database to take the modules from that can build the solving assemblies
        :param task_solver_ref: A task solver class that takes a task as init parameter and can work on
          changing assemblies. An instance of this class will be created for every new task plugged in the optimizer.
        :param timeout: A timeout parameter for the optimization process
        """
        self._db: ModulesDB = db
        self._task_solver: Optional[TaskSolverBase] = None
        self._task_solver_ref: Type[TaskSolverBase] = task_solver_ref
        self.timeout = timeout

    @property
    def db(self) -> ModulesDB:
        """
        The modules DB to construct solution assemblies from.

        Keeps access to alter the db private.
        """
        return self._db

    @property
    def task_solver(self) -> Optional[TaskSolverBase]:
        """
        Generates trajectories from assemblies and solution.

        Keeps access to alter the planner private
        """
        return self._task_solver

    def solve(self, task: Task, *args, **kwargs) -> SolutionBase:
        """Sets up the task solver and calls the implemented _solve method"""
        self._task = task
        self._task_solver = self._task_solver_ref(task, timeout=self.timeout)
        return self._solve(task, *args, **kwargs)

    @abc.abstractmethod
    def _solve(self, task: Task, *args, **kwargs) -> SolutionBase:
        """Contains the optimizer logic"""

    def visualize(self, viz: MeshcatVisualizer, prefix: str = "Optimizer"):
        """
        Visualizes the optimizer's current state in the given visualizer

        :param viz: The visualizer to use
        :param prefix: The namespace to use within the visualizer; add level with slash
        """
        if hasattr(self._task_solver, "visualize"):
            self._task_solver.visualize(viz, prefix + "/TaskSolver")
        else:
            logging.warning(f"Task solver {self._task_solver} has no visualization method")


class UsePredefinedAssembly(ConfigurationOptimizer):
    """Try to solve task with a given assembly."""

    def __init__(self,
                 assembly: ModuleAssembly = None,
                 task_solver_ref: Type[TaskSolverBase] = SimpleTaskSolver,
                 *, timeout: float = float('inf')):
        """Performs no optimization at all but instead works with a given assembly.

        If none is provided, a default one (6 DoF, ~1m reach) will be used.
        :param assembly: A user pre-defined assembly that's used to solve all tasks handed over to this class
        """
        db = ModulesDB()  # empty, as there will be no optimization over any modules
        super().__init__(db, task_solver_ref, timeout=timeout)
        if assembly is None:
            # TODO : Load default assembly from subrepository and provide it as default argument instead of if/else
            self.assembly: ModuleAssembly = prebuilt_robots.get_six_axis_assembly()
        else:
            self.assembly: ModuleAssembly = assembly

    def _solve(self, task: Task, *args, **kwargs) -> SolutionBase:
        t0 = time.time()
        trajectory = self.task_solver.solve(self.assembly)
        header = SolutionHeader(task.id, computationTime=time.time() - t0)
        solution = SolutionTrajectory(trajectory, header, task,
                                      self.assembly, GoalsFulfilled(), self.assembly.robot.placement)
        if not solution.valid:
            raise \
                ValueError(f"No valid solution found {str(solution.failed_goals)}; {str(solution.failed_constraints)}")
        return solution


class BruteForce(ConfigurationOptimizer):
    """
    A brute force optimizer that iterates over all assemblies and tries to find a solution with them
    """

    def __init__(self, db, assemblies: Iterable[ModuleAssembly],
                 task_solver_ref: Type[TaskSolverBase] = SimpleTaskSolver,
                 *, timeout: float = np.inf):
        """Instantiate the optimizer by providing the iterator to brute-force iterate over.

        :param db: Module database used for the optimization
        :param assemblies: Can be a list/set/iterable of assemblies. Can specifically be an AssemblyIterator object.
        :param task_solver_ref: A task solver class that takes a task as init parameter and can work on
          changing assemblies. An instance of this class will be created for every new task plugged in the optimizer.
        :param timeout: If no solution is found before timeout seconds, this raises a TimeoutError
        """
        super().__init__(db, task_solver_ref, timeout=timeout)
        self.assemblies = assemblies

    def _solve(self,
               task: Task,
               base_placement: TransformationLike = Transformation.neutral(),
               **kwargs) -> SolutionBase:
        """
        Try to solve a task.

        :param task: Task to solve
        :param base_placement: A custom base placement can be specified. This will not be optimized for
        :return: A solution if one is found.
        :raise TimeoutError: If no solution is found before timeout seconds
        :raise ValueError: If none of the assemblies can solve the task
        """
        t0 = time.time()
        for assembly in tqdm(self.assemblies):
            if time.time() - t0 > self.timeout:
                raise TimeoutError("Cannot find a valid solution with brute force within {}s.".format(self.timeout))
            t_robot = time.time()
            trajectory = self._evaluate_assembly(assembly, base_placement)
            if trajectory is not None:
                header = SolutionHeader(task.id, computationTime=time.time() - t_robot)
                solution = SolutionTrajectory(trajectory, header, task, assembly,
                                              GoalsFulfilled(weight=-1.), base_placement)
                return solution

        raise ValueError("None of the provided robot assemblies was suited for the problem")

    def _evaluate_assembly(self, assembly: ModuleAssembly, base_placement: TransformationLike) \
            -> Optional[Trajectory]:
        """
        For a given assembly, tries to find a valid solution

        :param assembly: One specific assembly to solve the task with
        :param base_placement: Where to put the assembly
        :return: A solution if it can find one, None else
        """
        trajectory = self.task_solver.solve(assembly, base_placement)
        if trajectory is None:
            logging.debug(f"Did not find a solution for {assembly} in {self._task.id}")

        return trajectory


class HierarchicalBruteForce(BruteForce):
    """Brute force, but using filters of increasing complexity to save evaluation time"""

    def __init__(self,
                 db: ModulesDB,
                 assemblies: Iterable[ModuleAssembly],
                 task_solver_ref: Type[TaskSolverBase] = SimpleTaskSolver,
                 filters: Sequence[AssemblyFilter.AssemblyFilter] = (), *,
                 timeout: float = np.inf):
        """Performs hierarchical elimination based on filter, then brute force optimizes

        :param db: Modules database all assemblies are built from
        :param assemblies: An iterator yielding all possible assemblies
        :param task_solver_ref: This task solver is going to be used in order to evaluate an assembly
        :param filters: Those filters, one after another, are going to be evaluated on the robot. Once one of them
          fails, the assembly fails.
        :param timeout: Maximum time to wait until the optimizer stops.
        """
        self.filters: Tuple[AssemblyFilter.AssemblyFilter] = tuple(filters)
        AssemblyFilter.assert_filters_compatible(type(f) for f in self.filters)
        super().__init__(db, assemblies, task_solver_ref, timeout=timeout)

    def _evaluate_assembly(self, assembly: ModuleAssembly, base_placement: TransformationLike) \
            -> Optional[Trajectory]:
        """Find a solution, but only if internal filters are passed"""
        valid, report = self._filter(assembly, base_placement)
        if not valid:
            return None

        # TODO: Use report results
        return super()._evaluate_assembly(assembly, base_placement)

    def _filter(self, assembly: ModuleAssembly, base_placement: TransformationLike) \
            -> Tuple[bool, AssemblyFilter.IntermediateFilterResults]:
        """
        Checks whether an assembly passes all filters for the given task

        :param assembly: Modules assembly to filter
        :return: A tuple with two elements:
         - A true/false indicator whether the robot passes the filters (true == passes, false == filer robot)
         - A report dictionary, containing intermediate results
        """
        intermediate_results = AssemblyFilter.IntermediateFilterResults()
        for robot_filter in self.filters:
            passes = robot_filter.check(assembly, self._task, intermediate_results)
            if not passes:
                return False, intermediate_results
        return True, intermediate_results
