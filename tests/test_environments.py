import importlib
import inspect
import itertools
import random
import sys
import time
import unittest
from warnings import warn

from gymnasium import Env
from stable_baselines3.common.env_checker import check_env
import timor.utilities.logging as logging
from timor.task import CostFunctions, Solution, Task

from mcs import TaskSolver
from mcs.environments import Proxies
from mcs.utilities.local_files import environments
from .utils import get_tasks_solutions


class LearningEnvironmentTests(unittest.TestCase):
    """Check (open AI) environment API for RL, etc."""

    def setUp(self) -> None:
        """Setup sampling and find all gym environments."""
        random.seed(8)
        environment_modules = dict()

        self.abstract_environments = dict()  # Empty for now, but could later be implemented
        self.environment_classes = dict()
        for env_file in environments.iterdir():
            if env_file.is_file():
                name = 'mcs.environments.' + env_file.name.split('.')[0]
                environment_modules[name] = importlib.import_module(name)  # Import the module

                for class_name, obj in inspect.getmembers(sys.modules[name], inspect.isclass):
                    if obj.__module__ is not sys.modules[name].__name__:
                        continue  # Ignore imported modules
                    if not issubclass(obj, Env):
                        continue

                    self.environment_classes[class_name] = obj

        self.instantiation_kwargs = {  # Gather some empty initialisation arguments to test empty environment init
            'task': Task.Task({'ID': 'test'}),
            'cost_function': CostFunctions.GoalsFulfilled()
        }

    def test_compatibility_and_inheritance(self):
        """
        Assert all environments are properly defined as children of the gym base environment.

        Assert all environments are compatible with Stable Baselines / Open AI Gym if not explicitly stated otherwise.
        https://stable-baselines.readthedocs.io/en/master/guide/custom_env.html
        """
        for name, cls in itertools.chain(self.environment_classes.items(), self.abstract_environments.items()):
            self.assertTrue(issubclass(cls, Env), msg="{} is not a subclass of Env".format(name))

        for name, cls in self.environment_classes.items():
            parameter_names = [name for name in inspect.signature(cls.__init__).parameters.keys() if name != 'self']
            kwargs = {param: self.instantiation_kwargs[param] for param in parameter_names
                      if param in self.instantiation_kwargs}
            if len(kwargs) != len(parameter_names):
                warn("Could not test instantiated environment {} because it requires some arguments that are not"
                     "defined in the default test case.".format(name))
                continue

            instance = cls(**kwargs)
            if instance.action_space is None or instance.observation_space is None:
                self.assertFalse(dict(inspect.getmembers(instance)).get('stable_baselines_compatible', True),
                                 msg="{} is not compatible with Stable Baselines without explicitly stating so."
                                 .format(name))
            else:
                self.assertTrue(check_env(instance),
                                msg="{} is not compatible with Stable Baselines / Open AI Gym".format(name))

    def test_proxy_environments(self, timeout=10, n_trials=10):
        """Assert that proxy environments can be instantiated and run."""
        eval_data = get_tasks_solutions('simple/PTP_1')
        task = Task.Task.from_json_file(eval_data['task_files'][0])
        solution = Solution.SolutionBase.from_json_file(eval_data['solution_files'][0], tasks={task.id: task})
        self.assertTrue(solution.valid, "Expected to find valid example solution")
        sol_env = Proxies.FullSolutionEvaluationEnvironment()
        _, reward, done, truncated, info = sol_env.step(solution)
        self.assertTrue(done)
        self.assertFalse(truncated)
        self.assertTrue(info['success'])

        task_solver = TaskSolver.SimpleHierarchicalTaskSolver(task, filters=('robot_creation', 'ik', 'static_torque'),
                                                              timeout=timeout)
        rob_env = Proxies.FixedPenaltyTaskSolverAssemblyEvaluationEnvironment(
            task_solver=task_solver, cost_function=CostFunctions.CycleTime())

        count_fail = 0
        for _ in range(n_trials):  # Do a few times to have higher chance of fail
            t0 = time.time()
            # There is some randomness to finding a solution, so as long as it takes less than 1min, keep trying.
            solution_planned, reward, done, truncated, info = rob_env.step(solution.module_assembly)
            if time.time() - t0 > timeout * 1.3:  # Not all calcs captured in timeout, so add some buffer
                if info.get('timeout', False):
                    continue
                self.fail(f"rob_env took way longer than timeout {time.time() - t0}s with timeout {timeout}s.")
            if info['success']:
                self.assertTrue(done)
                self.assertGreater(reward, rob_env.reward_fail)
                self.assertTrue(info['success'])
                self.assertFalse(truncated)
            else:
                count_fail += 1
                self.assertEqual(reward, rob_env.reward_fail)
                if info.get('filter_fail', False):
                    self.assertIsNone(solution_planned)
        logging.info(f"rob_env failed {count_fail} times out of {n_trials} trials.")


if __name__ == '__main__':
    unittest.main()
