import math
import unittest

import numpy as np
import numpy.testing as np_test
from timor.utilities import logging

from mcs.utilities.trajectory import from_kunz_trajectory


class TestTrajectory(unittest.TestCase):
    """Test trajectory utils of mcs."""

    def test_KunzTrajectory(self, num_tests=100):
        """Test kunz trajectory."""
        t = from_kunz_trajectory([np.asarray((1., 1.)), np.asarray((0., 0.))], 0.01,
                                 np.asarray((2., 2.)), np.asarray((2., 2.)))
        self.assertLessEqual(0., np.min(t.t))
        # Manually calc: Pure a/deceleration - 2*0.5 a_max t^2; here t = sqrt(2)
        self.assertLessEqual(np.max(t.t), math.sqrt(2) * 1.01)

        count_invalid = 0
        for _ in range(num_tests):
            rand_path = np.random.random((10, 8))  # 8 intermediate points, 8 DoF
            rand_allowed_distance = 10 ** np.random.randint(-4, -1)
            v_lim = 1 + np.random.random((8,))
            a_lim = 1 + np.random.random((8,))
            try:
                t = from_kunz_trajectory(rand_path, 1e-4, v_lim, a_lim, rand_allowed_distance)
            except ValueError:
                logging.info(f"Invalid combination: {rand_path}, {rand_allowed_distance}, {v_lim}, {a_lim}")
                count_invalid += 1
                continue

            # Add slight leeway such that equal is accepted
            np_test.assert_array_less(np.max(np.abs(t.dq), axis=0), v_lim * 1.001)
            np_test.assert_array_less(np.max(np.abs(t.ddq), axis=0), a_lim * 1.001)

            # Check that integration kind of consistent
            self.assertLessEqual(np.linalg.norm(np.trapz(t.ddq, t.t, axis=0) - t.dq[-1, :]) / t.t.shape[0], 1e-6)
            self.assertLessEqual(np.linalg.norm(np.trapz(t.dq, t.t, axis=0) - t.q[-1, :]) / t.t.shape[0], 1e-3)

        self.assertLessEqual(count_invalid / num_tests, 0.01)

    def test_kunz_trajectory_180_turn(self, tries=10):
        """Test that kunz trajectory can handle multiple 180° turns in one trajectory."""
        for _ in range(tries):
            q_start = np.random.random((2,))
            q_mid = np.random.random((2,))
            t = from_kunz_trajectory(np.asarray((q_start, q_mid, q_start, q_mid)),
                                     0.01, np.asarray((2., 2.)), np.asarray((2., 2.)))
            self.assertIsNotNone(t)
            np_test.assert_almost_equal(t.q[0, :], q_start)
            np_test.assert_almost_equal(t.q[-1, :], q_mid, decimal=4)
            np_test.assert_almost_equal(t.q[int(len(t) / 3), :], q_mid, decimal=4)
            np_test.assert_almost_equal(t.q[int(2 * t.t.shape[0] / 3), :], q_start, decimal=4)

    def test_close_to_via_points(self):
        """Make sure that if via_points_deviation is set they are actually passed closely."""
        via_points = [np.asarray((1., 1.)), np.asarray((0., 0.)), np.asarray((-1., 1.))]
        t = from_kunz_trajectory(via_points, 0.01,
                                 np.asarray((2., 2.)), np.asarray((2., 2.)), max_deviation=1e-3)
        t_replace = from_kunz_trajectory(via_points, 0.01,
                                         np.asarray((2., 2.)), np.asarray((2., 2.)), max_deviation=1e-3,
                                         max_via_points_deviation=1e-3, fix_via_point='replace')
        t_insert = from_kunz_trajectory(via_points, 0.01,
                                        np.asarray((2., 2.)), np.asarray((2., 2.)), max_deviation=1e-3,
                                        max_via_points_deviation=1e-3, fix_via_point='insert')
        self.assertLessEqual(1e-3, np.linalg.norm(t.q - via_points[1], axis=1).min())
        for via_point in via_points:
            self.assertLessEqual(np.linalg.norm(t_insert.q - via_point, axis=1).min(), 1e-3 + 1e-10)
            self.assertLessEqual(np.linalg.norm(t_replace.q - via_point, axis=1).min(), 1e-3 + 1e-10)

        self.assertEqual(t_replace.t.shape[0], t.t.shape[0])
        self.assertLessEqual(t_replace.t.shape[0], t_insert.t.shape[0])

        # ensure last one hit
        t = from_kunz_trajectory(via_points, 1.,
                                 np.asarray((2., 2.)), np.asarray((2., 2.)))
        self.assertFalse(np.allclose(t.q[-1, :], via_points[-1], atol=1e-3))
        t = from_kunz_trajectory(via_points, 1.,
                                 np.asarray((2., 2.)), np.asarray((2., 2.)),
                                 max_deviation=1e-3, max_via_points_deviation=1e-3)
        self.assertTrue(np.allclose(t.q[-1, :], via_points[-1], atol=1e-3))

        # make sure close together points at end don't fail
        t = from_kunz_trajectory(via_points + [via_points[-1] * 1.001], .1,
                                 np.asarray((2., 2.)), np.asarray((2., 2.)),
                                 max_deviation=1e-3, max_via_points_deviation=1e-3)
        self.assertTrue(np.allclose(t.q[-1, :], via_points[-1] * 1.001, atol=1e-3))

    def test_kunz_same_via_points(self):
        """Test handling of duplicate via points."""
        t = np.asarray(((1, 1), (2, 2), (2, 2), (3, 3)))
        traj = from_kunz_trajectory(t, 0.01, np.asarray((2., 2.)), np.asarray((2., 2.)))
        self.assertIsNotNone(traj)

        # Test that removing duplicates does not result in "fine" trajectories being deleted
        fine_t = np.asarray((1, 2, 3, 3 + 1e-6))
        traj = from_kunz_trajectory(fine_t.reshape((-1, 1)), 0.01, np.asarray((2.,)), np.asarray((2.,)))
        self.assertGreater(traj.q[-1, 0], 3)

    def test_kunz_single_time_step(self):
        """Test handling of resulting trajectories with single time step."""
        # Case of single via point should short circuit
        t = from_kunz_trajectory([np.asarray((1., 1.))], 0.01, np.asarray((2., 2.)), np.asarray((2., 2.)))
        self.assertIsNotNone(t)
        # Case of very short trajectory should be handled
        t = from_kunz_trajectory([np.asarray((1., 1.)), np.asarray((1., 1.)) + 1e-6],
                                 0.01, np.asarray((2., 2.)), np.asarray((2., 2.)))
        self.assertIsNotNone(t)
