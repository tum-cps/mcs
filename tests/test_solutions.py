#!/usr/bin/env python3
# Author: Jonathan Külz
# Date: 17.02.22
import json
import re
import unittest

from mcs.PathPlanner import PathPlanningFailedException
from timor import Module
from timor.configuration_search import LiuIterator
from timor.task import Solution, Task
from timor.utilities import errors, logging, spatial

from mcs import ConfigurationOptimizer
from .utils import get_tasks_solutions


class TestSolution(unittest.TestCase):
    """Test everything related to solutions here: are they valid, can we correctly identify valid solutions, ..."""

    def setUp(self) -> None:
        """Set up test file locations."""
        test_locations = get_tasks_solutions()
        self.task_files = test_locations["task_files"]
        self.task_dir = test_locations["task_dir"]
        self.solution_files = test_locations["solution_files"]
        self.anti_solution_files = test_locations["anti_solution_files"]

    def test_find_solutions(self):
        """Test if we can find solutions for all tasks with our current BruteForce Optimizer."""
        for description in self.task_files:
            if "Empty.json" in str(description):
                logging.info("Skipping Empty goal for now")
                continue
            task = Task.Task.from_json_file(description)
            db = Module.ModulesDB.from_name('PROMODULAR')

            base_pose = spatial.homogeneous(translation=[.25, -.25, 0])
            iterator = LiuIterator.Science2019(db, max_links_between_joints=1)
            optimizer = ConfigurationOptimizer.BruteForce(db, iterator, timeout=1)

            if task.id != 1:
                # TODO: Find a better optimizer. The current one will always fail
                try:
                    solution = optimizer.solve(task, base_pose=base_pose)
                except TimeoutError:
                    logging.warning(f"Timeout in solve task {task.id}: {task.name} in test_find_solutions.")
                    continue  # Solutions are not guaranteed currently
                except PathPlanningFailedException:
                    logging.warning(f"No path found in solve task {task.id}: {task.name} in test_find_solutions.")
                    continue
                except NotImplementedError:
                    logging.warning(
                        f"Optimizer not implemented for task {task.id}: {task.name} in test_find_solutions.")
                    continue
                self.assertEqual(solution.cost, -1. * len(task.goals), msg="For: " + str(description))
                # Reward of 1 for every goal
                solution.visualize()
            else:
                with self.assertRaises(TimeoutError, msg="For: " + str(description)):
                    solution = optimizer.solve(task, base_pose=base_pose)

    def test_predefined_solutions(self):
        """
        This serves as a test for solutions and constraints. There are positive and negative solutions.

        (Negative solution means solutions for which we expect a constraint to be hurt, so they raise)
        """
        for task_file in self.task_files:
            for solution_file in self.solution_files:
                # Task needs to be reset every time because the robot is added
                task = Task.Task.from_json_file(task_file)
                task_name = re.sub(r'\.json', '', task_file.name)
                if not re.match(task_name + r'(_\d)*\.json', solution_file.name):
                    continue  # Solution for another task
                solution = Solution.SolutionBase.from_json_file(solution_file, {task.id: task})
                self.assertTrue(solution.valid, f"Unexpected invalid: {solution_file}")
                solution.visualize()

            for solution_file in self.anti_solution_files[::-1]:
                task = Task.Task.from_json_file(task_file)
                task_name = re.sub(r'\.json', '', task_file.name)
                if not re.match(task_name + r'_[a-zA-Z]*.\.json', solution_file.name):
                    continue  # Solution for another task
                if 'validAssembly' in solution_file.name:
                    with self.assertRaises(errors.InvalidAssemblyError):
                        solution = Solution.SolutionBase.from_json_file(solution_file, {task.id: task})
                    continue
                solution = Solution.SolutionBase.from_json_file(solution_file, {task.id: task})
                self.assertFalse(solution.valid, f"Unexpected valid: {solution_file}")

    def test_simplified_empty_solutions(self):
        """
        Use Empty scenario to verify chain robot -> trajectory -> torques -> cost.

        Ensure that costs are either small or close together.
        """
        task = Task.Task.from_json_file([f for f in self.task_files if f.name == "Empty.json"][0])
        count = 0
        for solution_file in self.solution_files:
            if "Empty_" not in solution_file.name:
                continue
            count += 1
            solution = Solution.SolutionBase.from_json_file(solution_file, {task.id: task})
            with open(solution_file) as f:
                solution_content = json.load(f)
            abs_distance = abs(solution_content['cost'] - solution.cost)
            rel_distance = 1. - abs(solution_content['cost'] / solution.cost)
            self.assertTrue(abs_distance < 1e-4 or rel_distance < 1e-4,
                            msg=f"failed for {solution_file} with abs = {abs_distance} and rel = {rel_distance}")
        logging.info(f"Tested {count} simplified empty solutions")

    def test_verify_solution_on_predefined(self):
        """
        Tests the predefined solutions using the utility function verify_solution and makes sure

        the internal cost calculation matches the costs given in the solution.
        """
        tasks = [{"file": task_file,
                  "task": Task.Task.from_json_file(task_file)} for task_file in self.task_files]

        for solution_file in self.solution_files:
            sol_content = json.load(solution_file.open('r'))
            task_files = \
                [task['file'] for task in tasks
                 if task['task'].header.ID == sol_content['taskID']]

            if len(task_files) == 0:
                logging.warn("Skip solution {} as task {} not yet within test suite.".format(
                    solution_file, sol_content['taskID']))
                continue

            self.assertTrue(len(task_files) == 1, msg=f"No unique task found for solution {solution_file}.")
            task = Task.Task.from_json_file(task_files[0])
            solution = Solution.SolutionBase.from_json_file(solution_file, {task.id: task})

            self.assertTrue(solution.valid, msg="For: " + str(solution_file))

            # Make sure cost within 1% error or almost zero for the debug tasks based on Empty
            if "Empty_" in str(solution_file):
                if abs(sol_content['cost']) > 1e-4:
                    self.assertTrue(abs(1 - solution.cost / sol_content['cost']) < .01,
                                    msg="For: " + str(solution_file))
                else:  # For costs that are almost zero we require higher precision
                    self.assertAlmostEqual(solution.cost, 0, 4)

            # More leeway for the main solutions (1%)
            else:
                if abs(sol_content['cost']) > 1e-4:
                    self.assertTrue(abs(1 - solution.cost / sol_content['cost']) < .01,
                                    msg="For: " + str(solution_file))
                else:  # For costs that are almost zero we require higher precision
                    self.assertAlmostEqual(solution.cost, 0, 4)


if __name__ == '__main__':
    unittest.main()
