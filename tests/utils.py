from pathlib import Path
from typing import Dict, List, Union
import cobra


test_data = Path(__file__).parent.joinpath("data")


def get_tasks_solutions(search_string: str = "simple/", version="2022") -> Dict[str, Union[Path, List[Path]]]:
    """
    Gives access to all test tasks from the test_data folder.

    :param task_name: A regular expression pattern that is run against task files to be loaded
    :return: A dictionary mapping the "kind" of the task or solution file to the matching path.
    """
    task_uuids, _ = cobra.task.find_tasks(id_contains=search_string, version=version)
    task_files = [cobra.task.get_task(uuid=task_uuid) for task_uuid in task_uuids]
    assert all(task_files[0].parent == task_file.parent for task_file in task_files), \
        "All tasks must be in the same directory"

    solution_files = [cobra.solution.get_solution(sol_uuid)[0]
                      for task_uuid in task_uuids
                      for sol_uuid in cobra.solution.find_solutions(task_uuid=task_uuid)[0]]
    anti_solution_files = cobra.solution.get_negative_solutions()
    asset_dir = cobra.asset.ASSET_CACHE.joinpath(version).joinpath('assets')

    return {"task_files": task_files,
            "task_dir": task_files[0].parent,
            "solution_files": solution_files,
            "anti_solution_files": anti_solution_files,
            "asset_dir": asset_dir}
