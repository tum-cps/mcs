import unittest

import pytest

from mcs.PathPlanner import GoalStateInvalidError, StartStateInvalidError
import timor.utilities.prebuilt_robots


@pytest.mark.ompl
class OMPLPlannerTestCase(unittest.TestCase):
    """Check basic functionality of OMPL planner (creatable, rejects invalid start/goal)."""

    def setUp(self) -> None:
        """Setup assembly and a valid and invalid configurations."""
        self.assembly = timor.utilities.prebuilt_robots.get_six_axis_assembly()

        self.q_free = self.assembly.robot.configuration
        while self.assembly.robot.has_self_collision():
            self.q_free = self.assembly.robot.random_configuration()
            self.assembly.robot.update_configuration(self.q_free)

        while not self.assembly.robot.has_self_collision():
            self.q_coll = self.assembly.robot.random_configuration()
            self.assembly.robot.update_configuration(self.q_coll)

    def test_invalid_start_goal(self):
        """Test rejection of invalid start/goal configurations."""
        from mcs.OMPLPathPlanner import RrtcPlanner
        planner = RrtcPlanner(self.assembly.robot)

        with self.assertRaises(StartStateInvalidError):
            planner.find_path(self.q_coll, self.q_free)

        with self.assertRaises(GoalStateInvalidError):
            planner.find_path(self.q_free, self.q_coll)
