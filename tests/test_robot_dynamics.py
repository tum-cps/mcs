import math
import unittest

import numpy as np
import numpy.testing as np_test

from mcs.utilities.default_robots import get_six_axis_modrob_v2


class PinocchioRobotDynamicsSetup(unittest.TestCase):
    """Test if robots can instantiated, and works as expected."""

    def test_robot_id_fext(self):
        """
        Test if robot can calculate the inverse dynamics with external wrenches.

        So far only tests some STATIC cases for 0, or right sign,
        with and without gravity (including composing gravity and external wrench torques)
        TODO: This test should be ported to Timor eventually, but we can't use the modrob gen2 for that
        """
        # So far only tests some STATIC cases for 0, or right sign,
        # with and without gravity (including composing gravity and external wrench torques)
        robot = get_six_axis_modrob_v2().robot
        robot.model.gravity.setZero()  # Makes it easier to understand what is going on

        # fext
        tau = robot.id(np.zeros((6,)), np.zeros((6,)), np.zeros((6,)), eef_wrench=np.asarray((1., 0, 0, 0, 0, 0)))
        # Should all be close to 0 as force close to parallel to / intersecting joint axis
        np_test.assert_allclose(tau, 0, 1e-3, 1e-1)
        tau = robot.id(np.zeros((6,)), np.zeros((6,)), np.zeros((6,)), eef_wrench=np.asarray((0, 1., 0, 0, 0, 0)))
        # These joints don't have a lever / are parallel to force
        np_test.assert_allclose(tau[[0, 5]], 0, rtol=1e-3, atol=1e-1)
        # Test signs of torque (whether left / right
        np_test.assert_array_less(tau[[1, 3, 4]], 0)
        np_test.assert_array_less(0, tau[[2]])
        self.assertLess(abs(tau[3]), abs(tau[2]))  # same axis but longer lever
        self.assertLess(abs(tau[2]), abs(tau[1]))  # same axis but longer lever
        tau = robot.id(np.zeros((6,)), np.zeros((6,)), np.zeros((6,)), eef_wrench=np.asarray((0, 0, 1., 0, 0, 0)))
        # These joints don't have a lever / are parallel to force
        np_test.assert_allclose(tau, 0, 1e-3, 1e-1)

        # tau_ext
        tau = robot.id(np.zeros((6,)), np.zeros((6,)), np.zeros((6,)), eef_wrench=np.asarray((0, 0, 0, 1, 0, 0)))
        np_test.assert_allclose(tau[[1, 2, 3, 5]], 0)
        np_test.assert_allclose(tau[[0, 4]], -1.)
        tau = robot.id(np.zeros((6,)), np.zeros((6,)), np.zeros((6,)), eef_wrench=np.asarray((0, 0, 0, 0, 1, 0)))
        np_test.assert_allclose(tau, 0)  # All orthogonal
        tau = robot.id(np.zeros((6,)), np.zeros((6,)), np.zeros((6,)), eef_wrench=np.asarray((0, 0, 0, 0, 0, 1)))
        np_test.assert_allclose(tau[[1, 3, 5]], -1.)
        np_test.assert_allclose(tau[[2]], 1.)
        np_test.assert_allclose(tau[[0, 4]], 0.)

        # Test that world frame used
        tau = robot.id(np.asarray((0, math.pi / 2, 0, 0, 0, 0)), np.zeros((6,)), np.zeros((6,)),
                       eef_wrench=np.asarray((1., 0, 0, 0, 0, 0)))
        np_test.assert_allclose(tau[[0, 5]], 0, rtol=1e-3, atol=1e-1)
        np_test.assert_array_less(0, tau[[1, 3, 4]])
        np_test.assert_array_less(tau[[2]], 0)

        # Test composition with gravity - in static case superposition should hold
        for _ in range(100):
            q = robot.random_configuration()
            wrench = np.random.random((6,))
            robot.model.gravity.setZero()  # Makes it easier to understand what is going on
            tau_fext = robot.id(q, np.zeros((6,)), np.zeros((6,)),
                                eef_wrench=wrench)
            robot.model.gravity.setRandom()
            tau_with_grav_and_fext = robot.id(q, np.zeros((6,)), np.zeros((6,)),
                                              eef_wrench=wrench)
            tau_with_grav = robot.id(q, np.zeros((6,)), np.zeros((6,)))
            np_test.assert_allclose(tau_fext + tau_with_grav, tau_with_grav_and_fext)


if __name__ == '__main__':
    unittest.main()
