import contextlib
import io
from time import time
import unittest

import numpy as np
import cobra

from mcs.utilities.debug_solution import debug_solution
from timor.task import Goals
from timor.task.Solution import SolutionTrajectory
from timor.task.Task import Task
from timor.utilities import logging

from mcs.utilities.default_robots import get_six_axis_modrob_v2
from mcs.utilities.ik_helpers import create_ik_candidates_with_timeout
from .utils import get_tasks_solutions, test_data


class TestIKUtilities(unittest.TestCase):
    """Tests if IK utilities work as expected."""

    def setUp(self) -> None:
        """Setup test environment, esp. random seeds."""
        self.concert_task = Task.from_json_file(
            test_data.joinpath('tasks/CONCERT/Demo_1.json'),
            test_data.joinpath('tasks/assets/'))
        self.rng = np.random.default_rng(431)
        self.assembly = get_six_axis_modrob_v2()
        self.goals = {g_id: g for g_id, g in self.concert_task.goals_by_id.items() if isinstance(g, Goals.At)}
        self.assembly.robot.set_base_placement(
            self.concert_task.base_constraint.base_pose.nominal.in_world_coordinates())
        self.assembly.robot._rng = self.rng

    def test_ik_utils(self, timeout=5, n_trials=10):
        """Test that create_ik_candidates_with_timeout keeps timeout and produces valid and unique IK candidates."""
        count_success = 0
        for _ in range(n_trials):
            t0 = time()
            try:
                iks = create_ik_candidates_with_timeout(self.assembly.robot, self.goals,
                                                        timeout=timeout, minimum_search_time=timeout / 2,
                                                        min_ik_count=2, rng=self.rng,
                                                        ik_kwargs=dict(task=self.concert_task))
            except TimeoutError:
                self.assertGreater(time() - t0, timeout)  # Make sure timeouts thrown correctly
                self.assertGreater(1.5 * timeout, time() - t0)  # Make sure timeout not totally off
                continue
            else:
                self.assertLessEqual(time() - t0, timeout * 1.1)
                count_success += 1
                self.assertEqual(iks.keys(), self.goals.keys())
                for g_id in self.goals.keys():
                    self.assertGreaterEqual(len(iks[g_id]), 2)

            for g_id, qs in iks.items():
                for q in qs:
                    self.assertTrue(self.goals[g_id].goal_pose.valid(self.assembly.robot.fk(q)))
        self.assertGreater(count_success, 0, "No successful IK candidates found; weird ...")
        logging.info(f"create_ik_candidates_with_timeout succeeded {count_success} times out of {n_trials} trials.")

    def test_small_timeout(self):
        """Ensure small timeout kept"""
        with self.assertRaises(TimeoutError):
            create_ik_candidates_with_timeout(self.assembly.robot, self.goals, timeout=1e-3, min_ik_count=1,
                                              rng=self.rng, ik_kwargs=dict(task=self.concert_task))

    def test_trivial_edge_cases(self):
        """Test edge cases with trivial solutions"""
        robot = self.assembly.robot

        # Make sure no solutions found if none desired
        solutions = create_ik_candidates_with_timeout(robot, self.goals, timeout=np.inf,
                                                      min_ik_count=0, max_ik_count=0, minimum_search_time=0)
        self.assertEqual(solutions, {g_id: [] for g_id in self.goals.keys()})

    def test_seeding(self, timeout=5):
        """Test that ik utils are seed-able"""
        iks_per_seed = []
        while len(iks_per_seed) < 2:
            rng = np.random.default_rng(44)
            self.assembly.robot._rng = rng
            try:
                iks_per_seed.append(create_ik_candidates_with_timeout(
                    self.assembly.robot, self.goals, timeout=timeout, min_ik_count=1, rng=rng,
                    ik_kwargs=dict(task=self.concert_task), minimum_search_time=timeout / 5))
            except TimeoutError:
                continue
        self.assembly.robot._rng = self.rng  # reset rng for robot assembly

        # self.assertEqual(iks_per_seed[0], iks_per_seed[1])  # If ik ever deterministic given seed


class TestSolutionDebug(unittest.TestCase):
    """Tests if solution debug utilities work as expected."""

    def setUp(self):
        """Setup test environment, esp. random seeds."""
        self.task = Task.from_json_file(cobra.task.get_task(id="simple/PTP_1", version="2022"))

    def test_debug_output(self):
        """Test that debug works on common problems."""
        # Missing goal
        sol_missing_goal = SolutionTrajectory.from_json_file(
            next(x for x in get_tasks_solutions()['anti_solution_files'] if 'PTP_1_at' in str(x)),
            tasks={self.task.id: self.task})
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            debug_solution(sol_missing_goal)
        self.assertIn("Goal 1 fulfilled: False", f.getvalue())
        self.assertIn("AllGoalsFulfilled'> fulfilled: False", f.getvalue())

        # Self collision
        sol_self_collision = SolutionTrajectory.from_json_file(
            next(x for x in get_tasks_solutions()['anti_solution_files'] if 'PTP_1_selfColl' in str(x)),
            tasks={self.task.id: self.task})
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            debug_solution(sol_self_collision)
        self.assertIn("CollisionFree'> fulfilled: False", f.getvalue())
        self.assertIn("Fails at t = ", f.getvalue())
