import random
import time
import unittest

import numpy as np
import pytest

from mcs.PathPlanner import PathPlanningFailedException
from mcs.utilities.debug_solution import debug_solution
from mcs.utilities.trajectory import TrajectoryGenerationError
from timor.configuration_search import AssemblyFilter
from timor.task import Constraints
from timor.task.CostFunctions import CycleTime
from timor.task.Solution import SolutionHeader, SolutionTrajectory
from timor.task.Task import Goals, Task
import timor.utilities.logging as logging
from timor.utilities.transformation import Transformation

from mcs.TaskSolver import SimpleHierarchicalTaskSolver, SimpleTaskSolver
from mcs.utilities.default_robots import get_six_axis_modrob_v2
from .utils import get_tasks_solutions, test_data


@pytest.mark.ompl
class TestTaskSolver(unittest.TestCase):
    """Tests task solver, esp. if they find solutions on known solvable tasks and robots."""

    def setUp(self) -> None:
        """Setup test environment, esp. random seeds."""
        self.simple_tasks = tuple(Task.from_json_file(
            test_task,
            get_tasks_solutions()['asset_dir']) for test_task in get_tasks_solutions()['task_files'])
        self.concert_task = Task.from_json_file(
            test_data.joinpath('tasks/CONCERT/Demo_1.json'),
            test_data.joinpath('tasks/assets/'))
        self.assembly = get_six_axis_modrob_v2()
        self.assembly_medium = get_six_axis_modrob_v2(length='medium')
        random.seed(431)
        np.random.seed(431)
        import ompl.util  # Only import once ompl ensured by pytest mark
        ompl.util.RNG.setSeed(431)  # Set seed used to generate all OMPL random number generator seeds

    def test_task_solver_init(self):
        """Test task solver initialisation"""
        solver = SimpleTaskSolver(self.concert_task, timeout=30, safety_margin=0.1)
        self.assertIsNotNone(solver)
        self.assertIsNot(solver.task, self.concert_task)  # Should be a copy
        # Check that safety margin is set correctly
        self.assertEqual(solver.safety_margin, solver._collision_free_constraint.safety_margin)
        for c in solver.task.constraints:
            if type(c) is Constraints.CollisionFree:  # only hit that single collision free constraint
                self.assertEqual(solver.safety_margin, c.safety_margin)
        for c in self.concert_task.constraints:
            if isinstance(c, Constraints.CollisionFree):
                self.assertNotEquals(solver.safety_margin, c.safety_margin)

    def test_hierarchical_task_solver(self):
        """Test that task solver able to solve some of the simple example tasks."""
        robot_filter = AssemblyFilter.RobotCreationFilter()
        ik_filter = AssemblyFilter.InverseKinematicsSolvable(skip_not_applicable=True)
        static_torque_filter = AssemblyFilter.StaticTorquesValid(skip_not_applicable=False)
        retries = 10
        times = {}
        timing_tasks = []
        task2assembly = {  # Default is still long self.assembly
            'simple/PTP_pause': self.assembly_medium,
            'simple/PTP_3': self.assembly_medium
        }
        for task in self.simple_tasks:
            if task.id not in ('simple/PTP_2', 'simple/PTP_1', 'simple/Empty', 'simple/PTP_3', 'simple/PTP_pause'):
                # TODO: Implement ReturnTo goal -> 'simple/PTP_2_cycle'
                # TODO: Implement Leave goal -> 'simple/PTP_leave'
                # TODO: Make sure interpolated trajectory works out -> simple/traj_1|window
                continue
            logging.info(f"Testing task: {task.id}")
            timing_tasks.append(task)
            t0 = time.time()
            trajectory = None
            solution = None
            task_solver = SimpleHierarchicalTaskSolver(task, filters=(robot_filter, ik_filter, static_torque_filter),
                                                       timeout=60, time_limit_per_goal=25,
                                                       trajectory_resolution=task.header.timeStepSize)
            assembly = task2assembly.get(task.id, self.assembly)
            for i in range(retries):
                try:
                    trajectory = task_solver.solve(assembly)
                except (ValueError, TimeoutError, PathPlanningFailedException) as e:
                    logging.info(f"Retry {i+1} for {task.id}; solver error {e}")
                    logging.info(e)
                    continue
                if trajectory is None:
                    logging.info(f"Retry {i+1} for {task.id}; no trajectory")
                    continue

                solution = SolutionTrajectory(trajectory, SolutionHeader(taskID=task.header.ID), task,
                                              assembly, CycleTime(), assembly.robot.placement)
                if not solution.valid:
                    debug_solution(solution)
                    continue
                break
            self.assertIsNotNone(trajectory, msg=f"Trajectory None for {task.id} after {retries} retries")
            self.assertTrue(solution.valid, msg=f"Solution invalid for {task.id} after {retries} retries")
            times[task.id] = time.time() - t0

        for task in timing_tasks:
            t0 = time.time()
            for goal in (goal for goal in task.goals if isinstance(goal, Goals.At)):
                # Make it impossible to reach the goal
                goal.goal_pose.nominal._transformation = Transformation.from_translation(100 * goal.goal_pose[:3, 3])
            task_solver = SimpleHierarchicalTaskSolver(
                task, filters=(robot_filter, ik_filter, static_torque_filter), timeout=30)
            # Should be None as failing during filters
            trajectory = task_solver.solve(task2assembly.get(task.id, self.assembly))
            if task.id == 'simple/Empty':  # trivial, empty task. Trajectory should be starting position
                self.assertIsNotNone(trajectory, msg=f"Trajectory None for {task.id}")
                continue
            self.assertIsNone(trajectory, msg=f"Trajectory None for {task.id}")
            self.assertLess(time.time() - t0, times[task.id],
                            msg=f"Rejection should be faster for {task.id}; {time.time() - t0} vs times[task.id]")
            self.assertEqual(task_solver.filters[1].failed, 1, msg=f"First filter should fail for {task.id}")

    def test_simple_task_solver(self):
        """Test simple task solver"""
        task_solver = SimpleTaskSolver(self.concert_task, timeout=30)
        self.assertIsNotNone(task_solver)

        max_chances_to_solve = 2  # Inverse kinematics can sometimes fail
        traj = None
        attempt = 0
        for attempt in range(max_chances_to_solve):
            try:
                traj = task_solver.solve(self.assembly)
                break
            except (PathPlanningFailedException, TrajectoryGenerationError, TimeoutError) as e:
                logging.info(f"Failed with {e}; retry {attempt + 1} ...")
                self.assembly.robot.update_configuration(np.asarray((0, 0, 0, 0, 0, 0)))  # reset start

        self.assertIsNotNone(traj)
        if attempt > 0:
            logging.warning(f"Task solver needed {attempt + 1} attempts to solve the demo task.")
        sol = SolutionTrajectory(traj, SolutionHeader(taskID=self.concert_task.header.ID),
                                 self.concert_task, self.assembly, CycleTime(), self.assembly.robot.placement)

        if not sol.valid:
            debug_solution(sol)
            self.fail()
        self.assertGreater(sol.cost, 0)
        for goal in self.concert_task.goals:
            self.assertTrue(goal.achieved(sol))

    def test_solve_goals_together_has_shorter_path(self, n_tries=5):
        """Test that solve_goals_together has shorter path than solve_goals_separately"""
        task_solver_sequential = SimpleTaskSolver(self.concert_task, timeout=30, solve_goals_sequentially=True)
        traj_sequential = []
        plan_time_sequential = []
        # Set to mean run time of sequential solver that is less greedy and can return well before the timeout
        task_solver = SimpleTaskSolver(self.concert_task, timeout=10)
        traj_jointly = []
        plan_time_jointly = []

        for _ in range(n_tries):
            t0 = time.time()
            try:
                traj = task_solver.solve(self.assembly)
            except (PathPlanningFailedException, TrajectoryGenerationError, TimeoutError) as e:
                logging.info(f"Joint failed with {e}")
            else:
                plan_time_jointly.append(time.time() - t0)
                traj_jointly.append(traj)

            t0 = time.time()
            try:
                traj = task_solver_sequential.solve(self.assembly)
            except (PathPlanningFailedException, TrajectoryGenerationError, TimeoutError) as e:
                logging.info(f"Sequential failed with {e}")
            else:
                plan_time_sequential.append(time.time() - t0)
                traj_sequential.append(traj)

        print(f"Success rate jointly: {len(traj_jointly)}/{n_tries}; "
              f"success rate sequentially: {len(traj_sequential)}/{n_tries}")
        print("Average length jointly: ", np.mean([t.t[-1] for t in traj_jointly]), "Average length sequentially: ",
              np.mean([t.t[-1] for t in traj_sequential]))
        print("Average planning time jointly: ", np.mean(plan_time_jointly), "s; Average planning time sequentially: ",
              np.mean(plan_time_sequential), "s")
        self.assertGreater(len(traj_jointly), 0)
        self.assertGreater(len(traj_sequential), 0)
        self.assertGreaterEqual(np.mean([t.t[-1] for t in traj_sequential]), np.mean([t.t[-1] for t in traj_jointly]))


if __name__ == '__main__':
    unittest.main()
