import importlib.util
from pathlib import Path
import random
import tempfile
import unittest
import urllib.request

import cobra.task.task
import numpy as np
from timor import Task


def run_main(file: Path):
    """
    Imports a single python file and runs its main.

    Taken from: https://stackoverflow.com/questions/67631/how-to-import-a-module-given-the-full-path
    """
    spec = importlib.util.spec_from_file_location("imported", file)
    script = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(script)
    script.main()


class TestVisualization(unittest.TestCase):
    """
    Place to test all visualization related stuff.
    """

    def setUp(self) -> None:
        """
        Setup random generators.
        """
        random.seed(0)
        np.random.seed(0)

    def test_vis_task(self):
        """
        Test html visualization
        """
        task = Task.Task.from_json_file(cobra.task.get_task(id="simple/PTP_2"))
        with tempfile.NamedTemporaryFile("w") as html_file:
            vis = task.visualize()
            html_file.write(vis.viewer.static_html())

            html = urllib.request.urlopen("file://" + str(html_file.name))
            self.assertIsNotNone(html)


if __name__ == '__main__':
    unittest.main()
