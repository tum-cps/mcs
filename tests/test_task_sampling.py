import itertools
import unittest

import numpy as np
import numpy.testing as np_test
from scipy.spatial.distance import cdist
from timor import Transformation
from timor.task import Goals, Tolerance
from timor.utilities.tolerated_pose import ToleratedPose

from mcs.optimize.Sampler import CartesianSampler, GridIndexSampler, CartesianGridSampler, FiniteSetSampler, \
    GridSampler, TransformationGridSampler, SphericalSampler
from mcs.optimize.TaskGenerator import GoalBasedTaskGenerator, ObstacleSampler, VoxelizedTaskGenerator


class TestSamplers(unittest.TestCase):
    """Test cases for spatial, task and obstacle samplers."""

    seed = 99

    def test_goal_based_task_generator(self):
        """Test goal generation and random seed functionality"""
        N = 10
        rng = np.random.default_rng(self.seed)
        gen = GoalBasedTaskGenerator(n_goals=N, rng=rng)

        tasks = []
        for i, task in enumerate(gen):
            tasks.append(task)
            self.assertEqual(len(task.goals), N)
            for goal in task.goals:
                self.assertIsInstance(goal, Goals.Reach)
            if i >= N:
                break

        # Check that generators with equal random seeds yield the same tasks
        rng = np.random.default_rng(self.seed)
        gen_2 = GoalBasedTaskGenerator(n_goals=N, rng=rng, goals_ordered=True)
        for i, task in enumerate(gen_2):
            ref = tasks[i]
            self.assertEqual(task.id, ref.id)
            for g1, g2 in zip(task.goals, ref.goals):
                self.assertEqual(g1.id, g2.id)
                self.assertEqual(g1.goal_pose.nominal, g2.goal_pose.nominal)
            for o1, o2 in zip(task.obstacles, ref.obstacles):
                self.assertEqual(o1, o2)
            if i >= N:
                break

            self.assertGreater(len(task.constraints), len(tasks[i].constraints))

        # Also, if we don't reset the seed, we should expect different tasks
        gen_3 = GoalBasedTaskGenerator(n_goals=N, rng=rng, goals_ordered=True)
        for i, task in enumerate(gen_3):
            ref = tasks[i]
            self.assertNotEqual(task.id, ref.id)
            for g1, g2 in zip(task.goals, ref.goals):
                self.assertNotEqual(g1.id, g2.id)
            if i >= N:
                break

    def test_grid_index_sampler_values(self):
        """Test grid sampler."""
        for n in range(1, 20):
            grid = GridIndexSampler(grid_size=np.repeat(n, (3,)))
            for _ in range(10):
                idx_sample = grid.sample()
                for coordinate in idx_sample:
                    self.assertLess(coordinate, n)
                    self.assertGreaterEqual(coordinate, 0)
                    self.assertIsInstance(coordinate, int)

    def test_grid_index_sampler_distribution(self):
        """Test the distribution of samples from a grided index sampler."""
        grid_size = 500
        grid_index_sampler = GridIndexSampler((grid_size, grid_size))
        n_diff_samples = len(grid_index_sampler)

        # Make sure deterministic sampling works
        self.assertEqual(tuple(itertools.islice(grid_index_sampler.as_finite_iterable(randomize=False), grid_size)),
                         tuple(itertools.islice(grid_index_sampler.as_finite_iterable(randomize=False), grid_size)))
        # Check that the length of available samples is correct
        self.assertEqual(grid_size ** 2, len(list(grid_index_sampler.as_finite_iterable(randomize=False))))
        self.assertEqual(grid_size ** 2, len(list(grid_index_sampler.as_finite_iterable(randomize=True))))

        # Take 10% and 99% of all indices, randomly sampled, once exhaustive and once with replacement
        for n_test in (int(0.1 * n_diff_samples), int(0.99 * n_diff_samples)):
            vals_iter = tuple(itertools.islice(grid_index_sampler.as_finite_iterable(), n_test))
            vals_sample = np.asarray([grid_index_sampler.sample() for _ in range(n_test)])

            # tell apart by coverage of all indices
            bc_iter = np.bincount(tuple(v[0] + grid_size * v[1] for v in vals_iter))
            bc_sample = np.bincount(tuple(v[0] + grid_size * v[1] for v in vals_sample))
            # As long as drawn less than n_diff_samples each should be unique
            self.assertEqual(n_test, np.sum(bc_iter == 1))
            np_test.assert_array_equal(0., bc_iter[bc_iter != 1])  # all others are 0
            # Chance that fails is prod_{i=0}^n_test 1 - i/n_diff_samples (chance of drawing n_test unique samples)
            if np.prod(1 - np.arange(0, n_test) / n_diff_samples) < 1e-4:  # Make sure this is unlikely to fail
                self.assertNotEqual(n_test, np.sum(bc_sample == 1))
            # Check std. deviation of number hit
            # For bc_iter not interesting as either 0 or 1 and fully checked by counting 1's
            p = 1 / n_diff_samples
            std_expected_multinomial = np.sqrt(n_test * p * (1 - p))
            np_test.assert_array_almost_equal(std_expected_multinomial, np.std(bc_sample), decimal=2)

            # Index vice distribution
            bc_0_iter = np.bincount(tuple(v[0] for v in vals_iter))  # Cannot happen more often than grid_size
            np_test.assert_array_less(bc_0_iter, grid_size + 1)  # Cannot do less-equal; +1 ok as integer

            bc_0_sample = np.bincount(tuple(v[0] for v in vals_sample))  # Might happen more often than grid_size
            self.assertEqual(np.mean(bc_0_sample), n_test / grid_size)
            p = 1 / grid_size
            std_expected_multinomial_1D = np.sqrt(n_test * p * (1 - p))
            np_test.assert_array_almost_equal(std_expected_multinomial_1D, np.std(bc_0_sample), decimal=0)

    def test_grid_sampler(self):
        """Test sampler working on equidistant grids."""
        grid_size = 1000
        grid_width = .25
        N_test = 1000
        grid_sampler = GridSampler((grid_size, grid_size), grid_width)

        self.assertListEqual(
            list(zip(*itertools.islice(grid_sampler.as_finite_iterable(randomize=False), N_test))),
            list(zip(*itertools.islice(grid_sampler.as_finite_iterable(randomize=False), N_test))))
        self.assertEqual(grid_size ** 2, len(list(grid_sampler.as_finite_iterable(randomize=False))))
        self.assertEqual(grid_size ** 2, len(list(grid_sampler.as_finite_iterable(randomize=True))))

        # Ensure in line with underlying index
        for sample, _ in zip(itertools.repeat(grid_sampler.sample()), range(N_test)):
            for s in sample:
                self.assertLessEqual(-(grid_size / 2) * grid_width, s)
                self.assertLess(s, (grid_size / 2) * grid_width)

        # Distributions are same as grid index sampler but more expensive to test due to floats instead of int

    def test_grid_cartesian_sampling(self):
        """Tests the grid sampler, especially the discrete sampling"""
        N = 100000
        width = 1.
        grid = (2, 3, 4)
        grid = CartesianGridSampler(grid_size=grid, spacing=width, offset=Transformation.random())
        samples = tuple(tuple(grid.sample_translation()) for _ in range(N))
        self.assertEqual(len(set(samples)), 2 * 3 * 4)

        for grid_size in range(1, 20):
            grid = CartesianGridSampler(grid_size=grid_size, spacing=width)
            leftmost = 0
            for i in range(grid_size):
                continuous_position = grid.get_continuous_coordinate(i, grid_size, width)
                continuous_position_vector = grid.grid_idx2grid_value(np.asarray((0, 0, i)))
                if i == 0:
                    leftmost = continuous_position
                elif i == grid_size - 1:
                    self.assertAlmostEqual(-leftmost, continuous_position)
                    self.assertAlmostEqual(-leftmost, continuous_position_vector[-1])
                    self.assertAlmostEqual(leftmost, continuous_position_vector[0])

                # Check that the grid is centered around 0
                if grid_size % 2 == 0:
                    self.assertAlmostEqual(continuous_position % width, width / 2)
                else:
                    self.assertAlmostEqual(continuous_position % width, 0)

            # Grid volume is volume between samples + boundary (volume of the discretized space)
            self.assertAlmostEqual(grid.volume, width ** 3 * grid_size ** 3)

        # Some uniformness testing
        grid_size = 20
        grid = CartesianGridSampler(grid_size=(grid_size, grid_size, grid_size), spacing=.1)
        n_diff_samples = len(grid)
        n_samples = int(len(grid) / 16)
        vals_iter = tuple(itertools.islice(grid.as_finite_iterable(), n_samples))
        vals_sample = np.asarray([grid.sample() for _ in range(n_samples)])

        # Counter result would have chance \prod_{i=0}^{n_samples} 1-i/|grid| (draw n_samples unique samples)
        if np.prod(1 - np.arange(0, n_samples) / n_diff_samples) < 1e-4:  # Make sure this is unlikely to fail
            self.assertTrue(any(np.allclose(a.translation, b.translation)
                                for a, b in itertools.combinations(vals_sample, 2)))  # Rotation is random

        # Exhaustive iter cannot have duplicates if n_samples < n_diff_samples
        self.assertFalse(any(np.allclose(a.translation, b.translation)
                             for a, b in itertools.combinations(vals_iter, 2)))  # Rotation is random

    def test_obstacle_sampling(self):
        """Tests the automatic generation of obstacles"""
        transformation_sampler = SphericalSampler()
        V_sampling = transformation_sampler.volume

        v_min, v_max = 0.06, 0.15
        n = 5
        occupation = .6
        kwargs = {'min_volume': v_min, 'max_volume': v_max}
        n_sampler = ObstacleSampler(transformation_sampler, **kwargs, n_obstacles=n)
        relative_sampler = ObstacleSampler(transformation_sampler, **kwargs, relative_volume_occupied=occupation)

        n_obstacles = n_sampler.sample()
        relative_obstacles = relative_sampler.sample()
        rng = np.random.default_rng(self.seed)

        for _ in range(100):
            v_desired = rng.uniform(v_min, v_max)
            box = n_sampler._sample_box(v_desired)
            cylinder = n_sampler._sample_cylinder(v_desired)
            sphere = ObstacleSampler._sample_sphere(v_desired)
            for geometry in (box, cylinder, sphere):
                self.assertAlmostEqual(geometry.measure_volume, v_desired)

        self.assertEqual(len(n_obstacles), n)
        self.assertLessEqual(sum(o.collision.measure_volume for o in relative_obstacles),
                             V_sampling * occupation)
        self.assertGreaterEqual(sum(o.collision.measure_volume for o in relative_obstacles),
                                V_sampling * occupation - v_min)

        for o in n_obstacles | relative_obstacles:
            self.assertGreaterEqual(o.collision.measure_volume, v_min)
            self.assertLessEqual(o.collision.measure_volume, v_max)

        task_sampler = GoalBasedTaskGenerator(n_goals=1, obstacle_sampler=n_sampler)
        for i, task in enumerate(task_sampler):
            self.assertEqual(len(task.obstacles), n)
            if i >= 10:
                break

    def test_spatial_samplers(self):
        """Checks the samplers, especially on the uniformity of the samples"""
        n_uniformity = 10000
        n_sanity_check = 1000

        # Test uniform cartesian sampling
        sampler = CartesianSampler()
        samples = np.array([sampler.sample().homogeneous for _ in range(n_uniformity)])
        translations = samples[:, :3, 3]
        self.assertTrue(np.all(np.abs(translations) < 3 ** .5))
        dist = cdist(translations, translations)
        mask = np.ones((n_uniformity, n_uniformity), dtype=bool)
        mask[np.diag_indices(n_uniformity)] = False
        self.assertTrue(np.all(dist[mask] > 0))
        self.assertTrue(np.all(dist[mask] < 12 ** .5))
        # https://mathworld.wolfram.com/HypercubeLinePicking.html
        self.assertAlmostEqual(dist[mask].mean(), 2 * 0.661707182, delta=.01)  # Average distance in a unit cube

        # Test limited sampler - for the sake of simplicity, we sample integers instead of transformations
        limited = FiniteSetSampler(sampler, 1)
        the_one_sample = limited.sample()
        for _ in range(n_sanity_check):
            self.assertEqual(limited.sample(), the_one_sample)
        self.assertEqual(1, len(limited.as_finite_iterable()))
        self.assertEqual(the_one_sample, limited.as_finite_iterable()[0])

        limited = FiniteSetSampler(range(10), n_sanity_check)
        lots_of_samples = set(limited.sample() for _ in range(n_sanity_check))
        self.assertEqual(max(lots_of_samples), 9)
        self.assertEqual(lots_of_samples, set(range(10)))
        limited = FiniteSetSampler(range(10), 5)
        for _ in range(n_sanity_check):
            self.assertLess(limited.sample(), 5)

        limited = FiniteSetSampler(range(n_sanity_check), n_sanity_check)
        def sample_random(): return tuple(limited.as_finite_iterable(randomize=True))
        def sample_deterministic(): return tuple(limited.as_finite_iterable(randomize=False))
        self.assertNotEqual(sample_random(), sample_random())
        self.assertNotEqual(sample_random(), sample_deterministic())
        self.assertSetEqual(set(sample_random()), set(sample_deterministic()))

        # Make sure FiniteSetSampler works without size on finite length iterables
        with self.assertRaises(ValueError):
            _ = FiniteSetSampler(itertools.repeat(range(10)))
        finite = FiniteSetSampler(range(10))
        self.assertSetEqual(set(range(10)), set(finite.as_finite_iterable()))

        n_different = 10 ** 5
        n_draw = 1000
        avg_distance = n_different / n_draw
        limited = FiniteSetSampler(range(n_different), n_different)
        self.assertListEqual(list(range(1000)),
                             list(itertools.islice(limited.as_finite_iterable(randomize=False), n_draw)))
        rand_samples_exhaustive = np.fromiter(itertools.islice(limited.as_finite_iterable(), n_draw), dtype=int)
        # If randomly drawn each sample should have ~50% of being smaller / bigger than the next
        self.assertGreater(np.count_nonzero(rand_samples_exhaustive[1:] < rand_samples_exhaustive[:-1]), n_draw * 0.4)
        self.assertGreater(np.count_nonzero(rand_samples_exhaustive[1:] > rand_samples_exhaustive[:-1]), n_draw * 0.4)
        # Random samples should on average be equally spaced
        rand_samples_exhaustive_sorted = np.sort(rand_samples_exhaustive)
        dist = rand_samples_exhaustive_sorted[1:] - rand_samples_exhaustive_sorted[:-1]
        self.assertGreater(np.mean(dist), 0.9 * avg_distance)
        self.assertGreater(1.1 * avg_distance, np.mean(dist))
        # Random samples should not be same; chance for single value being in other is 10^3 / 10^5 = 1/100
        # -> expect 10 shared
        expected_shared = n_draw * n_draw / n_different
        rand_samples_exhaustive_2 = np.fromiter(itertools.islice(limited.as_finite_iterable(), n_draw), dtype=int)
        shared = np.intersect1d(rand_samples_exhaustive, rand_samples_exhaustive_2)
        self.assertGreater(shared.shape[0], 1)
        self.assertGreater(2 * expected_shared, shared.shape[0])

        # Test uniform spherical sampling
        sampler = SphericalSampler()
        samples = np.array([sampler.sample().homogeneous for _ in range(n_uniformity)])
        translations = samples[:, :3, 3]
        self.assertTrue(np.all(np.linalg.norm(translations, axis=1) < 1))
        dist = cdist(translations, translations)
        mask = np.ones((n_uniformity, n_uniformity), dtype=bool)
        mask[np.diag_indices(n_uniformity)] = False
        self.assertTrue(np.all(dist[mask] > 0))
        self.assertTrue(np.all(dist[mask] < 2))

        # https://math.stackexchange.com/questions/167932/mean-distance-between-2-points-in-a-ball
        self.assertAlmostEqual(dist[mask].mean(), 36 / 35, delta=.01)  # Average distance in a unit sphere

        # Another check for homogeneous distribution: The # of contained sample points should be ~ to volume
        samples = np.array([sampler.sample().homogeneous for _ in range(n_uniformity * 20)])
        translations = samples[:, :3, 3]
        square_1 = np.array(((-.7, -.3, .01), (.1, .5, .5))).T
        square_2 = np.array(((-.3, 0., -.3), (.1, .2, .3))).T
        contains_1 = (translations >= square_1[:, 0]) & (translations <= square_1[:, 1])
        contains_2 = (translations >= square_2[:, 0]) & (translations <= square_2[:, 1])
        contains_1 = contains_1[:, 0] & contains_1[:, 1] & contains_1[:, 2]
        contains_2 = contains_2[:, 0] & contains_2[:, 1] & contains_2[:, 2]

        V = sampler.volume
        V1 = abs(np.prod(square_1[:, 1] - square_1[:, 0]))
        V2 = abs(np.prod(square_2[:, 1] - square_2[:, 0]))

        for (v1, v2), (n1, n2) in zip(
                itertools.combinations((V, V1, V2), 2),
                itertools.combinations((20 * n_uniformity, contains_1.sum(), contains_2.sum()), 2)
        ):
            self.assertAlmostEqual((v1 / v2) / (n1 / n2), 1, delta=.1)

        # Test offset
        r = .5
        z = 1.
        off = Transformation.from_translation((0, 0, z))
        sampler = SphericalSampler(limits=((0, r), (0, np.pi), (-np.pi, np.pi)), offset=off)
        for _ in range(int(n_uniformity ** .5)):  # Test a few samples
            sample = sampler.sample()
            self.assertLessEqual(off.distance(sample).translation_euclidean, r)
            self.assertGreaterEqual(sample.norm.translation_euclidean, z - r)

    def test_tolerated_pose_sampling(self):
        """Test sampling of tolerated poses."""
        tolerated_poses = [ToleratedPose(Transformation.from_translation((1., 2., 3.)),
                                         Tolerance.CartesianXYZ((-1, 1), (-2, 2), (-0.5, 0.5))),  # Symmetric tolerance
                           ToleratedPose(Transformation.from_translation((1., 2., 3.)),
                                         Tolerance.CartesianXYZ((-1, 0), (-2, 2), (0, 0.5)))]  # Asymmetric tolerance
        for tolerated_pose in tolerated_poses:
            # Test with upper limit on voxel spacing
            sampler = TransformationGridSampler.from_tolerated_pose(tolerated_pose, grid_width=.4)
            for _ in range(100):
                sample = sampler.sample()
                self.assertTrue(tolerated_pose.valid(sample),
                                f"Invalid sample: {sample.homogeneous} for {tolerated_pose}")

            # Make sure these are limited iterators
            for transformation in itertools.islice(sampler, 100):
                self.assertTrue(tolerated_pose.valid(transformation))
            for transformation in sampler.as_finite_iterable():
                self.assertTrue(tolerated_pose.valid(transformation))

            # Randomized iterator should not be same twice
            self.assertNotEqual(
                tuple(transformation.translation.tobytes() for transformation in sampler.as_finite_iterable()),
                tuple(transformation.translation.tobytes() for transformation in sampler.as_finite_iterable()))
            # Non randomized iterator should be same order
            self.assertEqual(
                tuple(transformation.translation.tobytes()
                      for transformation in sampler.as_finite_iterable(randomize=False)),
                tuple(transformation.translation.tobytes()
                      for transformation in sampler.as_finite_iterable(randomize=False)))

            # Test with set voxel count
            sampler = TransformationGridSampler.from_tolerated_pose(tolerated_pose, grid_size=3)
            self.assertEqual(len(tuple(sampler.as_finite_iterable())), 3 ** 3)
            sampler = TransformationGridSampler.from_tolerated_pose(tolerated_pose, grid_size=(2, 3, 4))
            self.assertEqual(len(tuple(sampler.as_finite_iterable())), 2 * 3 * 4)

    def test_whitman_task_generation(self):
        """Test generation of tasks similar to Whitman et al. 2020"""
        n_goals = (1, 3, 10)
        n_obstacles = (0, 3, 5)
        num_tasks_each = 5
        grid_size = 5
        spacing = 0.25
        offset = Transformation.from_translation((0, 0, 0.625))
        rng = np.random.default_rng(99)

        for n_goal, n_obstacle in itertools.product(n_goals, n_obstacles):
            generator = VoxelizedTaskGenerator(n_goal, n_obstacle, grid_size, spacing, offset, rng=rng)
            generator2 = VoxelizedTaskGenerator(n_goal, n_obstacle, grid_size, spacing, offset, rng=rng,
                                                goals_centered_in_voxels=False)

            for t1, t2 in zip(itertools.islice(generator, num_tasks_each),
                              itertools.islice(generator2, num_tasks_each)):
                self.assertEqual(len(t1.goals), n_goal)
                self.assertEqual(len(t2.goals), n_goal)
                self.assertEqual(len(t1.obstacles), n_obstacle)
                self.assertEqual(len(t2.obstacles), n_obstacle)

                positions = [g.goal_pose.nominal for g in t1.goals] + [o.collision.placement for o in t2.obstacles]
                for p1, p2 in itertools.combinations(positions, 2):
                    self.assertNotEqual(p1, p2)

                goal_positions_second = [g.goal_pose.nominal for g in t2.goals]
                self.assertFalse(any(p1 == p2 for p1, p2 in itertools.product(positions, goal_positions_second)))


if __name__ == '__main__':
    unittest.main()
