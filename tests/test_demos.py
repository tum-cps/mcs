import importlib.util
from pathlib import Path
import random
import unittest
from unittest.mock import patch

import numpy as np
import pytest
from timor.utilities import logging

from mcs.utilities.local_files import package


def run_main(file: Path):
    """
    Imports a single python file and runs its main.

    Taken from: https://stackoverflow.com/questions/67631/how-to-import-a-module-given-the-full-path
    """
    spec = importlib.util.spec_from_file_location("imported", file)
    script = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(script)
    script.main()


class TestAllDemos(unittest.TestCase):
    """
    Run all demos in the demo folder and quit if one throws an error.
    """

    def setUp(self) -> None:
        """
        Find all demos to run. Files with leading _ will be left out.
        """
        random.seed(1)
        np.random.seed(1)
        self.demo_base_folder = package.joinpath('demos/base')
        self.demo_base_scripts = tuple(file for file in self.demo_base_folder.iterdir()
                                       if (not file.name.startswith('_'))
                                       and file.name.endswith('.py')
                                       )
        self.demo_ompl_folder = package.joinpath('demos/ompl')
        self.demo_ompl_scripts = tuple(file for file in self.demo_ompl_folder.iterdir()
                                       if (not file.name.startswith('_'))
                                       and file.name.endswith('.py')
                                       )

    def _run_scripts(self, demo_scripts):
        """Run all given demo_scripts."""
        for script in demo_scripts:
            self.assertTrue(script.is_file())
            logging.info(f"Running {script}")
            with patch('builtins.input', return_value='y'):  # Default all input calls to 'y'
                run_main(script)

    def test_base_demos(self):
        """Test base demos."""
        logging.info(f"Check Base Demos: {self.demo_base_scripts}")
        self._run_scripts(self.demo_base_scripts)

    @pytest.mark.ompl
    def test_ompl_demos(self):
        """Test ompl demos."""
        import ompl.util  # Fix OMPL master seed
        ompl.util.RNG.setSeed(2)
        logging.info(f"Check OMPL Demos: {self.demo_ompl_scripts}")
        self._run_scripts(self.demo_ompl_scripts)


if __name__ == '__main__':
    unittest.main()
