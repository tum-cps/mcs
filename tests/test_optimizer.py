import multiprocessing
import random
import time
import unittest

import numpy as np
import pytest
from timor.task.Task import Task
import timor.utilities.logging as logging

from .utils import test_data
from mcs.ConfigurationOptimizer import UsePredefinedAssembly


class TestOptimizer(unittest.TestCase):
    """Test the configuration optimizer, especially that they can solve known easily solvable."""

    def setUp(self) -> None:
        """Setup test environment, esp. random seeds."""
        self.task = Task.from_json_file(
            test_data.joinpath('tasks/CONCERT/Demo_1.json'),
            test_data.joinpath('tasks/assets/'))
        self.timeout = 30
        random.seed(1)
        np.random.seed(1)

    @pytest.mark.ompl
    def test_using_predefined_robot(self):
        """Test if the optimizer can solve a task with a predefined robot."""
        import ompl.util  # Fix OMPL master seed
        ompl.util.RNG.setSeed(1)
        optimizer = UsePredefinedAssembly()
        t0 = time.time()
        solution = None
        while time.time() - t0 > self.timeout:
            try:
                solution = optimizer.solve(self.task)
                self.assertTrue(solution.valid)
            except ValueError as e:
                # Only valid exceptions that might be thrown
                self.assertTrue("IK for goal" in str(e)
                                or "No valid solution found" in str(e)
                                or "Path could not be found" in str(e))
                self.assertTrue(len(multiprocessing.active_children()) == 0)

        if solution is None:
            logging.warning(f"Did not find solution for task {self.task.id}")


if __name__ == '__main__':
    unittest.main()
