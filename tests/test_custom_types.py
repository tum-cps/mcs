import random
import unittest

import numpy as np
from timor.configuration_search.AssemblyFilter import IntermediateFilterResults, OverwritesIntermediateResult


class CustomTypeUnitTests(unittest.TestCase):
    """Tests dtypes in utilities."""

    def setUp(self) -> None:
        """Fix random seeds to have deterministic tests"""
        random.seed(123)
        np.random.seed(123)

    def test_IntermediateResults(self):
        """Test that intermediate results work as expected (overwrite protection, typing)."""
        ir = IntermediateFilterResults()
        with self.assertRaises(TypeError):
            ir['custom_attribute'] = None
        with self.assertRaises(OverwritesIntermediateResult):
            ir.q_goal_pose = {'my_goal': (1, 2, 3)}
        ir.q_goal_pose['first'] = (1, 2, 3)
        ir.q_goal_pose['second'] = (1, 2, 3)
        with self.assertRaises(OverwritesIntermediateResult):
            ir.q_goal_pose['first'] = (2, 3, 4)

        with ir.allow_changes():
            new_value = (2, 3, 4, 5, 6)
            ir.q_goal_pose['first'] = new_value

        self.assertEqual(ir.q_goal_pose['first'], new_value)


if __name__ == '__main__':
    unittest.main()
