import json
import logging
import random
import unittest

import numpy as np
import numpy.testing as np_test
from timor import Solution
from timor.task.CostFunctions import CostFunctionBase
from timor.task.Task import Task
from timor.utilities import dtypes

from .utils import get_tasks_solutions


class JsonSerializationTests(unittest.TestCase):
    """Tests all relevant json (de)serialization methods"""

    def setUp(self) -> None:
        """Setup seeds + find test tasks"""
        random.seed(99)
        np.random.seed(99)

        # Set up tasks and solutions, for which there are already some json files
        self.task_files = get_tasks_solutions()['task_files']
        self.solution_files = get_tasks_solutions()['solution_files']

    def test_load_then_dump_solution(self, test_first_n_only=10):
        """Try loading and saving solutions."""
        # Time constraints, don't want to instantiate too many (takes minutes)
        # TODO: Solutions currently don't support a to_json method. Needs to be implemented and then tested.
        for i, solution in enumerate(dtypes.randomly(self.solution_files)):
            logging.info(f"Testing {solution}")
            if i >= test_first_n_only:
                break
            content = solution.open('r').read()
            task_id = json.loads(content)["taskID"]
            task_file = next(filter(lambda x: str(task_id) in str(x), self.task_files))
            task = Task.from_json_file(task_file)

            solution_instance = Solution.SolutionBase.from_json_file(solution, {str(task_id): task})

            dump = solution_instance.to_json_data()
            original = json.loads(content)
            for k in dump.keys() - {'moduleConnection', 'baseConnection', 'tags'}:
                if dump[k] == '':
                    self.assertTrue(k not in original or original[k] == dump[k])
                elif k == 'moduleOrder':
                    self.assertEqual(dump[k], tuple(map(str, original[k])))
                elif k == 'costFunction':  # Might be re-ordered or have "1" factor
                    self.assertEqual(CostFunctionBase.from_descriptor(dump[k]),
                                     CostFunctionBase.from_descriptor(original[k]))
                elif k == 'cost' and dump['costFunction'] in ('effort',):
                    continue
                elif k == 'cost':
                    np_test.assert_array_almost_equal(dump[k], original[k], decimal=1)
                else:
                    if (isinstance(original[k], dict) and 'goal2time' in original[k]
                            and len(original[k]['goal2time']) == 0):
                        original[k].pop('goal2time')
                    self.assertEqual(dump[k], original[k], f"Difference in key {k}")


if __name__ == '__main__':
    unittest.main()
