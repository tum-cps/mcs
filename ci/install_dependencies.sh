#!/bin/bash
set -e -x  # Make sure errors are returned to user and print run commands

# This script installs mcs including OMPL and its dependencies in the current environment. It is used in the neighboring
# Dockerfile to build the docker image for the mcs project. This file is tested with python 3.10 running in ubuntu:22.04

apt-get update
apt-get install -y libeigen3-dev python3-dev python3-pip  # Install python + kunz trajectory dependencies
pip install -U pip  # Out of date pip with ubuntu 22.04 unable to read timor-python

# Install timor from the timor-python folder
pip install -e timor-python/.
if python3 -c "import timor"; then echo "Timor installed"; else echo "Timor not installed" && exit 1; fi # Check that timor is installed
pip install -e .[full]
if python3 -c "import mcs"; then echo "MCS installed"; else echo "MCS not installed" && exit 1; fi  # Check that mcs is installed

# Install OMPL + dependencies
apt-get install -y zip g++ cmake pkg-config libboost-serialization-dev libboost-filesystem-dev libboost-system-dev libboost-program-options-dev libboost-test-dev libeigen3-dev libode-dev wget libyaml-cpp-dev python3-dev python3-pip
export CXX=g++
pip install pygccxml==2.5.0 pyplusplus
apt-get install -y castxml libboost-python-dev libboost-numpy-dev pypy3

# Build OMPL
mkdir -p /home/ompl
cd /home/ompl
wget https://github.com/ompl/ompl/archive/1.6.0.zip
unzip 1.6.0.zip
cd ompl-1.6.0
mkdir -p build/Release
cd build/Release
cmake ../..  -DPYTHON_EXEC=`which python3`
make -j $((`nproc`/2)) update_bindings
make -j $((`nproc`/2))
make install

# Clean up
apt-get clean all
rm -Rf /root/.cache/pip
rm /home/ompl/1.6.0.zip
rm -rf /home/ompl/build
rm -rf /home/ompl/py-bindings

# Check if OMPL is installed and importable by python
if python3 -c "import ompl.base; import ompl.geometric"; then
  echo "OMPL installed successfully"; else
  echo "OMPL not installed" && exit 1;
fi
