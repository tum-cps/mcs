#!python
from pybind11.setup_helpers import Pybind11Extension
from setuptools import setup

ext_modules = [
    Pybind11Extension(
        "mcs.utilities.kunz_trajectory",
        sorted(("src/mcs/utilities/kunz_trajectory.cpp",
                "src/mcs/utilities/kunz_trajectory/Path.cpp",
                "src/mcs/utilities/kunz_trajectory/Trajectory.cpp")),
        include_dirs=['/usr/include/eigen3']
    )
]

if __name__ == '__main__':
    setup(ext_modules=ext_modules)
