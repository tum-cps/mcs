[![coverage report](https://gitlab.lrz.de/tum-cps/mcs/badges/main/coverage.svg)](https://gitlab.lrz.de/tum-cps/mcs/-/jobs/artifacts/main/file/ci/coverage/html//index.html?job=coverage)  [![Documentation](https://readthedocs.org/projects/pip/badge/)](https://tum-cps.pages.gitlab.lrz.de/mcs/doc/)

[comment]: # (The documentation badge is always green, even when it is not passing)
# ModRob Configuration Synthesis

A Repo to collect and develop algorithms to synthesize modular robot configurations and possibly robot morphologies.

# Setup in short
1. Prerequisits:
   * a linux system
   * tested with Ubuntu 20.04 LTS + Python 3.9
   * [SSH key setup with gitlab](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account)
   * read access to the [cps-robotics group](https://gitlab.lrz.de/cps-robotics) (>= Reporter)
   * [git lfs](https://git-lfs.github.com/) installed
   * gcc, g++, libeigen3-dev installed (`(sudo) apt-get update && (sudo) apt-get install -y gcc g++ libeigen3-dev`)
2. After cloning the repository, initialize all submodules via `git submodule update --init` or during clone with `git clone --recurse-submodules  git@gitlab.lrz.de:tum-cps/mcs.git`, and update them via `git submodule update --merge` in case they have been changed.
3. Install all mcs dependencies and mcs locally by running `pip install -e <path_to_mcs_top_level_directory>.[full]`; do this also to re-compile updated C++ bindings.
4. The code in this repository heavily relies on bleeding edge features of timor-python ([repo](https://gitlab.lrz.de/tum-cps/timor-python), [docs](https://timor-python.readthedocs.io/en/latest/), [PyPI](https://pypi.org/project/timor-python/)), which is why we can't use the published version on PyPI in the general case. Install it locally running `pip install -e <path_to_timor_subrepository>`.

## Additional setup for development
* You may want to install mcs in a conda environment for better control and isolation:
  1. Create environment: `conda create --name mcs python==3.9`
  2. Activate it `conda activate mcs`
* If you want to use complete path planning we suggest using docker compose: `docker compose run -i --service-ports mcs bash`. For the adventurous please consider ci/install_dependencies.sh if you want to compile OMPL locally in your own environment.
* Create/adapt the [timor-config](timor-python/timor.config) to use local robots, tasks, etc. in unit tests. It could look like this:
```
[LOGGING]
filemode = a
level = DEBUG
filename = /tmp/timor.log

[FILE_LOCATIONS]
robots = ["<path-to-mcs>/timor-python/src/timor_sample_robots",
    "<path-to-mcs>/robots"]
test_data = <path-to-mcs>/tests/data
schema_dir = <path-to-mcs>/tests/data/schemas
```
All config features are discussed in timor.
* Configure pycharm to use _Existing environment_ (https://www.jetbrains.com/help/pycharm/conda-support-creating-conda-virtual-environment.html); if you want to develop with OMPL path planning we suggest using docker compose (File -> Settings -> Project Interpreter -> Add -> On docker compose -> Select [docker-compose.yml](docker-compose.yml) -> Select service mcs -> Many OKs -> check by running test cases)
* Set [src](src) and [timor-python/src](timor-python/src) as source directory and [tests](tests) as test directory (in pycharm right click on folder -> "Mark Directory as")
* Test installation with demos in src/mcs/demos/, e.g., [robot_introduction](src/mcs/demos/base/robot_introduction.py) which loads models from different sources or [solve_task](src/mcs/demos/ompl/solve_task.py) which plans a short trajectory for a robot.

# Continuous Integration

Please make sure to write unittests for every new feature implemented. Gitlab will automatically run all tests placed in the `tests/` folder at every push. Merges to main are only possible if the pipeline runs through.

Merge pipelines are evaluated on the merge result, not the latest branch commit.

To make sure you stay within our code quality guidelines we suggest using pre-commit hooks by running `pip install pre-commit && pre-commit install`.

**Code style** is enforced by flake8 (custom maximum line length for python code: 120 characters). The CI runner will fail if you don't follow the flake linting guidelines. Check the [.gitlab-ci.yml](.gitlab-ci.yml) file to see what flake8 command exactly is being used. If you explicitly want to ignore a flake error, use the [noqa](https://flake8.pycqa.org/en/3.1.1/user/ignoring-errors.html) tag.

**Coverage** of the unittest is automatically evaluated. If coverage drops under a certain level, the pipeline will fail. (currently, 80% - long term goal: 90%)

**Docstrings**: There is a docstring check in place. The CI pipeline fails if you implement a function, method or class without a docstring. For this purpose, [pydocstyle](http://www.pydocstyle.org/en/stable/) is used. For details, see the [config file](.pydocstyle). You can build the doc locally with `cd doc; pip install -r requirments.txt; make html` and opening [index.html](doc/build/html/index.html).

## Running CI build containers

Use the provided docker-compose file to get a shell with all dependencies installed: `docker compose run -i --service-ports mcs bash`.

Run tests (e.g. those with ompl): `pytest -m "ompl" tests`

# Config Files
To include additional robots, scenarios, etc. to be loaded with Timor, you can edit the timor.config file of your local timor installation. For instructions on how to find the file, check out the [Timor repository](https://gitlab.lrz.de/tum-cps/timor-python). It is advised to include the robots/ subrepository as an additional robot directory there.
