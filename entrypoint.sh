#!/bin/sh

RED='\033[0;31m'
NC='\033[0m' # No Color

printf "${RED}Adding ${PWD} to git safe directory${NC}\n"
git config --global --add safe.directory $PWD  # TODO Current directory
printf "${RED}Updating mcs and timor-python${NC} before running $@"
pip install -e .[full]  # Update mcs
pip install -e timor-python/ # Update timor-python
exec "$@"
